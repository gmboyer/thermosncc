# ThermoSNCC prototype

Thermodynamic Stellar Nebula Condensation Calculator for calculating condensation sequences under cosmochemical conditions. [Check out our poster  for more information.](https://gitlab.com/gmboyer/thermosncc/blob/master/Poster/AGU_2019_ThermoSNCC.pdf)

This project is under heavy development and exists only as a prototype. There are likely bugs!

## Prototype quick start guide:
### Installation
1) Click 'Download' and save as .zip
2) Navigate to [server.enki-portal.org] and follow the instructions there for accessing the ENKI online server. All you will need is a free GitLab account.
3) Once your server has started up, upload thermosncc-master.zip to your server directory. Click and drag the zip file to your directory or use the 'upload files' button.
4) Click the dropdown menu: File > New > New Terminal. Type ```unzip thermosncc-master.zip``` in the terminal prompt and hit enter. Wait a few moments for the zip to unpack and then open the new 'thermosncc-master' folder that has just been created. You may need to refresh your directory to see it.
### Calculating a sample condensation sequence
1) Open the 'Model Application' .ipynb file. From the dropdown menu, select Kernel > Restart Kernel and Run All. This will calculate a sample condensation sequence and plot results.

**Note:** it will likely take a long time to run this notebook for the first time while the stellar gas phase installs. Our goal is to eventually have this phase pre-installed on the ENKI server for everyone to immediately access.