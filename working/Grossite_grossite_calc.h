
const char *Grossite_grossite_identifier(void);
const char *Grossite_grossite_name(void);
const char *Grossite_grossite_formula(void);
const double Grossite_grossite_mw(void);
const double *Grossite_grossite_elements(void);

double Grossite_grossite_g(double T, double P);
double Grossite_grossite_dgdt(double T, double P);
double Grossite_grossite_dgdp(double T, double P);
double Grossite_grossite_d2gdt2(double T, double P);
double Grossite_grossite_d2gdtdp(double T, double P);
double Grossite_grossite_d2gdp2(double T, double P);
double Grossite_grossite_d3gdt3(double T, double P);
double Grossite_grossite_d3gdt2dp(double T, double P);
double Grossite_grossite_d3gdtdp2(double T, double P);
double Grossite_grossite_d3gdp3(double T, double P);

double Grossite_grossite_s(double T, double P);
double Grossite_grossite_v(double T, double P);
double Grossite_grossite_cv(double T, double P);
double Grossite_grossite_cp(double T, double P);
double Grossite_grossite_dcpdt(double T, double P);
double Grossite_grossite_alpha(double T, double P);
double Grossite_grossite_beta(double T, double P);
double Grossite_grossite_K(double T, double P);
double Grossite_grossite_Kp(double T, double P);

