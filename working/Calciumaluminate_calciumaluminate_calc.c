
static const char *identifier = "Mon Dec  2 10:59:21 2019";

static const double T_r = 298.15;
static const double P_r = 1.0;
static const double H_TrPr = -2330292.0;
static const double S_TrPr = 115.348;
static const double k0 = 227.04;
static const double k1 = -1669.1;
static const double k2 = -560800.0;
static const double k3 = -85540000.0;

#include <math.h>

static double calciumaluminate_g(double T, double P) {
    double result = 0.0;
    result += H_TrPr + 2*sqrt(T)*k1 + T*k0 - T*(S_TrPr + k0*log(T) - k0*log(T_r) + (1.0/2.0)*k2/((T_r)*(T_r)) + (1.0/3.0)*k3/((T_r)*(T_r)*(T_r)) + 2*k1/sqrt(T_r) - 1.0/2.0*k2/((T)*(T)) - 1.0/3.0*k3/((T)*(T)*(T)) - 2*k1/sqrt(T)) - 2*sqrt(T_r)*k1 - T_r*k0 + k2/T_r + (1.0/2.0)*k3/((T_r)*(T_r)) - k2/T - 1.0/2.0*k3/((T)*(T));
    return result;
}

static double calciumaluminate_dgdt(double T, double P) {
    double result = 0.0;
    result += -S_TrPr - T*(k0/T + k2/((T)*(T)*(T)) + k3/((T)*(T)*(T)*(T)) + k1/pow(T, 3.0/2.0)) - k0*log(T) + k0*log(T_r) + k0 - 1.0/2.0*k2/((T_r)*(T_r)) - 1.0/3.0*k3/((T_r)*(T_r)*(T_r)) - 2*k1/sqrt(T_r) + (3.0/2.0)*k2/((T)*(T)) + (4.0/3.0)*k3/((T)*(T)*(T)) + 3*k1/sqrt(T);
    return result;
}

static double calciumaluminate_dgdp(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double calciumaluminate_d2gdt2(double T, double P) {
    double result = 0.0;
    result += (1.0/2.0)*T*(2*k0/((T)*(T)) + 6*k2/((T)*(T)*(T)*(T)) + 8*k3/pow(T, 5) + 3*k1/pow(T, 5.0/2.0)) - 2*k0/T - 4*k2/((T)*(T)*(T)) - 5*k3/((T)*(T)*(T)*(T)) - 5.0/2.0*k1/pow(T, 3.0/2.0);
    return result;
}

static double calciumaluminate_d2gdtdp(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double calciumaluminate_d2gdp2(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double calciumaluminate_d3gdt3(double T, double P) {
    double result = 0.0;
    result += -1.0/4.0*T*(8*k0/((T)*(T)*(T)) + 48*k2/pow(T, 5) + 80*k3/pow(T, 6) + 15*k1/pow(T, 7.0/2.0)) + 3*k0/((T)*(T)) + 15*k2/((T)*(T)*(T)*(T)) + 24*k3/pow(T, 5) + (21.0/4.0)*k1/pow(T, 5.0/2.0);
    return result;
}

static double calciumaluminate_d3gdt2dp(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double calciumaluminate_d3gdtdp2(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double calciumaluminate_d3gdp3(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}


static double calciumaluminate_s(double T, double P) {
    double result = -calciumaluminate_dgdt(T, P);
    return result;
}

static double calciumaluminate_v(double T, double P) {
    double result = calciumaluminate_dgdp(T, P);
    return result;
}

static double calciumaluminate_cv(double T, double P) {
    double result = -T*calciumaluminate_d2gdt2(T, P);
    double dvdt = calciumaluminate_d2gdtdp(T, P);
    double dvdp = calciumaluminate_d2gdp2(T, P);
    result += T*dvdt*dvdt/dvdp;
    return result;
}

static double calciumaluminate_cp(double T, double P) {
    double result = -T*calciumaluminate_d2gdt2(T, P);
    return result;
}

static double calciumaluminate_dcpdt(double T, double P) {
    double result = -T*calciumaluminate_d3gdt3(T, P) - calciumaluminate_d2gdt2(T, P);
    return result;
}

static double calciumaluminate_alpha(double T, double P) {
    double result = calciumaluminate_d2gdtdp(T, P)/calciumaluminate_dgdp(T, P);
    return result;
}

static double calciumaluminate_beta(double T, double P) {
    double result = -calciumaluminate_d2gdp2(T, P)/calciumaluminate_dgdp(T, P);
    return result;
}

static double calciumaluminate_K(double T, double P) {
    double result = -calciumaluminate_dgdp(T, P)/calciumaluminate_d2gdp2(T, P);
    return result;
}

static double calciumaluminate_Kp(double T, double P) {
    double result = calciumaluminate_dgdp(T, P);
    result *= calciumaluminate_d3gdp3(T, P);
    result /= pow(calciumaluminate_d2gdp2(T, P), 2.0);
    return result - 1.0;
}


const char *Calciumaluminate_calciumaluminate_identifier(void) {
    return identifier;
}

const char *Calciumaluminate_calciumaluminate_name(void) {
    return "Calciumaluminate";
}

const char *Calciumaluminate_calciumaluminate_formula(void) {
    return "CaAl2O4";
}

const double Calciumaluminate_calciumaluminate_mw(void) {
    return 158.04068;
}

static const double elmformula[106] = {
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,4.0,0.0,0.0,0.0,
        0.0,2.0,0.0,0.0,0.0,0.0,
        0.0,0.0,1.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0
    };

const double *Calciumaluminate_calciumaluminate_elements(void) {
    return elmformula;
}

double Calciumaluminate_calciumaluminate_g(double T, double P) {
    return calciumaluminate_g(T, P);
}

double Calciumaluminate_calciumaluminate_dgdt(double T, double P) {
    return calciumaluminate_dgdt(T, P);
}

double Calciumaluminate_calciumaluminate_dgdp(double T, double P) {
    return calciumaluminate_dgdp(T, P);
}

double Calciumaluminate_calciumaluminate_d2gdt2(double T, double P) {
    return calciumaluminate_d2gdt2(T, P);
}

double Calciumaluminate_calciumaluminate_d2gdtdp(double T, double P) {
    return calciumaluminate_d2gdtdp(T, P);
}

double Calciumaluminate_calciumaluminate_d2gdp2(double T, double P) {
    return calciumaluminate_d2gdp2(T, P);
}

double Calciumaluminate_calciumaluminate_d3gdt3(double T, double P) {
    return calciumaluminate_d3gdt3(T, P);
}

double Calciumaluminate_calciumaluminate_d3gdt2dp(double T, double P) {
    return calciumaluminate_d3gdt2dp(T, P);
}

double Calciumaluminate_calciumaluminate_d3gdtdp2(double T, double P) {
    return calciumaluminate_d3gdtdp2(T, P);
}

double Calciumaluminate_calciumaluminate_d3gdp3(double T, double P) {
    return calciumaluminate_d3gdp3(T, P);
}

double Calciumaluminate_calciumaluminate_s(double T, double P) {
    return calciumaluminate_s(T, P);
}

double Calciumaluminate_calciumaluminate_v(double T, double P) {
    return calciumaluminate_v(T, P);
}

double Calciumaluminate_calciumaluminate_cv(double T, double P) {
    return calciumaluminate_cv(T, P);
}

double Calciumaluminate_calciumaluminate_cp(double T, double P) {
    return calciumaluminate_cp(T, P);
}

double Calciumaluminate_calciumaluminate_dcpdt(double T, double P) {
    return calciumaluminate_dcpdt(T, P);
}

double Calciumaluminate_calciumaluminate_alpha(double T, double P) {
    return calciumaluminate_alpha(T, P);
}

double Calciumaluminate_calciumaluminate_beta(double T, double P) {
    return calciumaluminate_beta(T, P);
}

double Calciumaluminate_calciumaluminate_K(double T, double P) {
    return calciumaluminate_K(T, P);
}

double Calciumaluminate_calciumaluminate_Kp(double T, double P) {
    return calciumaluminate_Kp(T, P);
}

