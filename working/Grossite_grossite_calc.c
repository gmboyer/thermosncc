
static const char *identifier = "Mon Dec  2 10:59:17 2019";

static const double T_r = 298.15;
static const double P_r = 1.0;
static const double H_TrPr = -4007413.0;
static const double S_TrPr = 175.06;
static const double k0 = 337.98;
static const double k1 = -1022.2;
static const double k2 = -12112600.0;
static const double k3 = 1510600000.0;

#include <math.h>

static double grossite_g(double T, double P) {
    double result = 0.0;
    result += H_TrPr + 2*sqrt(T)*k1 + T*k0 - T*(S_TrPr + k0*log(T) - k0*log(T_r) + (1.0/2.0)*k2/((T_r)*(T_r)) + (1.0/3.0)*k3/((T_r)*(T_r)*(T_r)) + 2*k1/sqrt(T_r) - 1.0/2.0*k2/((T)*(T)) - 1.0/3.0*k3/((T)*(T)*(T)) - 2*k1/sqrt(T)) - 2*sqrt(T_r)*k1 - T_r*k0 + k2/T_r + (1.0/2.0)*k3/((T_r)*(T_r)) - k2/T - 1.0/2.0*k3/((T)*(T));
    return result;
}

static double grossite_dgdt(double T, double P) {
    double result = 0.0;
    result += -S_TrPr - T*(k0/T + k2/((T)*(T)*(T)) + k3/((T)*(T)*(T)*(T)) + k1/pow(T, 3.0/2.0)) - k0*log(T) + k0*log(T_r) + k0 - 1.0/2.0*k2/((T_r)*(T_r)) - 1.0/3.0*k3/((T_r)*(T_r)*(T_r)) - 2*k1/sqrt(T_r) + (3.0/2.0)*k2/((T)*(T)) + (4.0/3.0)*k3/((T)*(T)*(T)) + 3*k1/sqrt(T);
    return result;
}

static double grossite_dgdp(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double grossite_d2gdt2(double T, double P) {
    double result = 0.0;
    result += (1.0/2.0)*T*(2*k0/((T)*(T)) + 6*k2/((T)*(T)*(T)*(T)) + 8*k3/pow(T, 5) + 3*k1/pow(T, 5.0/2.0)) - 2*k0/T - 4*k2/((T)*(T)*(T)) - 5*k3/((T)*(T)*(T)*(T)) - 5.0/2.0*k1/pow(T, 3.0/2.0);
    return result;
}

static double grossite_d2gdtdp(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double grossite_d2gdp2(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double grossite_d3gdt3(double T, double P) {
    double result = 0.0;
    result += -1.0/4.0*T*(8*k0/((T)*(T)*(T)) + 48*k2/pow(T, 5) + 80*k3/pow(T, 6) + 15*k1/pow(T, 7.0/2.0)) + 3*k0/((T)*(T)) + 15*k2/((T)*(T)*(T)*(T)) + 24*k3/pow(T, 5) + (21.0/4.0)*k1/pow(T, 5.0/2.0);
    return result;
}

static double grossite_d3gdt2dp(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double grossite_d3gdtdp2(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double grossite_d3gdp3(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}


static double grossite_s(double T, double P) {
    double result = -grossite_dgdt(T, P);
    return result;
}

static double grossite_v(double T, double P) {
    double result = grossite_dgdp(T, P);
    return result;
}

static double grossite_cv(double T, double P) {
    double result = -T*grossite_d2gdt2(T, P);
    double dvdt = grossite_d2gdtdp(T, P);
    double dvdp = grossite_d2gdp2(T, P);
    result += T*dvdt*dvdt/dvdp;
    return result;
}

static double grossite_cp(double T, double P) {
    double result = -T*grossite_d2gdt2(T, P);
    return result;
}

static double grossite_dcpdt(double T, double P) {
    double result = -T*grossite_d3gdt3(T, P) - grossite_d2gdt2(T, P);
    return result;
}

static double grossite_alpha(double T, double P) {
    double result = grossite_d2gdtdp(T, P)/grossite_dgdp(T, P);
    return result;
}

static double grossite_beta(double T, double P) {
    double result = -grossite_d2gdp2(T, P)/grossite_dgdp(T, P);
    return result;
}

static double grossite_K(double T, double P) {
    double result = -grossite_dgdp(T, P)/grossite_d2gdp2(T, P);
    return result;
}

static double grossite_Kp(double T, double P) {
    double result = grossite_dgdp(T, P);
    result *= grossite_d3gdp3(T, P);
    result /= pow(grossite_d2gdp2(T, P), 2.0);
    return result - 1.0;
}


const char *Grossite_grossite_identifier(void) {
    return identifier;
}

const char *Grossite_grossite_name(void) {
    return "Grossite";
}

const char *Grossite_grossite_formula(void) {
    return "CaAl4O7";
}

const double Grossite_grossite_mw(void) {
    return 260.00196000000005;
}

static const double elmformula[106] = {
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,7.0,0.0,0.0,0.0,
        0.0,4.0,0.0,0.0,0.0,0.0,
        0.0,0.0,1.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0
    };

const double *Grossite_grossite_elements(void) {
    return elmformula;
}

double Grossite_grossite_g(double T, double P) {
    return grossite_g(T, P);
}

double Grossite_grossite_dgdt(double T, double P) {
    return grossite_dgdt(T, P);
}

double Grossite_grossite_dgdp(double T, double P) {
    return grossite_dgdp(T, P);
}

double Grossite_grossite_d2gdt2(double T, double P) {
    return grossite_d2gdt2(T, P);
}

double Grossite_grossite_d2gdtdp(double T, double P) {
    return grossite_d2gdtdp(T, P);
}

double Grossite_grossite_d2gdp2(double T, double P) {
    return grossite_d2gdp2(T, P);
}

double Grossite_grossite_d3gdt3(double T, double P) {
    return grossite_d3gdt3(T, P);
}

double Grossite_grossite_d3gdt2dp(double T, double P) {
    return grossite_d3gdt2dp(T, P);
}

double Grossite_grossite_d3gdtdp2(double T, double P) {
    return grossite_d3gdtdp2(T, P);
}

double Grossite_grossite_d3gdp3(double T, double P) {
    return grossite_d3gdp3(T, P);
}

double Grossite_grossite_s(double T, double P) {
    return grossite_s(T, P);
}

double Grossite_grossite_v(double T, double P) {
    return grossite_v(T, P);
}

double Grossite_grossite_cv(double T, double P) {
    return grossite_cv(T, P);
}

double Grossite_grossite_cp(double T, double P) {
    return grossite_cp(T, P);
}

double Grossite_grossite_dcpdt(double T, double P) {
    return grossite_dcpdt(T, P);
}

double Grossite_grossite_alpha(double T, double P) {
    return grossite_alpha(T, P);
}

double Grossite_grossite_beta(double T, double P) {
    return grossite_beta(T, P);
}

double Grossite_grossite_K(double T, double P) {
    return grossite_K(T, P);
}

double Grossite_grossite_Kp(double T, double P) {
    return grossite_Kp(T, P);
}

