
const char *Hibonite_hibonite_identifier(void);
const char *Hibonite_hibonite_name(void);
const char *Hibonite_hibonite_formula(void);
const double Hibonite_hibonite_mw(void);
const double *Hibonite_hibonite_elements(void);

double Hibonite_hibonite_g(double T, double P);
double Hibonite_hibonite_dgdt(double T, double P);
double Hibonite_hibonite_dgdp(double T, double P);
double Hibonite_hibonite_d2gdt2(double T, double P);
double Hibonite_hibonite_d2gdtdp(double T, double P);
double Hibonite_hibonite_d2gdp2(double T, double P);
double Hibonite_hibonite_d3gdt3(double T, double P);
double Hibonite_hibonite_d3gdt2dp(double T, double P);
double Hibonite_hibonite_d3gdtdp2(double T, double P);
double Hibonite_hibonite_d3gdp3(double T, double P);

double Hibonite_hibonite_s(double T, double P);
double Hibonite_hibonite_v(double T, double P);
double Hibonite_hibonite_cv(double T, double P);
double Hibonite_hibonite_cp(double T, double P);
double Hibonite_hibonite_dcpdt(double T, double P);
double Hibonite_hibonite_alpha(double T, double P);
double Hibonite_hibonite_beta(double T, double P);
double Hibonite_hibonite_K(double T, double P);
double Hibonite_hibonite_Kp(double T, double P);

