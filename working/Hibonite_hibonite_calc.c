
static const char *identifier = "Mon Dec  2 10:59:32 2019";

static const double T_r = 298.15;
static const double P_r = 1.0;
static const double H_TrPr = -10724605.0;
static const double S_TrPr = 368.719;
static const double a = 973.423;
static const double c = -5517950.0;
static const double f = -3135.46;
static const double h = -63511.12;

#include <math.h>

static double hibonite_g(double T, double P) {
    double result = 0.0;
    result += H_TrPr + 2.0*sqrt(T)*f + T*a - T*(S_TrPr - 0.5*pow(T, -2.0)*c - 1.0*1.0/T*h - 2.0*pow(T, -0.5)*f + 0.5*pow(T_r, -2.0)*c + 1.0*1.0/T_r*h + 2.0*pow(T_r, -0.5)*f + 2.0*a*log(sqrt(T)) - 2.0*a*log(sqrt(T_r))) - 2.0*sqrt(T_r)*f - T_r*a + h*log(T) - h*log(T_r) + c/T_r - c/T;
    return result;
}

static double hibonite_dgdt(double T, double P) {
    double result = 0.0;
    result += -S_TrPr + 0.5*pow(T, -2.0)*c + 1.0*1.0/T*h + 3.0*pow(T, -0.5)*f - T*(1.0*pow(T, -3.0)*c + 1.0*pow(T, -2.0)*h + 1.0*pow(T, -1.5)*f + 1.0*1.0/T*a) - 0.5*pow(T_r, -2.0)*c - 1.0*1.0/T_r*h - 2.0*pow(T_r, -0.5)*f - 2.0*a*log(sqrt(T)) + 2.0*a*log(sqrt(T_r)) + a + h/T + c/((T)*(T));
    return result;
}

static double hibonite_dgdp(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double hibonite_d2gdt2(double T, double P) {
    double result = 0.0;
    result += -2.0*pow(T, -3.0)*c - 2.0*pow(T, -2.0)*h - 2.5*pow(T, -1.5)*f - 2.0*1.0/T*a + T*(3.0*pow(T, -4.0)*c + 2.0*pow(T, -3.0)*h + 1.5*pow(T, -2.5)*f + 1.0*pow(T, -2.0)*a) - h/((T)*(T)) - 2*c/((T)*(T)*(T));
    return result;
}

static double hibonite_d2gdtdp(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double hibonite_d2gdp2(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double hibonite_d3gdt3(double T, double P) {
    double result = 0.0;
    result += 9.0*pow(T, -4.0)*c + 6.0*pow(T, -3.0)*h + 5.25*pow(T, -2.5)*f + 3.0*pow(T, -2.0)*a - T*(12.0*pow(T, -5.0)*c + 6.0*pow(T, -4.0)*h + 3.75*pow(T, -3.5)*f + 2.0*pow(T, -3.0)*a) + 2*h/((T)*(T)*(T)) + 6*c/((T)*(T)*(T)*(T));
    return result;
}

static double hibonite_d3gdt2dp(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double hibonite_d3gdtdp2(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double hibonite_d3gdp3(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}


static double hibonite_s(double T, double P) {
    double result = -hibonite_dgdt(T, P);
    return result;
}

static double hibonite_v(double T, double P) {
    double result = hibonite_dgdp(T, P);
    return result;
}

static double hibonite_cv(double T, double P) {
    double result = -T*hibonite_d2gdt2(T, P);
    double dvdt = hibonite_d2gdtdp(T, P);
    double dvdp = hibonite_d2gdp2(T, P);
    result += T*dvdt*dvdt/dvdp;
    return result;
}

static double hibonite_cp(double T, double P) {
    double result = -T*hibonite_d2gdt2(T, P);
    return result;
}

static double hibonite_dcpdt(double T, double P) {
    double result = -T*hibonite_d3gdt3(T, P) - hibonite_d2gdt2(T, P);
    return result;
}

static double hibonite_alpha(double T, double P) {
    double result = hibonite_d2gdtdp(T, P)/hibonite_dgdp(T, P);
    return result;
}

static double hibonite_beta(double T, double P) {
    double result = -hibonite_d2gdp2(T, P)/hibonite_dgdp(T, P);
    return result;
}

static double hibonite_K(double T, double P) {
    double result = -hibonite_dgdp(T, P)/hibonite_d2gdp2(T, P);
    return result;
}

static double hibonite_Kp(double T, double P) {
    double result = hibonite_dgdp(T, P);
    result *= hibonite_d3gdp3(T, P);
    result /= pow(hibonite_d2gdp2(T, P), 2.0);
    return result - 1.0;
}


const char *Hibonite_hibonite_identifier(void) {
    return identifier;
}

const char *Hibonite_hibonite_name(void) {
    return "Hibonite";
}

const char *Hibonite_hibonite_formula(void) {
    return "CaAl12O19";
}

const double Hibonite_hibonite_mw(void) {
    return 667.84708;
}

static const double elmformula[106] = {
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,19.0,0.0,0.0,0.0,
        0.0,12.0,0.0,0.0,0.0,0.0,
        0.0,0.0,1.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0
    };

const double *Hibonite_hibonite_elements(void) {
    return elmformula;
}

double Hibonite_hibonite_g(double T, double P) {
    return hibonite_g(T, P);
}

double Hibonite_hibonite_dgdt(double T, double P) {
    return hibonite_dgdt(T, P);
}

double Hibonite_hibonite_dgdp(double T, double P) {
    return hibonite_dgdp(T, P);
}

double Hibonite_hibonite_d2gdt2(double T, double P) {
    return hibonite_d2gdt2(T, P);
}

double Hibonite_hibonite_d2gdtdp(double T, double P) {
    return hibonite_d2gdtdp(T, P);
}

double Hibonite_hibonite_d2gdp2(double T, double P) {
    return hibonite_d2gdp2(T, P);
}

double Hibonite_hibonite_d3gdt3(double T, double P) {
    return hibonite_d3gdt3(T, P);
}

double Hibonite_hibonite_d3gdt2dp(double T, double P) {
    return hibonite_d3gdt2dp(T, P);
}

double Hibonite_hibonite_d3gdtdp2(double T, double P) {
    return hibonite_d3gdtdp2(T, P);
}

double Hibonite_hibonite_d3gdp3(double T, double P) {
    return hibonite_d3gdp3(T, P);
}

double Hibonite_hibonite_s(double T, double P) {
    return hibonite_s(T, P);
}

double Hibonite_hibonite_v(double T, double P) {
    return hibonite_v(T, P);
}

double Hibonite_hibonite_cv(double T, double P) {
    return hibonite_cv(T, P);
}

double Hibonite_hibonite_cp(double T, double P) {
    return hibonite_cp(T, P);
}

double Hibonite_hibonite_dcpdt(double T, double P) {
    return hibonite_dcpdt(T, P);
}

double Hibonite_hibonite_alpha(double T, double P) {
    return hibonite_alpha(T, P);
}

double Hibonite_hibonite_beta(double T, double P) {
    return hibonite_beta(T, P);
}

double Hibonite_hibonite_K(double T, double P) {
    return hibonite_K(T, P);
}

double Hibonite_hibonite_Kp(double T, double P) {
    return hibonite_Kp(T, P);
}

