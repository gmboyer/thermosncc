
#include <stdlib.h>
#include <stdio.h>

#include "H1_gas_species_t_calib.h"
#include "He1_gas_species_t_calib.h"
#include "O1_gas_species_t_calib.h"
#include "Ne1_gas_species_t_calib.h"
#include "Mg1_gas_species_t_calib.h"
#include "Al1_gas_species_t_calib.h"
#include "Ar1_gas_species_t_calib.h"
#include "Ca1_gas_species_t_calib.h"
#include "Ti1_gas_species_t_calib.h"


typedef struct _endmembers {
  const char *(*name) (void);
  const char *(*formula) (void);
  const double (*mw) (void);
  const double *(*elements) (void);
  double (*mu0) (double t, double p);
  double (*dmu0dT) (double t, double p);
  double (*dmu0dP) (double t, double p);
  double (*d2mu0dT2) (double t, double p);
  double (*d2mu0dTdP) (double t, double p);
  double (*d2mu0dP2) (double t, double p);
  double (*d3mu0dT3) (double t, double p);
  double (*d3mu0dT2dP) (double t, double p);
  double (*d3mu0dTdP2) (double t, double p);
  double (*d3mu0dP3) (double t, double p);
} Endmembers;

static Endmembers endmember[] = {
  {
    H1_gas_species_t_calib_name,
    H1_gas_species_t_calib_formula,
    H1_gas_species_t_calib_mw,
    H1_gas_species_t_calib_elements,
    H1_gas_species_t_calib_g,
    H1_gas_species_t_calib_dgdt,
    H1_gas_species_t_calib_dgdp,
    H1_gas_species_t_calib_d2gdt2,
    H1_gas_species_t_calib_d2gdtdp,
    H1_gas_species_t_calib_d2gdp2,
    H1_gas_species_t_calib_d3gdt3,
    H1_gas_species_t_calib_d3gdt2dp,
    H1_gas_species_t_calib_d3gdtdp2,
    H1_gas_species_t_calib_d3gdp3
  },
  {
    He1_gas_species_t_calib_name,
    He1_gas_species_t_calib_formula,
    He1_gas_species_t_calib_mw,
    He1_gas_species_t_calib_elements,
    He1_gas_species_t_calib_g,
    He1_gas_species_t_calib_dgdt,
    He1_gas_species_t_calib_dgdp,
    He1_gas_species_t_calib_d2gdt2,
    He1_gas_species_t_calib_d2gdtdp,
    He1_gas_species_t_calib_d2gdp2,
    He1_gas_species_t_calib_d3gdt3,
    He1_gas_species_t_calib_d3gdt2dp,
    He1_gas_species_t_calib_d3gdtdp2,
    He1_gas_species_t_calib_d3gdp3
  },
  {
    O1_gas_species_t_calib_name,
    O1_gas_species_t_calib_formula,
    O1_gas_species_t_calib_mw,
    O1_gas_species_t_calib_elements,
    O1_gas_species_t_calib_g,
    O1_gas_species_t_calib_dgdt,
    O1_gas_species_t_calib_dgdp,
    O1_gas_species_t_calib_d2gdt2,
    O1_gas_species_t_calib_d2gdtdp,
    O1_gas_species_t_calib_d2gdp2,
    O1_gas_species_t_calib_d3gdt3,
    O1_gas_species_t_calib_d3gdt2dp,
    O1_gas_species_t_calib_d3gdtdp2,
    O1_gas_species_t_calib_d3gdp3
  },
  {
    Ne1_gas_species_t_calib_name,
    Ne1_gas_species_t_calib_formula,
    Ne1_gas_species_t_calib_mw,
    Ne1_gas_species_t_calib_elements,
    Ne1_gas_species_t_calib_g,
    Ne1_gas_species_t_calib_dgdt,
    Ne1_gas_species_t_calib_dgdp,
    Ne1_gas_species_t_calib_d2gdt2,
    Ne1_gas_species_t_calib_d2gdtdp,
    Ne1_gas_species_t_calib_d2gdp2,
    Ne1_gas_species_t_calib_d3gdt3,
    Ne1_gas_species_t_calib_d3gdt2dp,
    Ne1_gas_species_t_calib_d3gdtdp2,
    Ne1_gas_species_t_calib_d3gdp3
  },
  {
    Mg1_gas_species_t_calib_name,
    Mg1_gas_species_t_calib_formula,
    Mg1_gas_species_t_calib_mw,
    Mg1_gas_species_t_calib_elements,
    Mg1_gas_species_t_calib_g,
    Mg1_gas_species_t_calib_dgdt,
    Mg1_gas_species_t_calib_dgdp,
    Mg1_gas_species_t_calib_d2gdt2,
    Mg1_gas_species_t_calib_d2gdtdp,
    Mg1_gas_species_t_calib_d2gdp2,
    Mg1_gas_species_t_calib_d3gdt3,
    Mg1_gas_species_t_calib_d3gdt2dp,
    Mg1_gas_species_t_calib_d3gdtdp2,
    Mg1_gas_species_t_calib_d3gdp3
  },
  {
    Al1_gas_species_t_calib_name,
    Al1_gas_species_t_calib_formula,
    Al1_gas_species_t_calib_mw,
    Al1_gas_species_t_calib_elements,
    Al1_gas_species_t_calib_g,
    Al1_gas_species_t_calib_dgdt,
    Al1_gas_species_t_calib_dgdp,
    Al1_gas_species_t_calib_d2gdt2,
    Al1_gas_species_t_calib_d2gdtdp,
    Al1_gas_species_t_calib_d2gdp2,
    Al1_gas_species_t_calib_d3gdt3,
    Al1_gas_species_t_calib_d3gdt2dp,
    Al1_gas_species_t_calib_d3gdtdp2,
    Al1_gas_species_t_calib_d3gdp3
  },
  {
    Ar1_gas_species_t_calib_name,
    Ar1_gas_species_t_calib_formula,
    Ar1_gas_species_t_calib_mw,
    Ar1_gas_species_t_calib_elements,
    Ar1_gas_species_t_calib_g,
    Ar1_gas_species_t_calib_dgdt,
    Ar1_gas_species_t_calib_dgdp,
    Ar1_gas_species_t_calib_d2gdt2,
    Ar1_gas_species_t_calib_d2gdtdp,
    Ar1_gas_species_t_calib_d2gdp2,
    Ar1_gas_species_t_calib_d3gdt3,
    Ar1_gas_species_t_calib_d3gdt2dp,
    Ar1_gas_species_t_calib_d3gdtdp2,
    Ar1_gas_species_t_calib_d3gdp3
  },
  {
    Ca1_gas_species_t_calib_name,
    Ca1_gas_species_t_calib_formula,
    Ca1_gas_species_t_calib_mw,
    Ca1_gas_species_t_calib_elements,
    Ca1_gas_species_t_calib_g,
    Ca1_gas_species_t_calib_dgdt,
    Ca1_gas_species_t_calib_dgdp,
    Ca1_gas_species_t_calib_d2gdt2,
    Ca1_gas_species_t_calib_d2gdtdp,
    Ca1_gas_species_t_calib_d2gdp2,
    Ca1_gas_species_t_calib_d3gdt3,
    Ca1_gas_species_t_calib_d3gdt2dp,
    Ca1_gas_species_t_calib_d3gdtdp2,
    Ca1_gas_species_t_calib_d3gdp3
  },
  {
    Ti1_gas_species_t_calib_name,
    Ti1_gas_species_t_calib_formula,
    Ti1_gas_species_t_calib_mw,
    Ti1_gas_species_t_calib_elements,
    Ti1_gas_species_t_calib_g,
    Ti1_gas_species_t_calib_dgdt,
    Ti1_gas_species_t_calib_dgdp,
    Ti1_gas_species_t_calib_d2gdt2,
    Ti1_gas_species_t_calib_d2gdtdp,
    Ti1_gas_species_t_calib_d2gdp2,
    Ti1_gas_species_t_calib_d3gdt3,
    Ti1_gas_species_t_calib_d3gdt2dp,
    Ti1_gas_species_t_calib_d3gdtdp2,
    Ti1_gas_species_t_calib_d3gdp3
  },

};
static int nc = (sizeof endmember / sizeof(struct _endmembers));

static const double R=8.3143;


static char *identifier = "Mon Dec  2 10:27:05 2019";
static double T_r = 298.15;
static double P_r = 1.0;


#include "gas_soln_t_calc.h"
#include "gas_soln_t_calib.h"

const char *IdealGas_t_gas_soln_t_calib_identifier(void) {
    return identifier;
}

const char *IdealGas_t_gas_soln_t_calib_name(void) {
    return "IdealGas_t";
}

char *IdealGas_t_gas_soln_t_calib_formula(double T, double P, double n[9]) {
    double sum, elm[9];
    const double *end0 = (*endmember[0].elements)();
    const double *end1 = (*endmember[1].elements)();
    const double *end2 = (*endmember[2].elements)();
    const double *end3 = (*endmember[3].elements)();
    const double *end4 = (*endmember[4].elements)();
    const double *end5 = (*endmember[5].elements)();
    const double *end6 = (*endmember[6].elements)();
    const double *end7 = (*endmember[7].elements)();
    const double *end8 = (*endmember[8].elements)();
    int i;
    const char *fmt = "H%5.3fHe%5.3fO%5.3fNe%5.3fMg%5.3fAl%5.3fAr%5.3fCa%5.3fTi%5.3f";
    char *result = (char *) malloc(62*sizeof(char));
    for (i=0, sum=0.0; i<nc; i++) sum += n[i];
    if (sum == 0.0) return result;
    elm[0] = end0[1]*n[0]/sum + end1[1]*n[1]/sum + end2[1]*n[2]/sum + end3[1]*n[3]/sum + end4[1]*n[4]/sum + end5[1]*n[5]/sum + end6[1]*n[6]/sum + end7[1]*n[7]/sum + end8[1]*n[8]/sum;
    elm[1] = end0[2]*n[0]/sum + end1[2]*n[1]/sum + end2[2]*n[2]/sum + end3[2]*n[3]/sum + end4[2]*n[4]/sum + end5[2]*n[5]/sum + end6[2]*n[6]/sum + end7[2]*n[7]/sum + end8[2]*n[8]/sum;
    elm[2] = end0[8]*n[0]/sum + end1[8]*n[1]/sum + end2[8]*n[2]/sum + end3[8]*n[3]/sum + end4[8]*n[4]/sum + end5[8]*n[5]/sum + end6[8]*n[6]/sum + end7[8]*n[7]/sum + end8[8]*n[8]/sum;
    elm[3] = end0[10]*n[0]/sum + end1[10]*n[1]/sum + end2[10]*n[2]/sum + end3[10]*n[3]/sum + end4[10]*n[4]/sum + end5[10]*n[5]/sum + end6[10]*n[6]/sum + end7[10]*n[7]/sum + end8[10]*n[8]/sum;
    elm[4] = end0[12]*n[0]/sum + end1[12]*n[1]/sum + end2[12]*n[2]/sum + end3[12]*n[3]/sum + end4[12]*n[4]/sum + end5[12]*n[5]/sum + end6[12]*n[6]/sum + end7[12]*n[7]/sum + end8[12]*n[8]/sum;
    elm[5] = end0[13]*n[0]/sum + end1[13]*n[1]/sum + end2[13]*n[2]/sum + end3[13]*n[3]/sum + end4[13]*n[4]/sum + end5[13]*n[5]/sum + end6[13]*n[6]/sum + end7[13]*n[7]/sum + end8[13]*n[8]/sum;
    elm[6] = end0[18]*n[0]/sum + end1[18]*n[1]/sum + end2[18]*n[2]/sum + end3[18]*n[3]/sum + end4[18]*n[4]/sum + end5[18]*n[5]/sum + end6[18]*n[6]/sum + end7[18]*n[7]/sum + end8[18]*n[8]/sum;
    elm[7] = end0[20]*n[0]/sum + end1[20]*n[1]/sum + end2[20]*n[2]/sum + end3[20]*n[3]/sum + end4[20]*n[4]/sum + end5[20]*n[5]/sum + end6[20]*n[6]/sum + end7[20]*n[7]/sum + end8[20]*n[8]/sum;
    elm[8] = end0[22]*n[0]/sum + end1[22]*n[1]/sum + end2[22]*n[2]/sum + end3[22]*n[3]/sum + end4[22]*n[4]/sum + end5[22]*n[5]/sum + end6[22]*n[6]/sum + end7[22]*n[7]/sum + end8[22]*n[8]/sum;
    sprintf(result, fmt, elm[0], elm[1], elm[2], elm[3], elm[4], elm[5], elm[6], elm[7], elm[8]);
    return result;

}

double *IdealGas_t_gas_soln_t_calib_conv_elm_to_moles(double *e) {
    double *n = (double *) malloc(9*sizeof(double));
    n[0]=e[1];
    n[1]=e[2];
    n[2]=e[8];
    n[3]=e[10];
    n[4]=e[12];
    n[5]=e[13];
    n[6]=e[18];
    n[7]=e[20];
    n[8]=e[22];
    return n;

}

int IdealGas_t_gas_soln_t_calib_test_moles(double *n) {
    int result = 1;
    result &= (n[0] >= 0.0);
    result &= (n[1] >= 0.0);
    result &= (n[2] >= 0.0);
    result &= (n[3] >= 0.0);
    result &= (n[4] >= 0.0);
    result &= (n[5] >= 0.0);
    result &= (n[6] >= 0.0);
    result &= (n[7] >= 0.0);
    result &= (n[8] >= 0.0);
    return result;

}

const char *IdealGas_t_gas_soln_t_calib_endmember_name(int index) {
    return (*endmember[index].name)();
}

const char *IdealGas_t_gas_soln_t_calib_endmember_formula(int index) {
    return (*endmember[index].formula)();
}

const double IdealGas_t_gas_soln_t_calib_endmember_mw(int index) {
    return (*endmember[index].mw)();
}

const double *IdealGas_t_gas_soln_t_calib_endmember_elements(int index) {
    return (*endmember[index].elements)();
}

double IdealGas_t_gas_soln_t_calib_endmember_mu0(int index, double t, double p) {
    return (*endmember[index].mu0)(t, p);
}

double IdealGas_t_gas_soln_t_calib_endmember_dmu0dT(int index, double t, double p) {
    return (*endmember[index].dmu0dT)(t, p);
}

double IdealGas_t_gas_soln_t_calib_endmember_dmu0dP(int index, double t, double p) {
    return (*endmember[index].dmu0dP)(t, p);
}

double IdealGas_t_gas_soln_t_calib_endmember_d2mu0dT2(int index, double t, double p) {
    return (*endmember[index].d2mu0dT2)(t, p);
}

double IdealGas_t_gas_soln_t_calib_endmember_d2mu0dTdP(int index, double t, double p) {
    return (*endmember[index].d2mu0dTdP)(t, p);
}

double IdealGas_t_gas_soln_t_calib_endmember_d2mu0dP2(int index, double t, double p) {
    return (*endmember[index].d2mu0dP2)(t, p);
}

double IdealGas_t_gas_soln_t_calib_endmember_d3mu0dT3(int index, double t, double p) {
    return (*endmember[index].d3mu0dT3)(t, p);
}

double IdealGas_t_gas_soln_t_calib_endmember_d3mu0dT2dP(int index, double t, double p) {
    return (*endmember[index].d3mu0dT2dP)(t, p);
}

double IdealGas_t_gas_soln_t_calib_endmember_d3mu0dTdP2(int index, double t, double p) {
    return (*endmember[index].d3mu0dTdP2)(t, p);
}

double IdealGas_t_gas_soln_t_calib_endmember_d3mu0dP3(int index, double t, double p) {
    return (*endmember[index].d3mu0dP3)(t, p);
}

double IdealGas_t_gas_soln_t_calib_g(double T, double P, double n[9]) {
    return gas_soln_t_g(T, P, n);
}

void IdealGas_t_gas_soln_t_calib_dgdn(double T, double P, double n[9], double result[9]) {
    gas_soln_t_dgdn(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d2gdn2(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d2gdn2(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d3gdn3(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d3gdn3(T, P, n, result);
}

double IdealGas_t_gas_soln_t_calib_dgdt(double T, double P, double n[9]) {
    return gas_soln_t_dgdt(T, P, n);
}

void IdealGas_t_gas_soln_t_calib_d2gdndt(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d2gdndt(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d3gdn2dt(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d3gdn2dt(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d4gdn3dt(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d4gdn3dt(T, P, n, result);
}

double IdealGas_t_gas_soln_t_calib_dgdp(double T, double P, double n[9]) {
    return gas_soln_t_dgdp(T, P, n);
}

void IdealGas_t_gas_soln_t_calib_d2gdndp(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d2gdndp(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d3gdn2dp(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d3gdn2dp(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d4gdn3dp(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d4gdn3dp(T, P, n, result);
}

double IdealGas_t_gas_soln_t_calib_d2gdt2(double T, double P, double n[9]) {
    return gas_soln_t_d2gdt2(T, P, n);
}

void IdealGas_t_gas_soln_t_calib_d3gdndt2(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d3gdndt2(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d4gdn2dt2(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d4gdn2dt2(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d5gdn3dt2(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d5gdn3dt2(T, P, n, result);
}

double IdealGas_t_gas_soln_t_calib_d2gdtdp(double T, double P, double n[9]) {
    return gas_soln_t_d2gdtdp(T, P, n);
}

void IdealGas_t_gas_soln_t_calib_d3gdndtdp(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d3gdndtdp(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d4gdn2dtdp(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d4gdn2dtdp(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d5gdn3dtdp(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d5gdn3dtdp(T, P, n, result);
}

double IdealGas_t_gas_soln_t_calib_d2gdp2(double T, double P, double n[9]) {
    return gas_soln_t_d2gdp2(T, P, n);
}

void IdealGas_t_gas_soln_t_calib_d3gdndp2(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d3gdndp2(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d4gdn2dp2(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d4gdn2dp2(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d5gdn3dp2(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d5gdn3dp2(T, P, n, result);
}

double IdealGas_t_gas_soln_t_calib_d3gdt3(double T, double P, double n[9]) {
    return gas_soln_t_d3gdt3(T, P, n);
}

void IdealGas_t_gas_soln_t_calib_d4gdndt3(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d4gdndt3(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d5gdn2dt3(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d5gdn2dt3(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d6gdn3dt3(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d6gdn3dt3(T, P, n, result);
}

double IdealGas_t_gas_soln_t_calib_d3gdt2dp(double T, double P, double n[9]) {
    return gas_soln_t_d3gdt2dp(T, P, n);
}

void IdealGas_t_gas_soln_t_calib_d4gdndt2dp(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d4gdndt2dp(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d5gdn2dt2dp(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d5gdn2dt2dp(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d6gdn3dt2dp(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d6gdn3dt2dp(T, P, n, result);
}

double IdealGas_t_gas_soln_t_calib_d3gdtdp2(double T, double P, double n[9]) {
    return gas_soln_t_d3gdtdp2(T, P, n);
}

void IdealGas_t_gas_soln_t_calib_d4gdndtdp2(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d4gdndtdp2(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d5gdn2dtdp2(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d5gdn2dtdp2(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d6gdn3dtdp2(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d6gdn3dtdp2(T, P, n, result);
}

double IdealGas_t_gas_soln_t_calib_d3gdp3(double T, double P, double n[9]) {
    return gas_soln_t_d3gdp3(T, P, n);
}

void IdealGas_t_gas_soln_t_calib_d4gdndp3(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d4gdndp3(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d5gdn2dp3(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d5gdn2dp3(T, P, n, result);
}

void IdealGas_t_gas_soln_t_calib_d6gdn3dp3(double T, double P, double n[9], double result[9]) {
    gas_soln_t_d6gdn3dp3(T, P, n, result);
}

double IdealGas_t_gas_soln_t_calib_s(double T, double P, double n[9]) {
    return gas_soln_t_s(T, P, n);
}

double IdealGas_t_gas_soln_t_calib_v(double T, double P, double n[9]) {
    return gas_soln_t_v(T, P, n);
}

double IdealGas_t_gas_soln_t_calib_cv(double T, double P, double n[9]) {
    return gas_soln_t_cv(T, P, n);
}

double IdealGas_t_gas_soln_t_calib_cp(double T, double P, double n[9]) {
    return gas_soln_t_cp(T, P, n);
}

double IdealGas_t_gas_soln_t_calib_dcpdt(double T, double P, double n[9]) {
    return gas_soln_t_dcpdt(T, P, n);
}

double IdealGas_t_gas_soln_t_calib_alpha(double T, double P, double n[9]) {
    return gas_soln_t_alpha(T, P, n);
}

double IdealGas_t_gas_soln_t_calib_beta(double T, double P, double n[9]) {
    return gas_soln_t_beta(T, P, n);
}

double IdealGas_t_gas_soln_t_calib_K(double T, double P, double n[9]) {
    return gas_soln_t_K(T, P, n);
}

double IdealGas_t_gas_soln_t_calib_Kp(double T, double P, double n[9]) {
    return gas_soln_t_Kp(T, P, n);
}

int IdealGas_t_gas_soln_t_get_param_number(void) {
    return gas_soln_t_get_param_number();
}

const char **IdealGas_t_gas_soln_t_get_param_names(void) {
    return gas_soln_t_get_param_names();
}

const char **IdealGas_t_gas_soln_t_get_param_units(void) {
    return gas_soln_t_get_param_units();
}

void IdealGas_t_gas_soln_t_get_param_values(double **values) {
    gas_soln_t_get_param_values(values);
}

int IdealGas_t_gas_soln_t_set_param_values(double *values) {
    return gas_soln_t_set_param_values(values);
}

double IdealGas_t_gas_soln_t_get_param_value(int index) {
    return gas_soln_t_get_param_value(index);
}

int IdealGas_t_gas_soln_t_set_param_value(int index, double value) {
    return gas_soln_t_set_param_value(index, value);
}

double IdealGas_t_gas_soln_t_dparam_g(double T, double P, double n[9], int index) {
    return gas_soln_t_dparam_g(T, P, n, index);
}

double IdealGas_t_gas_soln_t_dparam_dgdt(double T, double P, double n[9], int index) {
    return gas_soln_t_dparam_dgdt(T, P, n, index);
}

double IdealGas_t_gas_soln_t_dparam_dgdp(double T, double P, double n[9], int index) {
    return gas_soln_t_dparam_dgdp(T, P, n, index);
}

double IdealGas_t_gas_soln_t_dparam_d2gdt2(double T, double P, double n[9], int index) {
    return gas_soln_t_dparam_d2gdt2(T, P, n, index);
}

double IdealGas_t_gas_soln_t_dparam_d2gdtdp(double T, double P, double n[9], int index) {
    return gas_soln_t_dparam_d2gdtdp(T, P, n, index);
}

double IdealGas_t_gas_soln_t_dparam_d2gdp2(double T, double P, double n[9], int index) {
    return gas_soln_t_dparam_d2gdp2(T, P, n, index);
}

double IdealGas_t_gas_soln_t_dparam_d3gdt3(double T, double P, double n[9], int index) {
    return gas_soln_t_dparam_d3gdt3(T, P, n, index);
}

double IdealGas_t_gas_soln_t_dparam_d3gdt2dp(double T, double P, double n[9], int index) {
    return gas_soln_t_dparam_d3gdt2dp(T, P, n, index);
}

double IdealGas_t_gas_soln_t_dparam_d3gdtdp2(double T, double P, double n[9], int index) {
    return gas_soln_t_dparam_d3gdtdp2(T, P, n, index);
}

double IdealGas_t_gas_soln_t_dparam_d3gdp3(double T, double P, double n[9], int index) {
    return gas_soln_t_dparam_d3gdp3(T, P, n, index);
}

void IdealGas_t_gas_soln_t_dparam_dgdn(double T, double P, double n[9], int index, double result[9]) {
    gas_soln_t_dparam_dgdn(T, P, n, index, result);
}

