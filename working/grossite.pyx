import numpy as np
cimport numpy as cnp # cimport gives us access to NumPy's C API

# here we just replicate the function signature from the header
cdef extern from "Grossite_grossite_calc.h":
    const char *Grossite_grossite_identifier();
    const char *Grossite_grossite_name();
    const char *Grossite_grossite_formula();
    const double Grossite_grossite_mw();
    const double *Grossite_grossite_elements();
    double Grossite_grossite_g(double t, double p)
    double Grossite_grossite_dgdt(double t, double p)
    double Grossite_grossite_dgdp(double t, double p)
    double Grossite_grossite_d2gdt2(double t, double p)
    double Grossite_grossite_d2gdtdp(double t, double p)
    double Grossite_grossite_d2gdp2(double t, double p)
    double Grossite_grossite_d3gdt3(double t, double p)
    double Grossite_grossite_d3gdt2dp(double t, double p)
    double Grossite_grossite_d3gdtdp2(double t, double p)
    double Grossite_grossite_d3gdp3(double t, double p)
    double Grossite_grossite_s(double t, double p)
    double Grossite_grossite_v(double t, double p)
    double Grossite_grossite_cv(double t, double p)
    double Grossite_grossite_cp(double t, double p)
    double Grossite_grossite_dcpdt(double t, double p)
    double Grossite_grossite_alpha(double t, double p)
    double Grossite_grossite_beta(double t, double p)
    double Grossite_grossite_K(double t, double p)
    double Grossite_grossite_Kp(double t, double p)

# here is the "wrapper" signature
def cy_Grossite_grossite_identifier():
    result = <bytes> Grossite_grossite_identifier()
    return result.decode('UTF-8')
def cy_Grossite_grossite_name():
    result = <bytes> Grossite_grossite_name()
    return result.decode('UTF-8')
def cy_Grossite_grossite_formula():
    result = <bytes> Grossite_grossite_formula()
    return result.decode('UTF-8')
def cy_Grossite_grossite_mw():
    result = Grossite_grossite_mw()
    return result
def cy_Grossite_grossite_elements():
    cdef const double *e = Grossite_grossite_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Grossite_grossite_g(double t, double p):
    result = Grossite_grossite_g(<double> t, <double> p)
    return result
def cy_Grossite_grossite_dgdt(double t, double p):
    result = Grossite_grossite_dgdt(<double> t, <double> p)
    return result
def cy_Grossite_grossite_dgdp(double t, double p):
    result = Grossite_grossite_dgdp(<double> t, <double> p)
    return result
def cy_Grossite_grossite_d2gdt2(double t, double p):
    result = Grossite_grossite_d2gdt2(<double> t, <double> p)
    return result
def cy_Grossite_grossite_d2gdtdp(double t, double p):
    result = Grossite_grossite_d2gdtdp(<double> t, <double> p)
    return result
def cy_Grossite_grossite_d2gdp2(double t, double p):
    result = Grossite_grossite_d2gdp2(<double> t, <double> p)
    return result
def cy_Grossite_grossite_d3gdt3(double t, double p):
    result = Grossite_grossite_d3gdt3(<double> t, <double> p)
    return result
def cy_Grossite_grossite_d3gdt2dp(double t, double p):
    result = Grossite_grossite_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Grossite_grossite_d3gdtdp2(double t, double p):
    result = Grossite_grossite_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Grossite_grossite_d3gdp3(double t, double p):
    result = Grossite_grossite_d3gdp3(<double> t, <double> p)
    return result
def cy_Grossite_grossite_s(double t, double p):
    result = Grossite_grossite_s(<double> t, <double> p)
    return result
def cy_Grossite_grossite_v(double t, double p):
    result = Grossite_grossite_v(<double> t, <double> p)
    return result
def cy_Grossite_grossite_cv(double t, double p):
    result = Grossite_grossite_cv(<double> t, <double> p)
    return result
def cy_Grossite_grossite_cp(double t, double p):
    result = Grossite_grossite_cp(<double> t, <double> p)
    return result
def cy_Grossite_grossite_dcpdt(double t, double p):
    result = Grossite_grossite_dcpdt(<double> t, <double> p)
    return result
def cy_Grossite_grossite_alpha(double t, double p):
    result = Grossite_grossite_alpha(<double> t, <double> p)
    return result
def cy_Grossite_grossite_beta(double t, double p):
    result = Grossite_grossite_beta(<double> t, <double> p)
    return result
def cy_Grossite_grossite_K(double t, double p):
    result = Grossite_grossite_K(<double> t, <double> p)
    return result
def cy_Grossite_grossite_Kp(double t, double p):
    result = Grossite_grossite_Kp(<double> t, <double> p)
    return result
