# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Al1_gas_species_t_calib.h":
    const char *Al1_gas_species_t_calib_identifier();
    const char *Al1_gas_species_t_calib_name();
    const char *Al1_gas_species_t_calib_formula();
    const double Al1_gas_species_t_calib_mw();
    const double *Al1_gas_species_t_calib_elements();
    double Al1_gas_species_t_calib_g(double t, double p);
    double Al1_gas_species_t_calib_dgdt(double t, double p);
    double Al1_gas_species_t_calib_dgdp(double t, double p);
    double Al1_gas_species_t_calib_d2gdt2(double t, double p);
    double Al1_gas_species_t_calib_d2gdtdp(double t, double p);
    double Al1_gas_species_t_calib_d2gdp2(double t, double p);
    double Al1_gas_species_t_calib_d3gdt3(double t, double p);
    double Al1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Al1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Al1_gas_species_t_calib_d3gdp3(double t, double p);
    double Al1_gas_species_t_calib_s(double t, double p);
    double Al1_gas_species_t_calib_v(double t, double p);
    double Al1_gas_species_t_calib_cv(double t, double p);
    double Al1_gas_species_t_calib_cp(double t, double p);
    double Al1_gas_species_t_calib_dcpdt(double t, double p);
    double Al1_gas_species_t_calib_alpha(double t, double p);
    double Al1_gas_species_t_calib_beta(double t, double p);
    double Al1_gas_species_t_calib_K(double t, double p);
    double Al1_gas_species_t_calib_Kp(double t, double p);
    int Al1_gas_species_t_get_param_number();
    const char **Al1_gas_species_t_get_param_names();
    const char **Al1_gas_species_t_get_param_units();
    void Al1_gas_species_t_get_param_values(double **values);
    int Al1_gas_species_t_set_param_values(double *values);
    double Al1_gas_species_t_get_param_value(int index);
    int Al1_gas_species_t_set_param_value(int index, double value);
    double Al1_gas_species_t_dparam_g(double t, double p, int index);
    double Al1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Al1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Al1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Al1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Al1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Al1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Al1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Al1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Al1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Al1_gas_species_t_calib_identifier():
    result = <bytes> Al1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Al1_gas_species_t_calib_name():
    result = <bytes> Al1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Al1_gas_species_t_calib_formula():
    result = <bytes> Al1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Al1_gas_species_t_calib_mw():
    result = Al1_gas_species_t_calib_mw()
    return result
def cy_Al1_gas_species_t_calib_elements():
    cdef const double *e = Al1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Al1_gas_species_t_calib_g(double t, double p):
    result = Al1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_dgdt(double t, double p):
    result = Al1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_dgdp(double t, double p):
    result = Al1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_d2gdt2(double t, double p):
    result = Al1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Al1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_d2gdp2(double t, double p):
    result = Al1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_d3gdt3(double t, double p):
    result = Al1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Al1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Al1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_d3gdp3(double t, double p):
    result = Al1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_s(double t, double p):
    result = Al1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_v(double t, double p):
    result = Al1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_cv(double t, double p):
    result = Al1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_cp(double t, double p):
    result = Al1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_dcpdt(double t, double p):
    result = Al1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_alpha(double t, double p):
    result = Al1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_beta(double t, double p):
    result = Al1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_K(double t, double p):
    result = Al1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_calib_Kp(double t, double p):
    result = Al1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Al1_gas_species_t_get_param_number():
    result = Al1_gas_species_t_get_param_number()
    return result
def cy_Al1_gas_species_t_get_param_names():
    cdef const char **names = Al1_gas_species_t_get_param_names()
    n = Al1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al1_gas_species_t_get_param_units():
    cdef const char **units = Al1_gas_species_t_get_param_units()
    n = Al1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al1_gas_species_t_get_param_values():
    n = Al1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Al1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Al1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Al1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Al1_gas_species_t_get_param_value(int index):
    result = Al1_gas_species_t_get_param_value(<int> index)
    return result
def cy_Al1_gas_species_t_set_param_value(int index, double value):
    result = Al1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Al1_gas_species_t_dparam_g(double t, double p, int index):
    result = Al1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Al1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Al1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Al1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Al1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Al1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Al1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Al1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Al1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Al1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Al1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Al1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Al1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Al1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Al1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Al1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Al1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Ca1H2O2_gas_species_t_calib.h":
    const char *Ca1H2O2_gas_species_t_calib_identifier();
    const char *Ca1H2O2_gas_species_t_calib_name();
    const char *Ca1H2O2_gas_species_t_calib_formula();
    const double Ca1H2O2_gas_species_t_calib_mw();
    const double *Ca1H2O2_gas_species_t_calib_elements();
    double Ca1H2O2_gas_species_t_calib_g(double t, double p);
    double Ca1H2O2_gas_species_t_calib_dgdt(double t, double p);
    double Ca1H2O2_gas_species_t_calib_dgdp(double t, double p);
    double Ca1H2O2_gas_species_t_calib_d2gdt2(double t, double p);
    double Ca1H2O2_gas_species_t_calib_d2gdtdp(double t, double p);
    double Ca1H2O2_gas_species_t_calib_d2gdp2(double t, double p);
    double Ca1H2O2_gas_species_t_calib_d3gdt3(double t, double p);
    double Ca1H2O2_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Ca1H2O2_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Ca1H2O2_gas_species_t_calib_d3gdp3(double t, double p);
    double Ca1H2O2_gas_species_t_calib_s(double t, double p);
    double Ca1H2O2_gas_species_t_calib_v(double t, double p);
    double Ca1H2O2_gas_species_t_calib_cv(double t, double p);
    double Ca1H2O2_gas_species_t_calib_cp(double t, double p);
    double Ca1H2O2_gas_species_t_calib_dcpdt(double t, double p);
    double Ca1H2O2_gas_species_t_calib_alpha(double t, double p);
    double Ca1H2O2_gas_species_t_calib_beta(double t, double p);
    double Ca1H2O2_gas_species_t_calib_K(double t, double p);
    double Ca1H2O2_gas_species_t_calib_Kp(double t, double p);
    int Ca1H2O2_gas_species_t_get_param_number();
    const char **Ca1H2O2_gas_species_t_get_param_names();
    const char **Ca1H2O2_gas_species_t_get_param_units();
    void Ca1H2O2_gas_species_t_get_param_values(double **values);
    int Ca1H2O2_gas_species_t_set_param_values(double *values);
    double Ca1H2O2_gas_species_t_get_param_value(int index);
    int Ca1H2O2_gas_species_t_set_param_value(int index, double value);
    double Ca1H2O2_gas_species_t_dparam_g(double t, double p, int index);
    double Ca1H2O2_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Ca1H2O2_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Ca1H2O2_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Ca1H2O2_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Ca1H2O2_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Ca1H2O2_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Ca1H2O2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Ca1H2O2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Ca1H2O2_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Ca1H2O2_gas_species_t_calib_identifier():
    result = <bytes> Ca1H2O2_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Ca1H2O2_gas_species_t_calib_name():
    result = <bytes> Ca1H2O2_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Ca1H2O2_gas_species_t_calib_formula():
    result = <bytes> Ca1H2O2_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Ca1H2O2_gas_species_t_calib_mw():
    result = Ca1H2O2_gas_species_t_calib_mw()
    return result
def cy_Ca1H2O2_gas_species_t_calib_elements():
    cdef const double *e = Ca1H2O2_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Ca1H2O2_gas_species_t_calib_g(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_dgdt(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_dgdp(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_d2gdt2(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_d2gdp2(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_d3gdt3(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_d3gdp3(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_s(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_v(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_cv(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_cp(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_dcpdt(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_alpha(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_beta(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_K(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_calib_Kp(double t, double p):
    result = Ca1H2O2_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Ca1H2O2_gas_species_t_get_param_number():
    result = Ca1H2O2_gas_species_t_get_param_number()
    return result
def cy_Ca1H2O2_gas_species_t_get_param_names():
    cdef const char **names = Ca1H2O2_gas_species_t_get_param_names()
    n = Ca1H2O2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Ca1H2O2_gas_species_t_get_param_units():
    cdef const char **units = Ca1H2O2_gas_species_t_get_param_units()
    n = Ca1H2O2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Ca1H2O2_gas_species_t_get_param_values():
    n = Ca1H2O2_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Ca1H2O2_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Ca1H2O2_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Ca1H2O2_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Ca1H2O2_gas_species_t_get_param_value(int index):
    result = Ca1H2O2_gas_species_t_get_param_value(<int> index)
    return result
def cy_Ca1H2O2_gas_species_t_set_param_value(int index, double value):
    result = Ca1H2O2_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Ca1H2O2_gas_species_t_dparam_g(double t, double p, int index):
    result = Ca1H2O2_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H2O2_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Ca1H2O2_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H2O2_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Ca1H2O2_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H2O2_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Ca1H2O2_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H2O2_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Ca1H2O2_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H2O2_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Ca1H2O2_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H2O2_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Ca1H2O2_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H2O2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Ca1H2O2_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H2O2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Ca1H2O2_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H2O2_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Ca1H2O2_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "H2_gas_species_t_calib.h":
    const char *H2_gas_species_t_calib_identifier();
    const char *H2_gas_species_t_calib_name();
    const char *H2_gas_species_t_calib_formula();
    const double H2_gas_species_t_calib_mw();
    const double *H2_gas_species_t_calib_elements();
    double H2_gas_species_t_calib_g(double t, double p);
    double H2_gas_species_t_calib_dgdt(double t, double p);
    double H2_gas_species_t_calib_dgdp(double t, double p);
    double H2_gas_species_t_calib_d2gdt2(double t, double p);
    double H2_gas_species_t_calib_d2gdtdp(double t, double p);
    double H2_gas_species_t_calib_d2gdp2(double t, double p);
    double H2_gas_species_t_calib_d3gdt3(double t, double p);
    double H2_gas_species_t_calib_d3gdt2dp(double t, double p);
    double H2_gas_species_t_calib_d3gdtdp2(double t, double p);
    double H2_gas_species_t_calib_d3gdp3(double t, double p);
    double H2_gas_species_t_calib_s(double t, double p);
    double H2_gas_species_t_calib_v(double t, double p);
    double H2_gas_species_t_calib_cv(double t, double p);
    double H2_gas_species_t_calib_cp(double t, double p);
    double H2_gas_species_t_calib_dcpdt(double t, double p);
    double H2_gas_species_t_calib_alpha(double t, double p);
    double H2_gas_species_t_calib_beta(double t, double p);
    double H2_gas_species_t_calib_K(double t, double p);
    double H2_gas_species_t_calib_Kp(double t, double p);
    int H2_gas_species_t_get_param_number();
    const char **H2_gas_species_t_get_param_names();
    const char **H2_gas_species_t_get_param_units();
    void H2_gas_species_t_get_param_values(double **values);
    int H2_gas_species_t_set_param_values(double *values);
    double H2_gas_species_t_get_param_value(int index);
    int H2_gas_species_t_set_param_value(int index, double value);
    double H2_gas_species_t_dparam_g(double t, double p, int index);
    double H2_gas_species_t_dparam_dgdt(double t, double p, int index);
    double H2_gas_species_t_dparam_dgdp(double t, double p, int index);
    double H2_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double H2_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double H2_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double H2_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double H2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double H2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double H2_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_H2_gas_species_t_calib_identifier():
    result = <bytes> H2_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_H2_gas_species_t_calib_name():
    result = <bytes> H2_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_H2_gas_species_t_calib_formula():
    result = <bytes> H2_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_H2_gas_species_t_calib_mw():
    result = H2_gas_species_t_calib_mw()
    return result
def cy_H2_gas_species_t_calib_elements():
    cdef const double *e = H2_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_H2_gas_species_t_calib_g(double t, double p):
    result = H2_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_dgdt(double t, double p):
    result = H2_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_dgdp(double t, double p):
    result = H2_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_d2gdt2(double t, double p):
    result = H2_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_d2gdtdp(double t, double p):
    result = H2_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_d2gdp2(double t, double p):
    result = H2_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_d3gdt3(double t, double p):
    result = H2_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = H2_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = H2_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_d3gdp3(double t, double p):
    result = H2_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_s(double t, double p):
    result = H2_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_v(double t, double p):
    result = H2_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_cv(double t, double p):
    result = H2_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_cp(double t, double p):
    result = H2_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_dcpdt(double t, double p):
    result = H2_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_alpha(double t, double p):
    result = H2_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_beta(double t, double p):
    result = H2_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_K(double t, double p):
    result = H2_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_calib_Kp(double t, double p):
    result = H2_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_H2_gas_species_t_get_param_number():
    result = H2_gas_species_t_get_param_number()
    return result
def cy_H2_gas_species_t_get_param_names():
    cdef const char **names = H2_gas_species_t_get_param_names()
    n = H2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H2_gas_species_t_get_param_units():
    cdef const char **units = H2_gas_species_t_get_param_units()
    n = H2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H2_gas_species_t_get_param_values():
    n = H2_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    H2_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_H2_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = H2_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_H2_gas_species_t_get_param_value(int index):
    result = H2_gas_species_t_get_param_value(<int> index)
    return result
def cy_H2_gas_species_t_set_param_value(int index, double value):
    result = H2_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_H2_gas_species_t_dparam_g(double t, double p, int index):
    result = H2_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_H2_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = H2_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_H2_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = H2_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_H2_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = H2_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_H2_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = H2_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_H2_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = H2_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_H2_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = H2_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_H2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = H2_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_H2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = H2_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_H2_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = H2_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Al1H1O1_058_gas_species_t_calib.h":
    const char *Al1H1O1_058_gas_species_t_calib_identifier();
    const char *Al1H1O1_058_gas_species_t_calib_name();
    const char *Al1H1O1_058_gas_species_t_calib_formula();
    const double Al1H1O1_058_gas_species_t_calib_mw();
    const double *Al1H1O1_058_gas_species_t_calib_elements();
    double Al1H1O1_058_gas_species_t_calib_g(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_dgdt(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_dgdp(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_d2gdt2(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_d2gdtdp(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_d2gdp2(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_d3gdt3(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_d3gdp3(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_s(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_v(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_cv(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_cp(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_dcpdt(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_alpha(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_beta(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_K(double t, double p);
    double Al1H1O1_058_gas_species_t_calib_Kp(double t, double p);
    int Al1H1O1_058_gas_species_t_get_param_number();
    const char **Al1H1O1_058_gas_species_t_get_param_names();
    const char **Al1H1O1_058_gas_species_t_get_param_units();
    void Al1H1O1_058_gas_species_t_get_param_values(double **values);
    int Al1H1O1_058_gas_species_t_set_param_values(double *values);
    double Al1H1O1_058_gas_species_t_get_param_value(int index);
    int Al1H1O1_058_gas_species_t_set_param_value(int index, double value);
    double Al1H1O1_058_gas_species_t_dparam_g(double t, double p, int index);
    double Al1H1O1_058_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Al1H1O1_058_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Al1H1O1_058_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Al1H1O1_058_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Al1H1O1_058_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Al1H1O1_058_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Al1H1O1_058_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Al1H1O1_058_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Al1H1O1_058_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Al1H1O1_058_gas_species_t_calib_identifier():
    result = <bytes> Al1H1O1_058_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Al1H1O1_058_gas_species_t_calib_name():
    result = <bytes> Al1H1O1_058_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Al1H1O1_058_gas_species_t_calib_formula():
    result = <bytes> Al1H1O1_058_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Al1H1O1_058_gas_species_t_calib_mw():
    result = Al1H1O1_058_gas_species_t_calib_mw()
    return result
def cy_Al1H1O1_058_gas_species_t_calib_elements():
    cdef const double *e = Al1H1O1_058_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Al1H1O1_058_gas_species_t_calib_g(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_dgdt(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_dgdp(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_d2gdt2(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_d2gdp2(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_d3gdt3(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_d3gdp3(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_s(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_v(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_cv(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_cp(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_dcpdt(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_alpha(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_beta(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_K(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_calib_Kp(double t, double p):
    result = Al1H1O1_058_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Al1H1O1_058_gas_species_t_get_param_number():
    result = Al1H1O1_058_gas_species_t_get_param_number()
    return result
def cy_Al1H1O1_058_gas_species_t_get_param_names():
    cdef const char **names = Al1H1O1_058_gas_species_t_get_param_names()
    n = Al1H1O1_058_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al1H1O1_058_gas_species_t_get_param_units():
    cdef const char **units = Al1H1O1_058_gas_species_t_get_param_units()
    n = Al1H1O1_058_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al1H1O1_058_gas_species_t_get_param_values():
    n = Al1H1O1_058_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Al1H1O1_058_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Al1H1O1_058_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Al1H1O1_058_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Al1H1O1_058_gas_species_t_get_param_value(int index):
    result = Al1H1O1_058_gas_species_t_get_param_value(<int> index)
    return result
def cy_Al1H1O1_058_gas_species_t_set_param_value(int index, double value):
    result = Al1H1O1_058_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Al1H1O1_058_gas_species_t_dparam_g(double t, double p, int index):
    result = Al1H1O1_058_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_058_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Al1H1O1_058_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_058_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Al1H1O1_058_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_058_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Al1H1O1_058_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_058_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Al1H1O1_058_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_058_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Al1H1O1_058_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_058_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Al1H1O1_058_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_058_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Al1H1O1_058_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_058_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Al1H1O1_058_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_058_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Al1H1O1_058_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "H1Mg1_gas_species_t_calib.h":
    const char *H1Mg1_gas_species_t_calib_identifier();
    const char *H1Mg1_gas_species_t_calib_name();
    const char *H1Mg1_gas_species_t_calib_formula();
    const double H1Mg1_gas_species_t_calib_mw();
    const double *H1Mg1_gas_species_t_calib_elements();
    double H1Mg1_gas_species_t_calib_g(double t, double p);
    double H1Mg1_gas_species_t_calib_dgdt(double t, double p);
    double H1Mg1_gas_species_t_calib_dgdp(double t, double p);
    double H1Mg1_gas_species_t_calib_d2gdt2(double t, double p);
    double H1Mg1_gas_species_t_calib_d2gdtdp(double t, double p);
    double H1Mg1_gas_species_t_calib_d2gdp2(double t, double p);
    double H1Mg1_gas_species_t_calib_d3gdt3(double t, double p);
    double H1Mg1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double H1Mg1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double H1Mg1_gas_species_t_calib_d3gdp3(double t, double p);
    double H1Mg1_gas_species_t_calib_s(double t, double p);
    double H1Mg1_gas_species_t_calib_v(double t, double p);
    double H1Mg1_gas_species_t_calib_cv(double t, double p);
    double H1Mg1_gas_species_t_calib_cp(double t, double p);
    double H1Mg1_gas_species_t_calib_dcpdt(double t, double p);
    double H1Mg1_gas_species_t_calib_alpha(double t, double p);
    double H1Mg1_gas_species_t_calib_beta(double t, double p);
    double H1Mg1_gas_species_t_calib_K(double t, double p);
    double H1Mg1_gas_species_t_calib_Kp(double t, double p);
    int H1Mg1_gas_species_t_get_param_number();
    const char **H1Mg1_gas_species_t_get_param_names();
    const char **H1Mg1_gas_species_t_get_param_units();
    void H1Mg1_gas_species_t_get_param_values(double **values);
    int H1Mg1_gas_species_t_set_param_values(double *values);
    double H1Mg1_gas_species_t_get_param_value(int index);
    int H1Mg1_gas_species_t_set_param_value(int index, double value);
    double H1Mg1_gas_species_t_dparam_g(double t, double p, int index);
    double H1Mg1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double H1Mg1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double H1Mg1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double H1Mg1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double H1Mg1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double H1Mg1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double H1Mg1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double H1Mg1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double H1Mg1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_H1Mg1_gas_species_t_calib_identifier():
    result = <bytes> H1Mg1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_H1Mg1_gas_species_t_calib_name():
    result = <bytes> H1Mg1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_H1Mg1_gas_species_t_calib_formula():
    result = <bytes> H1Mg1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_H1Mg1_gas_species_t_calib_mw():
    result = H1Mg1_gas_species_t_calib_mw()
    return result
def cy_H1Mg1_gas_species_t_calib_elements():
    cdef const double *e = H1Mg1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_H1Mg1_gas_species_t_calib_g(double t, double p):
    result = H1Mg1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_dgdt(double t, double p):
    result = H1Mg1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_dgdp(double t, double p):
    result = H1Mg1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_d2gdt2(double t, double p):
    result = H1Mg1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = H1Mg1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_d2gdp2(double t, double p):
    result = H1Mg1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_d3gdt3(double t, double p):
    result = H1Mg1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = H1Mg1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = H1Mg1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_d3gdp3(double t, double p):
    result = H1Mg1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_s(double t, double p):
    result = H1Mg1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_v(double t, double p):
    result = H1Mg1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_cv(double t, double p):
    result = H1Mg1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_cp(double t, double p):
    result = H1Mg1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_dcpdt(double t, double p):
    result = H1Mg1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_alpha(double t, double p):
    result = H1Mg1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_beta(double t, double p):
    result = H1Mg1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_K(double t, double p):
    result = H1Mg1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_calib_Kp(double t, double p):
    result = H1Mg1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_H1Mg1_gas_species_t_get_param_number():
    result = H1Mg1_gas_species_t_get_param_number()
    return result
def cy_H1Mg1_gas_species_t_get_param_names():
    cdef const char **names = H1Mg1_gas_species_t_get_param_names()
    n = H1Mg1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H1Mg1_gas_species_t_get_param_units():
    cdef const char **units = H1Mg1_gas_species_t_get_param_units()
    n = H1Mg1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H1Mg1_gas_species_t_get_param_values():
    n = H1Mg1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    H1Mg1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_H1Mg1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = H1Mg1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_H1Mg1_gas_species_t_get_param_value(int index):
    result = H1Mg1_gas_species_t_get_param_value(<int> index)
    return result
def cy_H1Mg1_gas_species_t_set_param_value(int index, double value):
    result = H1Mg1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_H1Mg1_gas_species_t_dparam_g(double t, double p, int index):
    result = H1Mg1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = H1Mg1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = H1Mg1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = H1Mg1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = H1Mg1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = H1Mg1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = H1Mg1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = H1Mg1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = H1Mg1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = H1Mg1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "H2O1_gas_species_t_calib.h":
    const char *H2O1_gas_species_t_calib_identifier();
    const char *H2O1_gas_species_t_calib_name();
    const char *H2O1_gas_species_t_calib_formula();
    const double H2O1_gas_species_t_calib_mw();
    const double *H2O1_gas_species_t_calib_elements();
    double H2O1_gas_species_t_calib_g(double t, double p);
    double H2O1_gas_species_t_calib_dgdt(double t, double p);
    double H2O1_gas_species_t_calib_dgdp(double t, double p);
    double H2O1_gas_species_t_calib_d2gdt2(double t, double p);
    double H2O1_gas_species_t_calib_d2gdtdp(double t, double p);
    double H2O1_gas_species_t_calib_d2gdp2(double t, double p);
    double H2O1_gas_species_t_calib_d3gdt3(double t, double p);
    double H2O1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double H2O1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double H2O1_gas_species_t_calib_d3gdp3(double t, double p);
    double H2O1_gas_species_t_calib_s(double t, double p);
    double H2O1_gas_species_t_calib_v(double t, double p);
    double H2O1_gas_species_t_calib_cv(double t, double p);
    double H2O1_gas_species_t_calib_cp(double t, double p);
    double H2O1_gas_species_t_calib_dcpdt(double t, double p);
    double H2O1_gas_species_t_calib_alpha(double t, double p);
    double H2O1_gas_species_t_calib_beta(double t, double p);
    double H2O1_gas_species_t_calib_K(double t, double p);
    double H2O1_gas_species_t_calib_Kp(double t, double p);
    int H2O1_gas_species_t_get_param_number();
    const char **H2O1_gas_species_t_get_param_names();
    const char **H2O1_gas_species_t_get_param_units();
    void H2O1_gas_species_t_get_param_values(double **values);
    int H2O1_gas_species_t_set_param_values(double *values);
    double H2O1_gas_species_t_get_param_value(int index);
    int H2O1_gas_species_t_set_param_value(int index, double value);
    double H2O1_gas_species_t_dparam_g(double t, double p, int index);
    double H2O1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double H2O1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double H2O1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double H2O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double H2O1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double H2O1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double H2O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double H2O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double H2O1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_H2O1_gas_species_t_calib_identifier():
    result = <bytes> H2O1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_H2O1_gas_species_t_calib_name():
    result = <bytes> H2O1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_H2O1_gas_species_t_calib_formula():
    result = <bytes> H2O1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_H2O1_gas_species_t_calib_mw():
    result = H2O1_gas_species_t_calib_mw()
    return result
def cy_H2O1_gas_species_t_calib_elements():
    cdef const double *e = H2O1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_H2O1_gas_species_t_calib_g(double t, double p):
    result = H2O1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_dgdt(double t, double p):
    result = H2O1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_dgdp(double t, double p):
    result = H2O1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_d2gdt2(double t, double p):
    result = H2O1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = H2O1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_d2gdp2(double t, double p):
    result = H2O1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_d3gdt3(double t, double p):
    result = H2O1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = H2O1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = H2O1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_d3gdp3(double t, double p):
    result = H2O1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_s(double t, double p):
    result = H2O1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_v(double t, double p):
    result = H2O1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_cv(double t, double p):
    result = H2O1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_cp(double t, double p):
    result = H2O1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_dcpdt(double t, double p):
    result = H2O1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_alpha(double t, double p):
    result = H2O1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_beta(double t, double p):
    result = H2O1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_K(double t, double p):
    result = H2O1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_calib_Kp(double t, double p):
    result = H2O1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_H2O1_gas_species_t_get_param_number():
    result = H2O1_gas_species_t_get_param_number()
    return result
def cy_H2O1_gas_species_t_get_param_names():
    cdef const char **names = H2O1_gas_species_t_get_param_names()
    n = H2O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H2O1_gas_species_t_get_param_units():
    cdef const char **units = H2O1_gas_species_t_get_param_units()
    n = H2O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H2O1_gas_species_t_get_param_values():
    n = H2O1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    H2O1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_H2O1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = H2O1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_H2O1_gas_species_t_get_param_value(int index):
    result = H2O1_gas_species_t_get_param_value(<int> index)
    return result
def cy_H2O1_gas_species_t_set_param_value(int index, double value):
    result = H2O1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_H2O1_gas_species_t_dparam_g(double t, double p, int index):
    result = H2O1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_H2O1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = H2O1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_H2O1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = H2O1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_H2O1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = H2O1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_H2O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = H2O1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_H2O1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = H2O1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_H2O1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = H2O1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_H2O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = H2O1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_H2O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = H2O1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_H2O1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = H2O1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Ca1O1_gas_species_t_calib.h":
    const char *Ca1O1_gas_species_t_calib_identifier();
    const char *Ca1O1_gas_species_t_calib_name();
    const char *Ca1O1_gas_species_t_calib_formula();
    const double Ca1O1_gas_species_t_calib_mw();
    const double *Ca1O1_gas_species_t_calib_elements();
    double Ca1O1_gas_species_t_calib_g(double t, double p);
    double Ca1O1_gas_species_t_calib_dgdt(double t, double p);
    double Ca1O1_gas_species_t_calib_dgdp(double t, double p);
    double Ca1O1_gas_species_t_calib_d2gdt2(double t, double p);
    double Ca1O1_gas_species_t_calib_d2gdtdp(double t, double p);
    double Ca1O1_gas_species_t_calib_d2gdp2(double t, double p);
    double Ca1O1_gas_species_t_calib_d3gdt3(double t, double p);
    double Ca1O1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Ca1O1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Ca1O1_gas_species_t_calib_d3gdp3(double t, double p);
    double Ca1O1_gas_species_t_calib_s(double t, double p);
    double Ca1O1_gas_species_t_calib_v(double t, double p);
    double Ca1O1_gas_species_t_calib_cv(double t, double p);
    double Ca1O1_gas_species_t_calib_cp(double t, double p);
    double Ca1O1_gas_species_t_calib_dcpdt(double t, double p);
    double Ca1O1_gas_species_t_calib_alpha(double t, double p);
    double Ca1O1_gas_species_t_calib_beta(double t, double p);
    double Ca1O1_gas_species_t_calib_K(double t, double p);
    double Ca1O1_gas_species_t_calib_Kp(double t, double p);
    int Ca1O1_gas_species_t_get_param_number();
    const char **Ca1O1_gas_species_t_get_param_names();
    const char **Ca1O1_gas_species_t_get_param_units();
    void Ca1O1_gas_species_t_get_param_values(double **values);
    int Ca1O1_gas_species_t_set_param_values(double *values);
    double Ca1O1_gas_species_t_get_param_value(int index);
    int Ca1O1_gas_species_t_set_param_value(int index, double value);
    double Ca1O1_gas_species_t_dparam_g(double t, double p, int index);
    double Ca1O1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Ca1O1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Ca1O1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Ca1O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Ca1O1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Ca1O1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Ca1O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Ca1O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Ca1O1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Ca1O1_gas_species_t_calib_identifier():
    result = <bytes> Ca1O1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Ca1O1_gas_species_t_calib_name():
    result = <bytes> Ca1O1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Ca1O1_gas_species_t_calib_formula():
    result = <bytes> Ca1O1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Ca1O1_gas_species_t_calib_mw():
    result = Ca1O1_gas_species_t_calib_mw()
    return result
def cy_Ca1O1_gas_species_t_calib_elements():
    cdef const double *e = Ca1O1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Ca1O1_gas_species_t_calib_g(double t, double p):
    result = Ca1O1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_dgdt(double t, double p):
    result = Ca1O1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_dgdp(double t, double p):
    result = Ca1O1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_d2gdt2(double t, double p):
    result = Ca1O1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Ca1O1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_d2gdp2(double t, double p):
    result = Ca1O1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_d3gdt3(double t, double p):
    result = Ca1O1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Ca1O1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Ca1O1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_d3gdp3(double t, double p):
    result = Ca1O1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_s(double t, double p):
    result = Ca1O1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_v(double t, double p):
    result = Ca1O1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_cv(double t, double p):
    result = Ca1O1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_cp(double t, double p):
    result = Ca1O1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_dcpdt(double t, double p):
    result = Ca1O1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_alpha(double t, double p):
    result = Ca1O1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_beta(double t, double p):
    result = Ca1O1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_K(double t, double p):
    result = Ca1O1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_calib_Kp(double t, double p):
    result = Ca1O1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Ca1O1_gas_species_t_get_param_number():
    result = Ca1O1_gas_species_t_get_param_number()
    return result
def cy_Ca1O1_gas_species_t_get_param_names():
    cdef const char **names = Ca1O1_gas_species_t_get_param_names()
    n = Ca1O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Ca1O1_gas_species_t_get_param_units():
    cdef const char **units = Ca1O1_gas_species_t_get_param_units()
    n = Ca1O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Ca1O1_gas_species_t_get_param_values():
    n = Ca1O1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Ca1O1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Ca1O1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Ca1O1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Ca1O1_gas_species_t_get_param_value(int index):
    result = Ca1O1_gas_species_t_get_param_value(<int> index)
    return result
def cy_Ca1O1_gas_species_t_set_param_value(int index, double value):
    result = Ca1O1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Ca1O1_gas_species_t_dparam_g(double t, double p, int index):
    result = Ca1O1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Ca1O1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Ca1O1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Ca1O1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Ca1O1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Ca1O1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Ca1O1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Ca1O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Ca1O1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Ca1O1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Ca1O1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Ca1O1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Ca1O1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Ca1O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Ca1O1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Ca1O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Ca1O1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Ca1O1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Ca1O1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Ne1_gas_species_t_calib.h":
    const char *Ne1_gas_species_t_calib_identifier();
    const char *Ne1_gas_species_t_calib_name();
    const char *Ne1_gas_species_t_calib_formula();
    const double Ne1_gas_species_t_calib_mw();
    const double *Ne1_gas_species_t_calib_elements();
    double Ne1_gas_species_t_calib_g(double t, double p);
    double Ne1_gas_species_t_calib_dgdt(double t, double p);
    double Ne1_gas_species_t_calib_dgdp(double t, double p);
    double Ne1_gas_species_t_calib_d2gdt2(double t, double p);
    double Ne1_gas_species_t_calib_d2gdtdp(double t, double p);
    double Ne1_gas_species_t_calib_d2gdp2(double t, double p);
    double Ne1_gas_species_t_calib_d3gdt3(double t, double p);
    double Ne1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Ne1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Ne1_gas_species_t_calib_d3gdp3(double t, double p);
    double Ne1_gas_species_t_calib_s(double t, double p);
    double Ne1_gas_species_t_calib_v(double t, double p);
    double Ne1_gas_species_t_calib_cv(double t, double p);
    double Ne1_gas_species_t_calib_cp(double t, double p);
    double Ne1_gas_species_t_calib_dcpdt(double t, double p);
    double Ne1_gas_species_t_calib_alpha(double t, double p);
    double Ne1_gas_species_t_calib_beta(double t, double p);
    double Ne1_gas_species_t_calib_K(double t, double p);
    double Ne1_gas_species_t_calib_Kp(double t, double p);
    int Ne1_gas_species_t_get_param_number();
    const char **Ne1_gas_species_t_get_param_names();
    const char **Ne1_gas_species_t_get_param_units();
    void Ne1_gas_species_t_get_param_values(double **values);
    int Ne1_gas_species_t_set_param_values(double *values);
    double Ne1_gas_species_t_get_param_value(int index);
    int Ne1_gas_species_t_set_param_value(int index, double value);
    double Ne1_gas_species_t_dparam_g(double t, double p, int index);
    double Ne1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Ne1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Ne1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Ne1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Ne1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Ne1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Ne1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Ne1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Ne1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Ne1_gas_species_t_calib_identifier():
    result = <bytes> Ne1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Ne1_gas_species_t_calib_name():
    result = <bytes> Ne1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Ne1_gas_species_t_calib_formula():
    result = <bytes> Ne1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Ne1_gas_species_t_calib_mw():
    result = Ne1_gas_species_t_calib_mw()
    return result
def cy_Ne1_gas_species_t_calib_elements():
    cdef const double *e = Ne1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Ne1_gas_species_t_calib_g(double t, double p):
    result = Ne1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_dgdt(double t, double p):
    result = Ne1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_dgdp(double t, double p):
    result = Ne1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_d2gdt2(double t, double p):
    result = Ne1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Ne1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_d2gdp2(double t, double p):
    result = Ne1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_d3gdt3(double t, double p):
    result = Ne1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Ne1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Ne1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_d3gdp3(double t, double p):
    result = Ne1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_s(double t, double p):
    result = Ne1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_v(double t, double p):
    result = Ne1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_cv(double t, double p):
    result = Ne1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_cp(double t, double p):
    result = Ne1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_dcpdt(double t, double p):
    result = Ne1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_alpha(double t, double p):
    result = Ne1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_beta(double t, double p):
    result = Ne1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_K(double t, double p):
    result = Ne1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_calib_Kp(double t, double p):
    result = Ne1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Ne1_gas_species_t_get_param_number():
    result = Ne1_gas_species_t_get_param_number()
    return result
def cy_Ne1_gas_species_t_get_param_names():
    cdef const char **names = Ne1_gas_species_t_get_param_names()
    n = Ne1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Ne1_gas_species_t_get_param_units():
    cdef const char **units = Ne1_gas_species_t_get_param_units()
    n = Ne1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Ne1_gas_species_t_get_param_values():
    n = Ne1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Ne1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Ne1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Ne1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Ne1_gas_species_t_get_param_value(int index):
    result = Ne1_gas_species_t_get_param_value(<int> index)
    return result
def cy_Ne1_gas_species_t_set_param_value(int index, double value):
    result = Ne1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Ne1_gas_species_t_dparam_g(double t, double p, int index):
    result = Ne1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Ne1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Ne1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Ne1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Ne1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Ne1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Ne1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Ne1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Ne1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Ne1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Ne1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Ne1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Ne1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Ne1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Ne1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Ne1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Ne1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Ne1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Ne1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "O1Ti1_gas_species_t_calib.h":
    const char *O1Ti1_gas_species_t_calib_identifier();
    const char *O1Ti1_gas_species_t_calib_name();
    const char *O1Ti1_gas_species_t_calib_formula();
    const double O1Ti1_gas_species_t_calib_mw();
    const double *O1Ti1_gas_species_t_calib_elements();
    double O1Ti1_gas_species_t_calib_g(double t, double p);
    double O1Ti1_gas_species_t_calib_dgdt(double t, double p);
    double O1Ti1_gas_species_t_calib_dgdp(double t, double p);
    double O1Ti1_gas_species_t_calib_d2gdt2(double t, double p);
    double O1Ti1_gas_species_t_calib_d2gdtdp(double t, double p);
    double O1Ti1_gas_species_t_calib_d2gdp2(double t, double p);
    double O1Ti1_gas_species_t_calib_d3gdt3(double t, double p);
    double O1Ti1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double O1Ti1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double O1Ti1_gas_species_t_calib_d3gdp3(double t, double p);
    double O1Ti1_gas_species_t_calib_s(double t, double p);
    double O1Ti1_gas_species_t_calib_v(double t, double p);
    double O1Ti1_gas_species_t_calib_cv(double t, double p);
    double O1Ti1_gas_species_t_calib_cp(double t, double p);
    double O1Ti1_gas_species_t_calib_dcpdt(double t, double p);
    double O1Ti1_gas_species_t_calib_alpha(double t, double p);
    double O1Ti1_gas_species_t_calib_beta(double t, double p);
    double O1Ti1_gas_species_t_calib_K(double t, double p);
    double O1Ti1_gas_species_t_calib_Kp(double t, double p);
    int O1Ti1_gas_species_t_get_param_number();
    const char **O1Ti1_gas_species_t_get_param_names();
    const char **O1Ti1_gas_species_t_get_param_units();
    void O1Ti1_gas_species_t_get_param_values(double **values);
    int O1Ti1_gas_species_t_set_param_values(double *values);
    double O1Ti1_gas_species_t_get_param_value(int index);
    int O1Ti1_gas_species_t_set_param_value(int index, double value);
    double O1Ti1_gas_species_t_dparam_g(double t, double p, int index);
    double O1Ti1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double O1Ti1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double O1Ti1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double O1Ti1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double O1Ti1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double O1Ti1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double O1Ti1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double O1Ti1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double O1Ti1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_O1Ti1_gas_species_t_calib_identifier():
    result = <bytes> O1Ti1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_O1Ti1_gas_species_t_calib_name():
    result = <bytes> O1Ti1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_O1Ti1_gas_species_t_calib_formula():
    result = <bytes> O1Ti1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_O1Ti1_gas_species_t_calib_mw():
    result = O1Ti1_gas_species_t_calib_mw()
    return result
def cy_O1Ti1_gas_species_t_calib_elements():
    cdef const double *e = O1Ti1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_O1Ti1_gas_species_t_calib_g(double t, double p):
    result = O1Ti1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_dgdt(double t, double p):
    result = O1Ti1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_dgdp(double t, double p):
    result = O1Ti1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_d2gdt2(double t, double p):
    result = O1Ti1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = O1Ti1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_d2gdp2(double t, double p):
    result = O1Ti1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_d3gdt3(double t, double p):
    result = O1Ti1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = O1Ti1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = O1Ti1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_d3gdp3(double t, double p):
    result = O1Ti1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_s(double t, double p):
    result = O1Ti1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_v(double t, double p):
    result = O1Ti1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_cv(double t, double p):
    result = O1Ti1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_cp(double t, double p):
    result = O1Ti1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_dcpdt(double t, double p):
    result = O1Ti1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_alpha(double t, double p):
    result = O1Ti1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_beta(double t, double p):
    result = O1Ti1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_K(double t, double p):
    result = O1Ti1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_calib_Kp(double t, double p):
    result = O1Ti1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_O1Ti1_gas_species_t_get_param_number():
    result = O1Ti1_gas_species_t_get_param_number()
    return result
def cy_O1Ti1_gas_species_t_get_param_names():
    cdef const char **names = O1Ti1_gas_species_t_get_param_names()
    n = O1Ti1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_O1Ti1_gas_species_t_get_param_units():
    cdef const char **units = O1Ti1_gas_species_t_get_param_units()
    n = O1Ti1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_O1Ti1_gas_species_t_get_param_values():
    n = O1Ti1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    O1Ti1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_O1Ti1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = O1Ti1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_O1Ti1_gas_species_t_get_param_value(int index):
    result = O1Ti1_gas_species_t_get_param_value(<int> index)
    return result
def cy_O1Ti1_gas_species_t_set_param_value(int index, double value):
    result = O1Ti1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_O1Ti1_gas_species_t_dparam_g(double t, double p, int index):
    result = O1Ti1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_O1Ti1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = O1Ti1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_O1Ti1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = O1Ti1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_O1Ti1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = O1Ti1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_O1Ti1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = O1Ti1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_O1Ti1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = O1Ti1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_O1Ti1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = O1Ti1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_O1Ti1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = O1Ti1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_O1Ti1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = O1Ti1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_O1Ti1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = O1Ti1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "O2_gas_species_t_calib.h":
    const char *O2_gas_species_t_calib_identifier();
    const char *O2_gas_species_t_calib_name();
    const char *O2_gas_species_t_calib_formula();
    const double O2_gas_species_t_calib_mw();
    const double *O2_gas_species_t_calib_elements();
    double O2_gas_species_t_calib_g(double t, double p);
    double O2_gas_species_t_calib_dgdt(double t, double p);
    double O2_gas_species_t_calib_dgdp(double t, double p);
    double O2_gas_species_t_calib_d2gdt2(double t, double p);
    double O2_gas_species_t_calib_d2gdtdp(double t, double p);
    double O2_gas_species_t_calib_d2gdp2(double t, double p);
    double O2_gas_species_t_calib_d3gdt3(double t, double p);
    double O2_gas_species_t_calib_d3gdt2dp(double t, double p);
    double O2_gas_species_t_calib_d3gdtdp2(double t, double p);
    double O2_gas_species_t_calib_d3gdp3(double t, double p);
    double O2_gas_species_t_calib_s(double t, double p);
    double O2_gas_species_t_calib_v(double t, double p);
    double O2_gas_species_t_calib_cv(double t, double p);
    double O2_gas_species_t_calib_cp(double t, double p);
    double O2_gas_species_t_calib_dcpdt(double t, double p);
    double O2_gas_species_t_calib_alpha(double t, double p);
    double O2_gas_species_t_calib_beta(double t, double p);
    double O2_gas_species_t_calib_K(double t, double p);
    double O2_gas_species_t_calib_Kp(double t, double p);
    int O2_gas_species_t_get_param_number();
    const char **O2_gas_species_t_get_param_names();
    const char **O2_gas_species_t_get_param_units();
    void O2_gas_species_t_get_param_values(double **values);
    int O2_gas_species_t_set_param_values(double *values);
    double O2_gas_species_t_get_param_value(int index);
    int O2_gas_species_t_set_param_value(int index, double value);
    double O2_gas_species_t_dparam_g(double t, double p, int index);
    double O2_gas_species_t_dparam_dgdt(double t, double p, int index);
    double O2_gas_species_t_dparam_dgdp(double t, double p, int index);
    double O2_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double O2_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double O2_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double O2_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double O2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double O2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double O2_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_O2_gas_species_t_calib_identifier():
    result = <bytes> O2_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_O2_gas_species_t_calib_name():
    result = <bytes> O2_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_O2_gas_species_t_calib_formula():
    result = <bytes> O2_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_O2_gas_species_t_calib_mw():
    result = O2_gas_species_t_calib_mw()
    return result
def cy_O2_gas_species_t_calib_elements():
    cdef const double *e = O2_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_O2_gas_species_t_calib_g(double t, double p):
    result = O2_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_dgdt(double t, double p):
    result = O2_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_dgdp(double t, double p):
    result = O2_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_d2gdt2(double t, double p):
    result = O2_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_d2gdtdp(double t, double p):
    result = O2_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_d2gdp2(double t, double p):
    result = O2_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_d3gdt3(double t, double p):
    result = O2_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = O2_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = O2_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_d3gdp3(double t, double p):
    result = O2_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_s(double t, double p):
    result = O2_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_v(double t, double p):
    result = O2_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_cv(double t, double p):
    result = O2_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_cp(double t, double p):
    result = O2_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_dcpdt(double t, double p):
    result = O2_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_alpha(double t, double p):
    result = O2_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_beta(double t, double p):
    result = O2_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_K(double t, double p):
    result = O2_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_calib_Kp(double t, double p):
    result = O2_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_O2_gas_species_t_get_param_number():
    result = O2_gas_species_t_get_param_number()
    return result
def cy_O2_gas_species_t_get_param_names():
    cdef const char **names = O2_gas_species_t_get_param_names()
    n = O2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_O2_gas_species_t_get_param_units():
    cdef const char **units = O2_gas_species_t_get_param_units()
    n = O2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_O2_gas_species_t_get_param_values():
    n = O2_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    O2_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_O2_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = O2_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_O2_gas_species_t_get_param_value(int index):
    result = O2_gas_species_t_get_param_value(<int> index)
    return result
def cy_O2_gas_species_t_set_param_value(int index, double value):
    result = O2_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_O2_gas_species_t_dparam_g(double t, double p, int index):
    result = O2_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_O2_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = O2_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_O2_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = O2_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_O2_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = O2_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_O2_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = O2_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_O2_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = O2_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_O2_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = O2_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_O2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = O2_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_O2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = O2_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_O2_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = O2_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Ar1_gas_species_t_calib.h":
    const char *Ar1_gas_species_t_calib_identifier();
    const char *Ar1_gas_species_t_calib_name();
    const char *Ar1_gas_species_t_calib_formula();
    const double Ar1_gas_species_t_calib_mw();
    const double *Ar1_gas_species_t_calib_elements();
    double Ar1_gas_species_t_calib_g(double t, double p);
    double Ar1_gas_species_t_calib_dgdt(double t, double p);
    double Ar1_gas_species_t_calib_dgdp(double t, double p);
    double Ar1_gas_species_t_calib_d2gdt2(double t, double p);
    double Ar1_gas_species_t_calib_d2gdtdp(double t, double p);
    double Ar1_gas_species_t_calib_d2gdp2(double t, double p);
    double Ar1_gas_species_t_calib_d3gdt3(double t, double p);
    double Ar1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Ar1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Ar1_gas_species_t_calib_d3gdp3(double t, double p);
    double Ar1_gas_species_t_calib_s(double t, double p);
    double Ar1_gas_species_t_calib_v(double t, double p);
    double Ar1_gas_species_t_calib_cv(double t, double p);
    double Ar1_gas_species_t_calib_cp(double t, double p);
    double Ar1_gas_species_t_calib_dcpdt(double t, double p);
    double Ar1_gas_species_t_calib_alpha(double t, double p);
    double Ar1_gas_species_t_calib_beta(double t, double p);
    double Ar1_gas_species_t_calib_K(double t, double p);
    double Ar1_gas_species_t_calib_Kp(double t, double p);
    int Ar1_gas_species_t_get_param_number();
    const char **Ar1_gas_species_t_get_param_names();
    const char **Ar1_gas_species_t_get_param_units();
    void Ar1_gas_species_t_get_param_values(double **values);
    int Ar1_gas_species_t_set_param_values(double *values);
    double Ar1_gas_species_t_get_param_value(int index);
    int Ar1_gas_species_t_set_param_value(int index, double value);
    double Ar1_gas_species_t_dparam_g(double t, double p, int index);
    double Ar1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Ar1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Ar1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Ar1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Ar1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Ar1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Ar1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Ar1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Ar1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Ar1_gas_species_t_calib_identifier():
    result = <bytes> Ar1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Ar1_gas_species_t_calib_name():
    result = <bytes> Ar1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Ar1_gas_species_t_calib_formula():
    result = <bytes> Ar1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Ar1_gas_species_t_calib_mw():
    result = Ar1_gas_species_t_calib_mw()
    return result
def cy_Ar1_gas_species_t_calib_elements():
    cdef const double *e = Ar1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Ar1_gas_species_t_calib_g(double t, double p):
    result = Ar1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_dgdt(double t, double p):
    result = Ar1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_dgdp(double t, double p):
    result = Ar1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_d2gdt2(double t, double p):
    result = Ar1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Ar1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_d2gdp2(double t, double p):
    result = Ar1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_d3gdt3(double t, double p):
    result = Ar1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Ar1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Ar1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_d3gdp3(double t, double p):
    result = Ar1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_s(double t, double p):
    result = Ar1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_v(double t, double p):
    result = Ar1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_cv(double t, double p):
    result = Ar1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_cp(double t, double p):
    result = Ar1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_dcpdt(double t, double p):
    result = Ar1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_alpha(double t, double p):
    result = Ar1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_beta(double t, double p):
    result = Ar1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_K(double t, double p):
    result = Ar1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_calib_Kp(double t, double p):
    result = Ar1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Ar1_gas_species_t_get_param_number():
    result = Ar1_gas_species_t_get_param_number()
    return result
def cy_Ar1_gas_species_t_get_param_names():
    cdef const char **names = Ar1_gas_species_t_get_param_names()
    n = Ar1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Ar1_gas_species_t_get_param_units():
    cdef const char **units = Ar1_gas_species_t_get_param_units()
    n = Ar1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Ar1_gas_species_t_get_param_values():
    n = Ar1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Ar1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Ar1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Ar1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Ar1_gas_species_t_get_param_value(int index):
    result = Ar1_gas_species_t_get_param_value(<int> index)
    return result
def cy_Ar1_gas_species_t_set_param_value(int index, double value):
    result = Ar1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Ar1_gas_species_t_dparam_g(double t, double p, int index):
    result = Ar1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Ar1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Ar1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Ar1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Ar1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Ar1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Ar1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Ar1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Ar1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Ar1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Ar1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Ar1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Ar1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Ar1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Ar1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Ar1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Ar1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Ar1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Ar1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "O3_gas_species_t_calib.h":
    const char *O3_gas_species_t_calib_identifier();
    const char *O3_gas_species_t_calib_name();
    const char *O3_gas_species_t_calib_formula();
    const double O3_gas_species_t_calib_mw();
    const double *O3_gas_species_t_calib_elements();
    double O3_gas_species_t_calib_g(double t, double p);
    double O3_gas_species_t_calib_dgdt(double t, double p);
    double O3_gas_species_t_calib_dgdp(double t, double p);
    double O3_gas_species_t_calib_d2gdt2(double t, double p);
    double O3_gas_species_t_calib_d2gdtdp(double t, double p);
    double O3_gas_species_t_calib_d2gdp2(double t, double p);
    double O3_gas_species_t_calib_d3gdt3(double t, double p);
    double O3_gas_species_t_calib_d3gdt2dp(double t, double p);
    double O3_gas_species_t_calib_d3gdtdp2(double t, double p);
    double O3_gas_species_t_calib_d3gdp3(double t, double p);
    double O3_gas_species_t_calib_s(double t, double p);
    double O3_gas_species_t_calib_v(double t, double p);
    double O3_gas_species_t_calib_cv(double t, double p);
    double O3_gas_species_t_calib_cp(double t, double p);
    double O3_gas_species_t_calib_dcpdt(double t, double p);
    double O3_gas_species_t_calib_alpha(double t, double p);
    double O3_gas_species_t_calib_beta(double t, double p);
    double O3_gas_species_t_calib_K(double t, double p);
    double O3_gas_species_t_calib_Kp(double t, double p);
    int O3_gas_species_t_get_param_number();
    const char **O3_gas_species_t_get_param_names();
    const char **O3_gas_species_t_get_param_units();
    void O3_gas_species_t_get_param_values(double **values);
    int O3_gas_species_t_set_param_values(double *values);
    double O3_gas_species_t_get_param_value(int index);
    int O3_gas_species_t_set_param_value(int index, double value);
    double O3_gas_species_t_dparam_g(double t, double p, int index);
    double O3_gas_species_t_dparam_dgdt(double t, double p, int index);
    double O3_gas_species_t_dparam_dgdp(double t, double p, int index);
    double O3_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double O3_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double O3_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double O3_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double O3_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double O3_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double O3_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_O3_gas_species_t_calib_identifier():
    result = <bytes> O3_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_O3_gas_species_t_calib_name():
    result = <bytes> O3_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_O3_gas_species_t_calib_formula():
    result = <bytes> O3_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_O3_gas_species_t_calib_mw():
    result = O3_gas_species_t_calib_mw()
    return result
def cy_O3_gas_species_t_calib_elements():
    cdef const double *e = O3_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_O3_gas_species_t_calib_g(double t, double p):
    result = O3_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_dgdt(double t, double p):
    result = O3_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_dgdp(double t, double p):
    result = O3_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_d2gdt2(double t, double p):
    result = O3_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_d2gdtdp(double t, double p):
    result = O3_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_d2gdp2(double t, double p):
    result = O3_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_d3gdt3(double t, double p):
    result = O3_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = O3_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = O3_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_d3gdp3(double t, double p):
    result = O3_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_s(double t, double p):
    result = O3_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_v(double t, double p):
    result = O3_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_cv(double t, double p):
    result = O3_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_cp(double t, double p):
    result = O3_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_dcpdt(double t, double p):
    result = O3_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_alpha(double t, double p):
    result = O3_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_beta(double t, double p):
    result = O3_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_K(double t, double p):
    result = O3_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_calib_Kp(double t, double p):
    result = O3_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_O3_gas_species_t_get_param_number():
    result = O3_gas_species_t_get_param_number()
    return result
def cy_O3_gas_species_t_get_param_names():
    cdef const char **names = O3_gas_species_t_get_param_names()
    n = O3_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_O3_gas_species_t_get_param_units():
    cdef const char **units = O3_gas_species_t_get_param_units()
    n = O3_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_O3_gas_species_t_get_param_values():
    n = O3_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    O3_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_O3_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = O3_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_O3_gas_species_t_get_param_value(int index):
    result = O3_gas_species_t_get_param_value(<int> index)
    return result
def cy_O3_gas_species_t_set_param_value(int index, double value):
    result = O3_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_O3_gas_species_t_dparam_g(double t, double p, int index):
    result = O3_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_O3_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = O3_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_O3_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = O3_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_O3_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = O3_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_O3_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = O3_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_O3_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = O3_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_O3_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = O3_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_O3_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = O3_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_O3_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = O3_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_O3_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = O3_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "H1O2_gas_species_t_calib.h":
    const char *H1O2_gas_species_t_calib_identifier();
    const char *H1O2_gas_species_t_calib_name();
    const char *H1O2_gas_species_t_calib_formula();
    const double H1O2_gas_species_t_calib_mw();
    const double *H1O2_gas_species_t_calib_elements();
    double H1O2_gas_species_t_calib_g(double t, double p);
    double H1O2_gas_species_t_calib_dgdt(double t, double p);
    double H1O2_gas_species_t_calib_dgdp(double t, double p);
    double H1O2_gas_species_t_calib_d2gdt2(double t, double p);
    double H1O2_gas_species_t_calib_d2gdtdp(double t, double p);
    double H1O2_gas_species_t_calib_d2gdp2(double t, double p);
    double H1O2_gas_species_t_calib_d3gdt3(double t, double p);
    double H1O2_gas_species_t_calib_d3gdt2dp(double t, double p);
    double H1O2_gas_species_t_calib_d3gdtdp2(double t, double p);
    double H1O2_gas_species_t_calib_d3gdp3(double t, double p);
    double H1O2_gas_species_t_calib_s(double t, double p);
    double H1O2_gas_species_t_calib_v(double t, double p);
    double H1O2_gas_species_t_calib_cv(double t, double p);
    double H1O2_gas_species_t_calib_cp(double t, double p);
    double H1O2_gas_species_t_calib_dcpdt(double t, double p);
    double H1O2_gas_species_t_calib_alpha(double t, double p);
    double H1O2_gas_species_t_calib_beta(double t, double p);
    double H1O2_gas_species_t_calib_K(double t, double p);
    double H1O2_gas_species_t_calib_Kp(double t, double p);
    int H1O2_gas_species_t_get_param_number();
    const char **H1O2_gas_species_t_get_param_names();
    const char **H1O2_gas_species_t_get_param_units();
    void H1O2_gas_species_t_get_param_values(double **values);
    int H1O2_gas_species_t_set_param_values(double *values);
    double H1O2_gas_species_t_get_param_value(int index);
    int H1O2_gas_species_t_set_param_value(int index, double value);
    double H1O2_gas_species_t_dparam_g(double t, double p, int index);
    double H1O2_gas_species_t_dparam_dgdt(double t, double p, int index);
    double H1O2_gas_species_t_dparam_dgdp(double t, double p, int index);
    double H1O2_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double H1O2_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double H1O2_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double H1O2_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double H1O2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double H1O2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double H1O2_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_H1O2_gas_species_t_calib_identifier():
    result = <bytes> H1O2_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_H1O2_gas_species_t_calib_name():
    result = <bytes> H1O2_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_H1O2_gas_species_t_calib_formula():
    result = <bytes> H1O2_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_H1O2_gas_species_t_calib_mw():
    result = H1O2_gas_species_t_calib_mw()
    return result
def cy_H1O2_gas_species_t_calib_elements():
    cdef const double *e = H1O2_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_H1O2_gas_species_t_calib_g(double t, double p):
    result = H1O2_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_dgdt(double t, double p):
    result = H1O2_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_dgdp(double t, double p):
    result = H1O2_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_d2gdt2(double t, double p):
    result = H1O2_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_d2gdtdp(double t, double p):
    result = H1O2_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_d2gdp2(double t, double p):
    result = H1O2_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_d3gdt3(double t, double p):
    result = H1O2_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = H1O2_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = H1O2_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_d3gdp3(double t, double p):
    result = H1O2_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_s(double t, double p):
    result = H1O2_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_v(double t, double p):
    result = H1O2_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_cv(double t, double p):
    result = H1O2_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_cp(double t, double p):
    result = H1O2_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_dcpdt(double t, double p):
    result = H1O2_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_alpha(double t, double p):
    result = H1O2_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_beta(double t, double p):
    result = H1O2_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_K(double t, double p):
    result = H1O2_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_calib_Kp(double t, double p):
    result = H1O2_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_H1O2_gas_species_t_get_param_number():
    result = H1O2_gas_species_t_get_param_number()
    return result
def cy_H1O2_gas_species_t_get_param_names():
    cdef const char **names = H1O2_gas_species_t_get_param_names()
    n = H1O2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H1O2_gas_species_t_get_param_units():
    cdef const char **units = H1O2_gas_species_t_get_param_units()
    n = H1O2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H1O2_gas_species_t_get_param_values():
    n = H1O2_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    H1O2_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_H1O2_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = H1O2_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_H1O2_gas_species_t_get_param_value(int index):
    result = H1O2_gas_species_t_get_param_value(<int> index)
    return result
def cy_H1O2_gas_species_t_set_param_value(int index, double value):
    result = H1O2_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_H1O2_gas_species_t_dparam_g(double t, double p, int index):
    result = H1O2_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_H1O2_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = H1O2_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_H1O2_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = H1O2_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_H1O2_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = H1O2_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_H1O2_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = H1O2_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_H1O2_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = H1O2_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_H1O2_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = H1O2_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_H1O2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = H1O2_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_H1O2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = H1O2_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_H1O2_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = H1O2_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "O1_gas_species_t_calib.h":
    const char *O1_gas_species_t_calib_identifier();
    const char *O1_gas_species_t_calib_name();
    const char *O1_gas_species_t_calib_formula();
    const double O1_gas_species_t_calib_mw();
    const double *O1_gas_species_t_calib_elements();
    double O1_gas_species_t_calib_g(double t, double p);
    double O1_gas_species_t_calib_dgdt(double t, double p);
    double O1_gas_species_t_calib_dgdp(double t, double p);
    double O1_gas_species_t_calib_d2gdt2(double t, double p);
    double O1_gas_species_t_calib_d2gdtdp(double t, double p);
    double O1_gas_species_t_calib_d2gdp2(double t, double p);
    double O1_gas_species_t_calib_d3gdt3(double t, double p);
    double O1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double O1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double O1_gas_species_t_calib_d3gdp3(double t, double p);
    double O1_gas_species_t_calib_s(double t, double p);
    double O1_gas_species_t_calib_v(double t, double p);
    double O1_gas_species_t_calib_cv(double t, double p);
    double O1_gas_species_t_calib_cp(double t, double p);
    double O1_gas_species_t_calib_dcpdt(double t, double p);
    double O1_gas_species_t_calib_alpha(double t, double p);
    double O1_gas_species_t_calib_beta(double t, double p);
    double O1_gas_species_t_calib_K(double t, double p);
    double O1_gas_species_t_calib_Kp(double t, double p);
    int O1_gas_species_t_get_param_number();
    const char **O1_gas_species_t_get_param_names();
    const char **O1_gas_species_t_get_param_units();
    void O1_gas_species_t_get_param_values(double **values);
    int O1_gas_species_t_set_param_values(double *values);
    double O1_gas_species_t_get_param_value(int index);
    int O1_gas_species_t_set_param_value(int index, double value);
    double O1_gas_species_t_dparam_g(double t, double p, int index);
    double O1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double O1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double O1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double O1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double O1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double O1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_O1_gas_species_t_calib_identifier():
    result = <bytes> O1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_O1_gas_species_t_calib_name():
    result = <bytes> O1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_O1_gas_species_t_calib_formula():
    result = <bytes> O1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_O1_gas_species_t_calib_mw():
    result = O1_gas_species_t_calib_mw()
    return result
def cy_O1_gas_species_t_calib_elements():
    cdef const double *e = O1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_O1_gas_species_t_calib_g(double t, double p):
    result = O1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_dgdt(double t, double p):
    result = O1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_dgdp(double t, double p):
    result = O1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_d2gdt2(double t, double p):
    result = O1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = O1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_d2gdp2(double t, double p):
    result = O1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_d3gdt3(double t, double p):
    result = O1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = O1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = O1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_d3gdp3(double t, double p):
    result = O1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_s(double t, double p):
    result = O1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_v(double t, double p):
    result = O1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_cv(double t, double p):
    result = O1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_cp(double t, double p):
    result = O1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_dcpdt(double t, double p):
    result = O1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_alpha(double t, double p):
    result = O1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_beta(double t, double p):
    result = O1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_K(double t, double p):
    result = O1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_calib_Kp(double t, double p):
    result = O1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_O1_gas_species_t_get_param_number():
    result = O1_gas_species_t_get_param_number()
    return result
def cy_O1_gas_species_t_get_param_names():
    cdef const char **names = O1_gas_species_t_get_param_names()
    n = O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_O1_gas_species_t_get_param_units():
    cdef const char **units = O1_gas_species_t_get_param_units()
    n = O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_O1_gas_species_t_get_param_values():
    n = O1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    O1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_O1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = O1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_O1_gas_species_t_get_param_value(int index):
    result = O1_gas_species_t_get_param_value(<int> index)
    return result
def cy_O1_gas_species_t_set_param_value(int index, double value):
    result = O1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_O1_gas_species_t_dparam_g(double t, double p, int index):
    result = O1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_O1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = O1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_O1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = O1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_O1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = O1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = O1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_O1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = O1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_O1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = O1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = O1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = O1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_O1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = O1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "H1Mg1O1_gas_species_t_calib.h":
    const char *H1Mg1O1_gas_species_t_calib_identifier();
    const char *H1Mg1O1_gas_species_t_calib_name();
    const char *H1Mg1O1_gas_species_t_calib_formula();
    const double H1Mg1O1_gas_species_t_calib_mw();
    const double *H1Mg1O1_gas_species_t_calib_elements();
    double H1Mg1O1_gas_species_t_calib_g(double t, double p);
    double H1Mg1O1_gas_species_t_calib_dgdt(double t, double p);
    double H1Mg1O1_gas_species_t_calib_dgdp(double t, double p);
    double H1Mg1O1_gas_species_t_calib_d2gdt2(double t, double p);
    double H1Mg1O1_gas_species_t_calib_d2gdtdp(double t, double p);
    double H1Mg1O1_gas_species_t_calib_d2gdp2(double t, double p);
    double H1Mg1O1_gas_species_t_calib_d3gdt3(double t, double p);
    double H1Mg1O1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double H1Mg1O1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double H1Mg1O1_gas_species_t_calib_d3gdp3(double t, double p);
    double H1Mg1O1_gas_species_t_calib_s(double t, double p);
    double H1Mg1O1_gas_species_t_calib_v(double t, double p);
    double H1Mg1O1_gas_species_t_calib_cv(double t, double p);
    double H1Mg1O1_gas_species_t_calib_cp(double t, double p);
    double H1Mg1O1_gas_species_t_calib_dcpdt(double t, double p);
    double H1Mg1O1_gas_species_t_calib_alpha(double t, double p);
    double H1Mg1O1_gas_species_t_calib_beta(double t, double p);
    double H1Mg1O1_gas_species_t_calib_K(double t, double p);
    double H1Mg1O1_gas_species_t_calib_Kp(double t, double p);
    int H1Mg1O1_gas_species_t_get_param_number();
    const char **H1Mg1O1_gas_species_t_get_param_names();
    const char **H1Mg1O1_gas_species_t_get_param_units();
    void H1Mg1O1_gas_species_t_get_param_values(double **values);
    int H1Mg1O1_gas_species_t_set_param_values(double *values);
    double H1Mg1O1_gas_species_t_get_param_value(int index);
    int H1Mg1O1_gas_species_t_set_param_value(int index, double value);
    double H1Mg1O1_gas_species_t_dparam_g(double t, double p, int index);
    double H1Mg1O1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double H1Mg1O1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double H1Mg1O1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double H1Mg1O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double H1Mg1O1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double H1Mg1O1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double H1Mg1O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double H1Mg1O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double H1Mg1O1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_H1Mg1O1_gas_species_t_calib_identifier():
    result = <bytes> H1Mg1O1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_H1Mg1O1_gas_species_t_calib_name():
    result = <bytes> H1Mg1O1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_H1Mg1O1_gas_species_t_calib_formula():
    result = <bytes> H1Mg1O1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_H1Mg1O1_gas_species_t_calib_mw():
    result = H1Mg1O1_gas_species_t_calib_mw()
    return result
def cy_H1Mg1O1_gas_species_t_calib_elements():
    cdef const double *e = H1Mg1O1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_H1Mg1O1_gas_species_t_calib_g(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_dgdt(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_dgdp(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_d2gdt2(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_d2gdp2(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_d3gdt3(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_d3gdp3(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_s(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_v(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_cv(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_cp(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_dcpdt(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_alpha(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_beta(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_K(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_calib_Kp(double t, double p):
    result = H1Mg1O1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_H1Mg1O1_gas_species_t_get_param_number():
    result = H1Mg1O1_gas_species_t_get_param_number()
    return result
def cy_H1Mg1O1_gas_species_t_get_param_names():
    cdef const char **names = H1Mg1O1_gas_species_t_get_param_names()
    n = H1Mg1O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H1Mg1O1_gas_species_t_get_param_units():
    cdef const char **units = H1Mg1O1_gas_species_t_get_param_units()
    n = H1Mg1O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H1Mg1O1_gas_species_t_get_param_values():
    n = H1Mg1O1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    H1Mg1O1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_H1Mg1O1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = H1Mg1O1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_H1Mg1O1_gas_species_t_get_param_value(int index):
    result = H1Mg1O1_gas_species_t_get_param_value(<int> index)
    return result
def cy_H1Mg1O1_gas_species_t_set_param_value(int index, double value):
    result = H1Mg1O1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_H1Mg1O1_gas_species_t_dparam_g(double t, double p, int index):
    result = H1Mg1O1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1O1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = H1Mg1O1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1O1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = H1Mg1O1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1O1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = H1Mg1O1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = H1Mg1O1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1O1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = H1Mg1O1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1O1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = H1Mg1O1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = H1Mg1O1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = H1Mg1O1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_H1Mg1O1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = H1Mg1O1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "H2Mg1O2_gas_species_t_calib.h":
    const char *H2Mg1O2_gas_species_t_calib_identifier();
    const char *H2Mg1O2_gas_species_t_calib_name();
    const char *H2Mg1O2_gas_species_t_calib_formula();
    const double H2Mg1O2_gas_species_t_calib_mw();
    const double *H2Mg1O2_gas_species_t_calib_elements();
    double H2Mg1O2_gas_species_t_calib_g(double t, double p);
    double H2Mg1O2_gas_species_t_calib_dgdt(double t, double p);
    double H2Mg1O2_gas_species_t_calib_dgdp(double t, double p);
    double H2Mg1O2_gas_species_t_calib_d2gdt2(double t, double p);
    double H2Mg1O2_gas_species_t_calib_d2gdtdp(double t, double p);
    double H2Mg1O2_gas_species_t_calib_d2gdp2(double t, double p);
    double H2Mg1O2_gas_species_t_calib_d3gdt3(double t, double p);
    double H2Mg1O2_gas_species_t_calib_d3gdt2dp(double t, double p);
    double H2Mg1O2_gas_species_t_calib_d3gdtdp2(double t, double p);
    double H2Mg1O2_gas_species_t_calib_d3gdp3(double t, double p);
    double H2Mg1O2_gas_species_t_calib_s(double t, double p);
    double H2Mg1O2_gas_species_t_calib_v(double t, double p);
    double H2Mg1O2_gas_species_t_calib_cv(double t, double p);
    double H2Mg1O2_gas_species_t_calib_cp(double t, double p);
    double H2Mg1O2_gas_species_t_calib_dcpdt(double t, double p);
    double H2Mg1O2_gas_species_t_calib_alpha(double t, double p);
    double H2Mg1O2_gas_species_t_calib_beta(double t, double p);
    double H2Mg1O2_gas_species_t_calib_K(double t, double p);
    double H2Mg1O2_gas_species_t_calib_Kp(double t, double p);
    int H2Mg1O2_gas_species_t_get_param_number();
    const char **H2Mg1O2_gas_species_t_get_param_names();
    const char **H2Mg1O2_gas_species_t_get_param_units();
    void H2Mg1O2_gas_species_t_get_param_values(double **values);
    int H2Mg1O2_gas_species_t_set_param_values(double *values);
    double H2Mg1O2_gas_species_t_get_param_value(int index);
    int H2Mg1O2_gas_species_t_set_param_value(int index, double value);
    double H2Mg1O2_gas_species_t_dparam_g(double t, double p, int index);
    double H2Mg1O2_gas_species_t_dparam_dgdt(double t, double p, int index);
    double H2Mg1O2_gas_species_t_dparam_dgdp(double t, double p, int index);
    double H2Mg1O2_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double H2Mg1O2_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double H2Mg1O2_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double H2Mg1O2_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double H2Mg1O2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double H2Mg1O2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double H2Mg1O2_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_H2Mg1O2_gas_species_t_calib_identifier():
    result = <bytes> H2Mg1O2_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_H2Mg1O2_gas_species_t_calib_name():
    result = <bytes> H2Mg1O2_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_H2Mg1O2_gas_species_t_calib_formula():
    result = <bytes> H2Mg1O2_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_H2Mg1O2_gas_species_t_calib_mw():
    result = H2Mg1O2_gas_species_t_calib_mw()
    return result
def cy_H2Mg1O2_gas_species_t_calib_elements():
    cdef const double *e = H2Mg1O2_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_H2Mg1O2_gas_species_t_calib_g(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_dgdt(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_dgdp(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_d2gdt2(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_d2gdtdp(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_d2gdp2(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_d3gdt3(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_d3gdp3(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_s(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_v(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_cv(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_cp(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_dcpdt(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_alpha(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_beta(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_K(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_calib_Kp(double t, double p):
    result = H2Mg1O2_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_H2Mg1O2_gas_species_t_get_param_number():
    result = H2Mg1O2_gas_species_t_get_param_number()
    return result
def cy_H2Mg1O2_gas_species_t_get_param_names():
    cdef const char **names = H2Mg1O2_gas_species_t_get_param_names()
    n = H2Mg1O2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H2Mg1O2_gas_species_t_get_param_units():
    cdef const char **units = H2Mg1O2_gas_species_t_get_param_units()
    n = H2Mg1O2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H2Mg1O2_gas_species_t_get_param_values():
    n = H2Mg1O2_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    H2Mg1O2_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_H2Mg1O2_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = H2Mg1O2_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_H2Mg1O2_gas_species_t_get_param_value(int index):
    result = H2Mg1O2_gas_species_t_get_param_value(<int> index)
    return result
def cy_H2Mg1O2_gas_species_t_set_param_value(int index, double value):
    result = H2Mg1O2_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_H2Mg1O2_gas_species_t_dparam_g(double t, double p, int index):
    result = H2Mg1O2_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_H2Mg1O2_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = H2Mg1O2_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_H2Mg1O2_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = H2Mg1O2_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_H2Mg1O2_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = H2Mg1O2_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_H2Mg1O2_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = H2Mg1O2_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_H2Mg1O2_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = H2Mg1O2_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_H2Mg1O2_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = H2Mg1O2_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_H2Mg1O2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = H2Mg1O2_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_H2Mg1O2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = H2Mg1O2_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_H2Mg1O2_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = H2Mg1O2_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Al2_gas_species_t_calib.h":
    const char *Al2_gas_species_t_calib_identifier();
    const char *Al2_gas_species_t_calib_name();
    const char *Al2_gas_species_t_calib_formula();
    const double Al2_gas_species_t_calib_mw();
    const double *Al2_gas_species_t_calib_elements();
    double Al2_gas_species_t_calib_g(double t, double p);
    double Al2_gas_species_t_calib_dgdt(double t, double p);
    double Al2_gas_species_t_calib_dgdp(double t, double p);
    double Al2_gas_species_t_calib_d2gdt2(double t, double p);
    double Al2_gas_species_t_calib_d2gdtdp(double t, double p);
    double Al2_gas_species_t_calib_d2gdp2(double t, double p);
    double Al2_gas_species_t_calib_d3gdt3(double t, double p);
    double Al2_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Al2_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Al2_gas_species_t_calib_d3gdp3(double t, double p);
    double Al2_gas_species_t_calib_s(double t, double p);
    double Al2_gas_species_t_calib_v(double t, double p);
    double Al2_gas_species_t_calib_cv(double t, double p);
    double Al2_gas_species_t_calib_cp(double t, double p);
    double Al2_gas_species_t_calib_dcpdt(double t, double p);
    double Al2_gas_species_t_calib_alpha(double t, double p);
    double Al2_gas_species_t_calib_beta(double t, double p);
    double Al2_gas_species_t_calib_K(double t, double p);
    double Al2_gas_species_t_calib_Kp(double t, double p);
    int Al2_gas_species_t_get_param_number();
    const char **Al2_gas_species_t_get_param_names();
    const char **Al2_gas_species_t_get_param_units();
    void Al2_gas_species_t_get_param_values(double **values);
    int Al2_gas_species_t_set_param_values(double *values);
    double Al2_gas_species_t_get_param_value(int index);
    int Al2_gas_species_t_set_param_value(int index, double value);
    double Al2_gas_species_t_dparam_g(double t, double p, int index);
    double Al2_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Al2_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Al2_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Al2_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Al2_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Al2_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Al2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Al2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Al2_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Al2_gas_species_t_calib_identifier():
    result = <bytes> Al2_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Al2_gas_species_t_calib_name():
    result = <bytes> Al2_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Al2_gas_species_t_calib_formula():
    result = <bytes> Al2_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Al2_gas_species_t_calib_mw():
    result = Al2_gas_species_t_calib_mw()
    return result
def cy_Al2_gas_species_t_calib_elements():
    cdef const double *e = Al2_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Al2_gas_species_t_calib_g(double t, double p):
    result = Al2_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_dgdt(double t, double p):
    result = Al2_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_dgdp(double t, double p):
    result = Al2_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_d2gdt2(double t, double p):
    result = Al2_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Al2_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_d2gdp2(double t, double p):
    result = Al2_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_d3gdt3(double t, double p):
    result = Al2_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Al2_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Al2_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_d3gdp3(double t, double p):
    result = Al2_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_s(double t, double p):
    result = Al2_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_v(double t, double p):
    result = Al2_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_cv(double t, double p):
    result = Al2_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_cp(double t, double p):
    result = Al2_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_dcpdt(double t, double p):
    result = Al2_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_alpha(double t, double p):
    result = Al2_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_beta(double t, double p):
    result = Al2_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_K(double t, double p):
    result = Al2_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_calib_Kp(double t, double p):
    result = Al2_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Al2_gas_species_t_get_param_number():
    result = Al2_gas_species_t_get_param_number()
    return result
def cy_Al2_gas_species_t_get_param_names():
    cdef const char **names = Al2_gas_species_t_get_param_names()
    n = Al2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al2_gas_species_t_get_param_units():
    cdef const char **units = Al2_gas_species_t_get_param_units()
    n = Al2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al2_gas_species_t_get_param_values():
    n = Al2_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Al2_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Al2_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Al2_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Al2_gas_species_t_get_param_value(int index):
    result = Al2_gas_species_t_get_param_value(<int> index)
    return result
def cy_Al2_gas_species_t_set_param_value(int index, double value):
    result = Al2_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Al2_gas_species_t_dparam_g(double t, double p, int index):
    result = Al2_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Al2_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Al2_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Al2_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Al2_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Al2_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Al2_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Al2_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Al2_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Al2_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Al2_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al2_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Al2_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Al2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Al2_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Al2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Al2_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al2_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Al2_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "O2Ti1_gas_species_t_calib.h":
    const char *O2Ti1_gas_species_t_calib_identifier();
    const char *O2Ti1_gas_species_t_calib_name();
    const char *O2Ti1_gas_species_t_calib_formula();
    const double O2Ti1_gas_species_t_calib_mw();
    const double *O2Ti1_gas_species_t_calib_elements();
    double O2Ti1_gas_species_t_calib_g(double t, double p);
    double O2Ti1_gas_species_t_calib_dgdt(double t, double p);
    double O2Ti1_gas_species_t_calib_dgdp(double t, double p);
    double O2Ti1_gas_species_t_calib_d2gdt2(double t, double p);
    double O2Ti1_gas_species_t_calib_d2gdtdp(double t, double p);
    double O2Ti1_gas_species_t_calib_d2gdp2(double t, double p);
    double O2Ti1_gas_species_t_calib_d3gdt3(double t, double p);
    double O2Ti1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double O2Ti1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double O2Ti1_gas_species_t_calib_d3gdp3(double t, double p);
    double O2Ti1_gas_species_t_calib_s(double t, double p);
    double O2Ti1_gas_species_t_calib_v(double t, double p);
    double O2Ti1_gas_species_t_calib_cv(double t, double p);
    double O2Ti1_gas_species_t_calib_cp(double t, double p);
    double O2Ti1_gas_species_t_calib_dcpdt(double t, double p);
    double O2Ti1_gas_species_t_calib_alpha(double t, double p);
    double O2Ti1_gas_species_t_calib_beta(double t, double p);
    double O2Ti1_gas_species_t_calib_K(double t, double p);
    double O2Ti1_gas_species_t_calib_Kp(double t, double p);
    int O2Ti1_gas_species_t_get_param_number();
    const char **O2Ti1_gas_species_t_get_param_names();
    const char **O2Ti1_gas_species_t_get_param_units();
    void O2Ti1_gas_species_t_get_param_values(double **values);
    int O2Ti1_gas_species_t_set_param_values(double *values);
    double O2Ti1_gas_species_t_get_param_value(int index);
    int O2Ti1_gas_species_t_set_param_value(int index, double value);
    double O2Ti1_gas_species_t_dparam_g(double t, double p, int index);
    double O2Ti1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double O2Ti1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double O2Ti1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double O2Ti1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double O2Ti1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double O2Ti1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double O2Ti1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double O2Ti1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double O2Ti1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_O2Ti1_gas_species_t_calib_identifier():
    result = <bytes> O2Ti1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_O2Ti1_gas_species_t_calib_name():
    result = <bytes> O2Ti1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_O2Ti1_gas_species_t_calib_formula():
    result = <bytes> O2Ti1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_O2Ti1_gas_species_t_calib_mw():
    result = O2Ti1_gas_species_t_calib_mw()
    return result
def cy_O2Ti1_gas_species_t_calib_elements():
    cdef const double *e = O2Ti1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_O2Ti1_gas_species_t_calib_g(double t, double p):
    result = O2Ti1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_dgdt(double t, double p):
    result = O2Ti1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_dgdp(double t, double p):
    result = O2Ti1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_d2gdt2(double t, double p):
    result = O2Ti1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = O2Ti1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_d2gdp2(double t, double p):
    result = O2Ti1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_d3gdt3(double t, double p):
    result = O2Ti1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = O2Ti1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = O2Ti1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_d3gdp3(double t, double p):
    result = O2Ti1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_s(double t, double p):
    result = O2Ti1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_v(double t, double p):
    result = O2Ti1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_cv(double t, double p):
    result = O2Ti1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_cp(double t, double p):
    result = O2Ti1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_dcpdt(double t, double p):
    result = O2Ti1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_alpha(double t, double p):
    result = O2Ti1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_beta(double t, double p):
    result = O2Ti1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_K(double t, double p):
    result = O2Ti1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_calib_Kp(double t, double p):
    result = O2Ti1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_O2Ti1_gas_species_t_get_param_number():
    result = O2Ti1_gas_species_t_get_param_number()
    return result
def cy_O2Ti1_gas_species_t_get_param_names():
    cdef const char **names = O2Ti1_gas_species_t_get_param_names()
    n = O2Ti1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_O2Ti1_gas_species_t_get_param_units():
    cdef const char **units = O2Ti1_gas_species_t_get_param_units()
    n = O2Ti1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_O2Ti1_gas_species_t_get_param_values():
    n = O2Ti1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    O2Ti1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_O2Ti1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = O2Ti1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_O2Ti1_gas_species_t_get_param_value(int index):
    result = O2Ti1_gas_species_t_get_param_value(<int> index)
    return result
def cy_O2Ti1_gas_species_t_set_param_value(int index, double value):
    result = O2Ti1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_O2Ti1_gas_species_t_dparam_g(double t, double p, int index):
    result = O2Ti1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_O2Ti1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = O2Ti1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_O2Ti1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = O2Ti1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_O2Ti1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = O2Ti1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_O2Ti1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = O2Ti1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_O2Ti1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = O2Ti1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_O2Ti1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = O2Ti1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_O2Ti1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = O2Ti1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_O2Ti1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = O2Ti1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_O2Ti1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = O2Ti1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Al1H1_gas_species_t_calib.h":
    const char *Al1H1_gas_species_t_calib_identifier();
    const char *Al1H1_gas_species_t_calib_name();
    const char *Al1H1_gas_species_t_calib_formula();
    const double Al1H1_gas_species_t_calib_mw();
    const double *Al1H1_gas_species_t_calib_elements();
    double Al1H1_gas_species_t_calib_g(double t, double p);
    double Al1H1_gas_species_t_calib_dgdt(double t, double p);
    double Al1H1_gas_species_t_calib_dgdp(double t, double p);
    double Al1H1_gas_species_t_calib_d2gdt2(double t, double p);
    double Al1H1_gas_species_t_calib_d2gdtdp(double t, double p);
    double Al1H1_gas_species_t_calib_d2gdp2(double t, double p);
    double Al1H1_gas_species_t_calib_d3gdt3(double t, double p);
    double Al1H1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Al1H1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Al1H1_gas_species_t_calib_d3gdp3(double t, double p);
    double Al1H1_gas_species_t_calib_s(double t, double p);
    double Al1H1_gas_species_t_calib_v(double t, double p);
    double Al1H1_gas_species_t_calib_cv(double t, double p);
    double Al1H1_gas_species_t_calib_cp(double t, double p);
    double Al1H1_gas_species_t_calib_dcpdt(double t, double p);
    double Al1H1_gas_species_t_calib_alpha(double t, double p);
    double Al1H1_gas_species_t_calib_beta(double t, double p);
    double Al1H1_gas_species_t_calib_K(double t, double p);
    double Al1H1_gas_species_t_calib_Kp(double t, double p);
    int Al1H1_gas_species_t_get_param_number();
    const char **Al1H1_gas_species_t_get_param_names();
    const char **Al1H1_gas_species_t_get_param_units();
    void Al1H1_gas_species_t_get_param_values(double **values);
    int Al1H1_gas_species_t_set_param_values(double *values);
    double Al1H1_gas_species_t_get_param_value(int index);
    int Al1H1_gas_species_t_set_param_value(int index, double value);
    double Al1H1_gas_species_t_dparam_g(double t, double p, int index);
    double Al1H1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Al1H1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Al1H1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Al1H1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Al1H1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Al1H1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Al1H1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Al1H1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Al1H1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Al1H1_gas_species_t_calib_identifier():
    result = <bytes> Al1H1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Al1H1_gas_species_t_calib_name():
    result = <bytes> Al1H1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Al1H1_gas_species_t_calib_formula():
    result = <bytes> Al1H1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Al1H1_gas_species_t_calib_mw():
    result = Al1H1_gas_species_t_calib_mw()
    return result
def cy_Al1H1_gas_species_t_calib_elements():
    cdef const double *e = Al1H1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Al1H1_gas_species_t_calib_g(double t, double p):
    result = Al1H1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_dgdt(double t, double p):
    result = Al1H1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_dgdp(double t, double p):
    result = Al1H1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_d2gdt2(double t, double p):
    result = Al1H1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Al1H1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_d2gdp2(double t, double p):
    result = Al1H1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_d3gdt3(double t, double p):
    result = Al1H1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Al1H1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Al1H1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_d3gdp3(double t, double p):
    result = Al1H1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_s(double t, double p):
    result = Al1H1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_v(double t, double p):
    result = Al1H1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_cv(double t, double p):
    result = Al1H1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_cp(double t, double p):
    result = Al1H1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_dcpdt(double t, double p):
    result = Al1H1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_alpha(double t, double p):
    result = Al1H1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_beta(double t, double p):
    result = Al1H1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_K(double t, double p):
    result = Al1H1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_calib_Kp(double t, double p):
    result = Al1H1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Al1H1_gas_species_t_get_param_number():
    result = Al1H1_gas_species_t_get_param_number()
    return result
def cy_Al1H1_gas_species_t_get_param_names():
    cdef const char **names = Al1H1_gas_species_t_get_param_names()
    n = Al1H1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al1H1_gas_species_t_get_param_units():
    cdef const char **units = Al1H1_gas_species_t_get_param_units()
    n = Al1H1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al1H1_gas_species_t_get_param_values():
    n = Al1H1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Al1H1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Al1H1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Al1H1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Al1H1_gas_species_t_get_param_value(int index):
    result = Al1H1_gas_species_t_get_param_value(<int> index)
    return result
def cy_Al1H1_gas_species_t_set_param_value(int index, double value):
    result = Al1H1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Al1H1_gas_species_t_dparam_g(double t, double p, int index):
    result = Al1H1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Al1H1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Al1H1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Al1H1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Al1H1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Al1H1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Al1H1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Al1H1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Al1H1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Al1H1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Ca1_gas_species_t_calib.h":
    const char *Ca1_gas_species_t_calib_identifier();
    const char *Ca1_gas_species_t_calib_name();
    const char *Ca1_gas_species_t_calib_formula();
    const double Ca1_gas_species_t_calib_mw();
    const double *Ca1_gas_species_t_calib_elements();
    double Ca1_gas_species_t_calib_g(double t, double p);
    double Ca1_gas_species_t_calib_dgdt(double t, double p);
    double Ca1_gas_species_t_calib_dgdp(double t, double p);
    double Ca1_gas_species_t_calib_d2gdt2(double t, double p);
    double Ca1_gas_species_t_calib_d2gdtdp(double t, double p);
    double Ca1_gas_species_t_calib_d2gdp2(double t, double p);
    double Ca1_gas_species_t_calib_d3gdt3(double t, double p);
    double Ca1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Ca1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Ca1_gas_species_t_calib_d3gdp3(double t, double p);
    double Ca1_gas_species_t_calib_s(double t, double p);
    double Ca1_gas_species_t_calib_v(double t, double p);
    double Ca1_gas_species_t_calib_cv(double t, double p);
    double Ca1_gas_species_t_calib_cp(double t, double p);
    double Ca1_gas_species_t_calib_dcpdt(double t, double p);
    double Ca1_gas_species_t_calib_alpha(double t, double p);
    double Ca1_gas_species_t_calib_beta(double t, double p);
    double Ca1_gas_species_t_calib_K(double t, double p);
    double Ca1_gas_species_t_calib_Kp(double t, double p);
    int Ca1_gas_species_t_get_param_number();
    const char **Ca1_gas_species_t_get_param_names();
    const char **Ca1_gas_species_t_get_param_units();
    void Ca1_gas_species_t_get_param_values(double **values);
    int Ca1_gas_species_t_set_param_values(double *values);
    double Ca1_gas_species_t_get_param_value(int index);
    int Ca1_gas_species_t_set_param_value(int index, double value);
    double Ca1_gas_species_t_dparam_g(double t, double p, int index);
    double Ca1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Ca1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Ca1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Ca1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Ca1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Ca1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Ca1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Ca1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Ca1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Ca1_gas_species_t_calib_identifier():
    result = <bytes> Ca1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Ca1_gas_species_t_calib_name():
    result = <bytes> Ca1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Ca1_gas_species_t_calib_formula():
    result = <bytes> Ca1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Ca1_gas_species_t_calib_mw():
    result = Ca1_gas_species_t_calib_mw()
    return result
def cy_Ca1_gas_species_t_calib_elements():
    cdef const double *e = Ca1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Ca1_gas_species_t_calib_g(double t, double p):
    result = Ca1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_dgdt(double t, double p):
    result = Ca1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_dgdp(double t, double p):
    result = Ca1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_d2gdt2(double t, double p):
    result = Ca1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Ca1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_d2gdp2(double t, double p):
    result = Ca1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_d3gdt3(double t, double p):
    result = Ca1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Ca1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Ca1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_d3gdp3(double t, double p):
    result = Ca1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_s(double t, double p):
    result = Ca1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_v(double t, double p):
    result = Ca1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_cv(double t, double p):
    result = Ca1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_cp(double t, double p):
    result = Ca1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_dcpdt(double t, double p):
    result = Ca1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_alpha(double t, double p):
    result = Ca1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_beta(double t, double p):
    result = Ca1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_K(double t, double p):
    result = Ca1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_calib_Kp(double t, double p):
    result = Ca1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Ca1_gas_species_t_get_param_number():
    result = Ca1_gas_species_t_get_param_number()
    return result
def cy_Ca1_gas_species_t_get_param_names():
    cdef const char **names = Ca1_gas_species_t_get_param_names()
    n = Ca1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Ca1_gas_species_t_get_param_units():
    cdef const char **units = Ca1_gas_species_t_get_param_units()
    n = Ca1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Ca1_gas_species_t_get_param_values():
    n = Ca1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Ca1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Ca1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Ca1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Ca1_gas_species_t_get_param_value(int index):
    result = Ca1_gas_species_t_get_param_value(<int> index)
    return result
def cy_Ca1_gas_species_t_set_param_value(int index, double value):
    result = Ca1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Ca1_gas_species_t_dparam_g(double t, double p, int index):
    result = Ca1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Ca1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Ca1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Ca1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Ca1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Ca1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Ca1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Ca1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Ca1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Ca1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Ca1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Ca1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Ca1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Ca1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Ca1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Ca1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Ca1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Ca1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Ca1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Al2O1_gas_species_t_calib.h":
    const char *Al2O1_gas_species_t_calib_identifier();
    const char *Al2O1_gas_species_t_calib_name();
    const char *Al2O1_gas_species_t_calib_formula();
    const double Al2O1_gas_species_t_calib_mw();
    const double *Al2O1_gas_species_t_calib_elements();
    double Al2O1_gas_species_t_calib_g(double t, double p);
    double Al2O1_gas_species_t_calib_dgdt(double t, double p);
    double Al2O1_gas_species_t_calib_dgdp(double t, double p);
    double Al2O1_gas_species_t_calib_d2gdt2(double t, double p);
    double Al2O1_gas_species_t_calib_d2gdtdp(double t, double p);
    double Al2O1_gas_species_t_calib_d2gdp2(double t, double p);
    double Al2O1_gas_species_t_calib_d3gdt3(double t, double p);
    double Al2O1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Al2O1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Al2O1_gas_species_t_calib_d3gdp3(double t, double p);
    double Al2O1_gas_species_t_calib_s(double t, double p);
    double Al2O1_gas_species_t_calib_v(double t, double p);
    double Al2O1_gas_species_t_calib_cv(double t, double p);
    double Al2O1_gas_species_t_calib_cp(double t, double p);
    double Al2O1_gas_species_t_calib_dcpdt(double t, double p);
    double Al2O1_gas_species_t_calib_alpha(double t, double p);
    double Al2O1_gas_species_t_calib_beta(double t, double p);
    double Al2O1_gas_species_t_calib_K(double t, double p);
    double Al2O1_gas_species_t_calib_Kp(double t, double p);
    int Al2O1_gas_species_t_get_param_number();
    const char **Al2O1_gas_species_t_get_param_names();
    const char **Al2O1_gas_species_t_get_param_units();
    void Al2O1_gas_species_t_get_param_values(double **values);
    int Al2O1_gas_species_t_set_param_values(double *values);
    double Al2O1_gas_species_t_get_param_value(int index);
    int Al2O1_gas_species_t_set_param_value(int index, double value);
    double Al2O1_gas_species_t_dparam_g(double t, double p, int index);
    double Al2O1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Al2O1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Al2O1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Al2O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Al2O1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Al2O1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Al2O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Al2O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Al2O1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Al2O1_gas_species_t_calib_identifier():
    result = <bytes> Al2O1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Al2O1_gas_species_t_calib_name():
    result = <bytes> Al2O1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Al2O1_gas_species_t_calib_formula():
    result = <bytes> Al2O1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Al2O1_gas_species_t_calib_mw():
    result = Al2O1_gas_species_t_calib_mw()
    return result
def cy_Al2O1_gas_species_t_calib_elements():
    cdef const double *e = Al2O1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Al2O1_gas_species_t_calib_g(double t, double p):
    result = Al2O1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_dgdt(double t, double p):
    result = Al2O1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_dgdp(double t, double p):
    result = Al2O1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_d2gdt2(double t, double p):
    result = Al2O1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Al2O1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_d2gdp2(double t, double p):
    result = Al2O1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_d3gdt3(double t, double p):
    result = Al2O1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Al2O1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Al2O1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_d3gdp3(double t, double p):
    result = Al2O1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_s(double t, double p):
    result = Al2O1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_v(double t, double p):
    result = Al2O1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_cv(double t, double p):
    result = Al2O1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_cp(double t, double p):
    result = Al2O1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_dcpdt(double t, double p):
    result = Al2O1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_alpha(double t, double p):
    result = Al2O1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_beta(double t, double p):
    result = Al2O1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_K(double t, double p):
    result = Al2O1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_calib_Kp(double t, double p):
    result = Al2O1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Al2O1_gas_species_t_get_param_number():
    result = Al2O1_gas_species_t_get_param_number()
    return result
def cy_Al2O1_gas_species_t_get_param_names():
    cdef const char **names = Al2O1_gas_species_t_get_param_names()
    n = Al2O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al2O1_gas_species_t_get_param_units():
    cdef const char **units = Al2O1_gas_species_t_get_param_units()
    n = Al2O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al2O1_gas_species_t_get_param_values():
    n = Al2O1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Al2O1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Al2O1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Al2O1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Al2O1_gas_species_t_get_param_value(int index):
    result = Al2O1_gas_species_t_get_param_value(<int> index)
    return result
def cy_Al2O1_gas_species_t_set_param_value(int index, double value):
    result = Al2O1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Al2O1_gas_species_t_dparam_g(double t, double p, int index):
    result = Al2O1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Al2O1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Al2O1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Al2O1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Al2O1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Al2O1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Al2O1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Al2O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Al2O1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Al2O1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Al2O1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al2O1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Al2O1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Al2O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Al2O1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Al2O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Al2O1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al2O1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Al2O1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "He1_gas_species_t_calib.h":
    const char *He1_gas_species_t_calib_identifier();
    const char *He1_gas_species_t_calib_name();
    const char *He1_gas_species_t_calib_formula();
    const double He1_gas_species_t_calib_mw();
    const double *He1_gas_species_t_calib_elements();
    double He1_gas_species_t_calib_g(double t, double p);
    double He1_gas_species_t_calib_dgdt(double t, double p);
    double He1_gas_species_t_calib_dgdp(double t, double p);
    double He1_gas_species_t_calib_d2gdt2(double t, double p);
    double He1_gas_species_t_calib_d2gdtdp(double t, double p);
    double He1_gas_species_t_calib_d2gdp2(double t, double p);
    double He1_gas_species_t_calib_d3gdt3(double t, double p);
    double He1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double He1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double He1_gas_species_t_calib_d3gdp3(double t, double p);
    double He1_gas_species_t_calib_s(double t, double p);
    double He1_gas_species_t_calib_v(double t, double p);
    double He1_gas_species_t_calib_cv(double t, double p);
    double He1_gas_species_t_calib_cp(double t, double p);
    double He1_gas_species_t_calib_dcpdt(double t, double p);
    double He1_gas_species_t_calib_alpha(double t, double p);
    double He1_gas_species_t_calib_beta(double t, double p);
    double He1_gas_species_t_calib_K(double t, double p);
    double He1_gas_species_t_calib_Kp(double t, double p);
    int He1_gas_species_t_get_param_number();
    const char **He1_gas_species_t_get_param_names();
    const char **He1_gas_species_t_get_param_units();
    void He1_gas_species_t_get_param_values(double **values);
    int He1_gas_species_t_set_param_values(double *values);
    double He1_gas_species_t_get_param_value(int index);
    int He1_gas_species_t_set_param_value(int index, double value);
    double He1_gas_species_t_dparam_g(double t, double p, int index);
    double He1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double He1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double He1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double He1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double He1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double He1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double He1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double He1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double He1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_He1_gas_species_t_calib_identifier():
    result = <bytes> He1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_He1_gas_species_t_calib_name():
    result = <bytes> He1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_He1_gas_species_t_calib_formula():
    result = <bytes> He1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_He1_gas_species_t_calib_mw():
    result = He1_gas_species_t_calib_mw()
    return result
def cy_He1_gas_species_t_calib_elements():
    cdef const double *e = He1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_He1_gas_species_t_calib_g(double t, double p):
    result = He1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_dgdt(double t, double p):
    result = He1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_dgdp(double t, double p):
    result = He1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_d2gdt2(double t, double p):
    result = He1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = He1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_d2gdp2(double t, double p):
    result = He1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_d3gdt3(double t, double p):
    result = He1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = He1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = He1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_d3gdp3(double t, double p):
    result = He1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_s(double t, double p):
    result = He1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_v(double t, double p):
    result = He1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_cv(double t, double p):
    result = He1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_cp(double t, double p):
    result = He1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_dcpdt(double t, double p):
    result = He1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_alpha(double t, double p):
    result = He1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_beta(double t, double p):
    result = He1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_K(double t, double p):
    result = He1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_calib_Kp(double t, double p):
    result = He1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_He1_gas_species_t_get_param_number():
    result = He1_gas_species_t_get_param_number()
    return result
def cy_He1_gas_species_t_get_param_names():
    cdef const char **names = He1_gas_species_t_get_param_names()
    n = He1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_He1_gas_species_t_get_param_units():
    cdef const char **units = He1_gas_species_t_get_param_units()
    n = He1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_He1_gas_species_t_get_param_values():
    n = He1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    He1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_He1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = He1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_He1_gas_species_t_get_param_value(int index):
    result = He1_gas_species_t_get_param_value(<int> index)
    return result
def cy_He1_gas_species_t_set_param_value(int index, double value):
    result = He1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_He1_gas_species_t_dparam_g(double t, double p, int index):
    result = He1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_He1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = He1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_He1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = He1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_He1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = He1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_He1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = He1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_He1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = He1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_He1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = He1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_He1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = He1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_He1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = He1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_He1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = He1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Ca1H1O1_gas_species_t_calib.h":
    const char *Ca1H1O1_gas_species_t_calib_identifier();
    const char *Ca1H1O1_gas_species_t_calib_name();
    const char *Ca1H1O1_gas_species_t_calib_formula();
    const double Ca1H1O1_gas_species_t_calib_mw();
    const double *Ca1H1O1_gas_species_t_calib_elements();
    double Ca1H1O1_gas_species_t_calib_g(double t, double p);
    double Ca1H1O1_gas_species_t_calib_dgdt(double t, double p);
    double Ca1H1O1_gas_species_t_calib_dgdp(double t, double p);
    double Ca1H1O1_gas_species_t_calib_d2gdt2(double t, double p);
    double Ca1H1O1_gas_species_t_calib_d2gdtdp(double t, double p);
    double Ca1H1O1_gas_species_t_calib_d2gdp2(double t, double p);
    double Ca1H1O1_gas_species_t_calib_d3gdt3(double t, double p);
    double Ca1H1O1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Ca1H1O1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Ca1H1O1_gas_species_t_calib_d3gdp3(double t, double p);
    double Ca1H1O1_gas_species_t_calib_s(double t, double p);
    double Ca1H1O1_gas_species_t_calib_v(double t, double p);
    double Ca1H1O1_gas_species_t_calib_cv(double t, double p);
    double Ca1H1O1_gas_species_t_calib_cp(double t, double p);
    double Ca1H1O1_gas_species_t_calib_dcpdt(double t, double p);
    double Ca1H1O1_gas_species_t_calib_alpha(double t, double p);
    double Ca1H1O1_gas_species_t_calib_beta(double t, double p);
    double Ca1H1O1_gas_species_t_calib_K(double t, double p);
    double Ca1H1O1_gas_species_t_calib_Kp(double t, double p);
    int Ca1H1O1_gas_species_t_get_param_number();
    const char **Ca1H1O1_gas_species_t_get_param_names();
    const char **Ca1H1O1_gas_species_t_get_param_units();
    void Ca1H1O1_gas_species_t_get_param_values(double **values);
    int Ca1H1O1_gas_species_t_set_param_values(double *values);
    double Ca1H1O1_gas_species_t_get_param_value(int index);
    int Ca1H1O1_gas_species_t_set_param_value(int index, double value);
    double Ca1H1O1_gas_species_t_dparam_g(double t, double p, int index);
    double Ca1H1O1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Ca1H1O1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Ca1H1O1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Ca1H1O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Ca1H1O1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Ca1H1O1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Ca1H1O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Ca1H1O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Ca1H1O1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Ca1H1O1_gas_species_t_calib_identifier():
    result = <bytes> Ca1H1O1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Ca1H1O1_gas_species_t_calib_name():
    result = <bytes> Ca1H1O1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Ca1H1O1_gas_species_t_calib_formula():
    result = <bytes> Ca1H1O1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Ca1H1O1_gas_species_t_calib_mw():
    result = Ca1H1O1_gas_species_t_calib_mw()
    return result
def cy_Ca1H1O1_gas_species_t_calib_elements():
    cdef const double *e = Ca1H1O1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Ca1H1O1_gas_species_t_calib_g(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_dgdt(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_dgdp(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_d2gdt2(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_d2gdp2(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_d3gdt3(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_d3gdp3(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_s(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_v(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_cv(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_cp(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_dcpdt(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_alpha(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_beta(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_K(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_calib_Kp(double t, double p):
    result = Ca1H1O1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Ca1H1O1_gas_species_t_get_param_number():
    result = Ca1H1O1_gas_species_t_get_param_number()
    return result
def cy_Ca1H1O1_gas_species_t_get_param_names():
    cdef const char **names = Ca1H1O1_gas_species_t_get_param_names()
    n = Ca1H1O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Ca1H1O1_gas_species_t_get_param_units():
    cdef const char **units = Ca1H1O1_gas_species_t_get_param_units()
    n = Ca1H1O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Ca1H1O1_gas_species_t_get_param_values():
    n = Ca1H1O1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Ca1H1O1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Ca1H1O1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Ca1H1O1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Ca1H1O1_gas_species_t_get_param_value(int index):
    result = Ca1H1O1_gas_species_t_get_param_value(<int> index)
    return result
def cy_Ca1H1O1_gas_species_t_set_param_value(int index, double value):
    result = Ca1H1O1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Ca1H1O1_gas_species_t_dparam_g(double t, double p, int index):
    result = Ca1H1O1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H1O1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Ca1H1O1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H1O1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Ca1H1O1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H1O1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Ca1H1O1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H1O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Ca1H1O1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H1O1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Ca1H1O1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H1O1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Ca1H1O1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H1O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Ca1H1O1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H1O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Ca1H1O1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Ca1H1O1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Ca1H1O1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Al2O2_gas_species_t_calib.h":
    const char *Al2O2_gas_species_t_calib_identifier();
    const char *Al2O2_gas_species_t_calib_name();
    const char *Al2O2_gas_species_t_calib_formula();
    const double Al2O2_gas_species_t_calib_mw();
    const double *Al2O2_gas_species_t_calib_elements();
    double Al2O2_gas_species_t_calib_g(double t, double p);
    double Al2O2_gas_species_t_calib_dgdt(double t, double p);
    double Al2O2_gas_species_t_calib_dgdp(double t, double p);
    double Al2O2_gas_species_t_calib_d2gdt2(double t, double p);
    double Al2O2_gas_species_t_calib_d2gdtdp(double t, double p);
    double Al2O2_gas_species_t_calib_d2gdp2(double t, double p);
    double Al2O2_gas_species_t_calib_d3gdt3(double t, double p);
    double Al2O2_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Al2O2_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Al2O2_gas_species_t_calib_d3gdp3(double t, double p);
    double Al2O2_gas_species_t_calib_s(double t, double p);
    double Al2O2_gas_species_t_calib_v(double t, double p);
    double Al2O2_gas_species_t_calib_cv(double t, double p);
    double Al2O2_gas_species_t_calib_cp(double t, double p);
    double Al2O2_gas_species_t_calib_dcpdt(double t, double p);
    double Al2O2_gas_species_t_calib_alpha(double t, double p);
    double Al2O2_gas_species_t_calib_beta(double t, double p);
    double Al2O2_gas_species_t_calib_K(double t, double p);
    double Al2O2_gas_species_t_calib_Kp(double t, double p);
    int Al2O2_gas_species_t_get_param_number();
    const char **Al2O2_gas_species_t_get_param_names();
    const char **Al2O2_gas_species_t_get_param_units();
    void Al2O2_gas_species_t_get_param_values(double **values);
    int Al2O2_gas_species_t_set_param_values(double *values);
    double Al2O2_gas_species_t_get_param_value(int index);
    int Al2O2_gas_species_t_set_param_value(int index, double value);
    double Al2O2_gas_species_t_dparam_g(double t, double p, int index);
    double Al2O2_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Al2O2_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Al2O2_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Al2O2_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Al2O2_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Al2O2_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Al2O2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Al2O2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Al2O2_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Al2O2_gas_species_t_calib_identifier():
    result = <bytes> Al2O2_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Al2O2_gas_species_t_calib_name():
    result = <bytes> Al2O2_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Al2O2_gas_species_t_calib_formula():
    result = <bytes> Al2O2_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Al2O2_gas_species_t_calib_mw():
    result = Al2O2_gas_species_t_calib_mw()
    return result
def cy_Al2O2_gas_species_t_calib_elements():
    cdef const double *e = Al2O2_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Al2O2_gas_species_t_calib_g(double t, double p):
    result = Al2O2_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_dgdt(double t, double p):
    result = Al2O2_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_dgdp(double t, double p):
    result = Al2O2_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_d2gdt2(double t, double p):
    result = Al2O2_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Al2O2_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_d2gdp2(double t, double p):
    result = Al2O2_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_d3gdt3(double t, double p):
    result = Al2O2_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Al2O2_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Al2O2_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_d3gdp3(double t, double p):
    result = Al2O2_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_s(double t, double p):
    result = Al2O2_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_v(double t, double p):
    result = Al2O2_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_cv(double t, double p):
    result = Al2O2_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_cp(double t, double p):
    result = Al2O2_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_dcpdt(double t, double p):
    result = Al2O2_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_alpha(double t, double p):
    result = Al2O2_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_beta(double t, double p):
    result = Al2O2_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_K(double t, double p):
    result = Al2O2_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_calib_Kp(double t, double p):
    result = Al2O2_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Al2O2_gas_species_t_get_param_number():
    result = Al2O2_gas_species_t_get_param_number()
    return result
def cy_Al2O2_gas_species_t_get_param_names():
    cdef const char **names = Al2O2_gas_species_t_get_param_names()
    n = Al2O2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al2O2_gas_species_t_get_param_units():
    cdef const char **units = Al2O2_gas_species_t_get_param_units()
    n = Al2O2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al2O2_gas_species_t_get_param_values():
    n = Al2O2_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Al2O2_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Al2O2_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Al2O2_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Al2O2_gas_species_t_get_param_value(int index):
    result = Al2O2_gas_species_t_get_param_value(<int> index)
    return result
def cy_Al2O2_gas_species_t_set_param_value(int index, double value):
    result = Al2O2_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Al2O2_gas_species_t_dparam_g(double t, double p, int index):
    result = Al2O2_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Al2O2_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Al2O2_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Al2O2_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Al2O2_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Al2O2_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Al2O2_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Al2O2_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Al2O2_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Al2O2_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Al2O2_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al2O2_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Al2O2_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Al2O2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Al2O2_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Al2O2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Al2O2_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al2O2_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Al2O2_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Al1H1O1_057_gas_species_t_calib.h":
    const char *Al1H1O1_057_gas_species_t_calib_identifier();
    const char *Al1H1O1_057_gas_species_t_calib_name();
    const char *Al1H1O1_057_gas_species_t_calib_formula();
    const double Al1H1O1_057_gas_species_t_calib_mw();
    const double *Al1H1O1_057_gas_species_t_calib_elements();
    double Al1H1O1_057_gas_species_t_calib_g(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_dgdt(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_dgdp(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_d2gdt2(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_d2gdtdp(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_d2gdp2(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_d3gdt3(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_d3gdp3(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_s(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_v(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_cv(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_cp(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_dcpdt(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_alpha(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_beta(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_K(double t, double p);
    double Al1H1O1_057_gas_species_t_calib_Kp(double t, double p);
    int Al1H1O1_057_gas_species_t_get_param_number();
    const char **Al1H1O1_057_gas_species_t_get_param_names();
    const char **Al1H1O1_057_gas_species_t_get_param_units();
    void Al1H1O1_057_gas_species_t_get_param_values(double **values);
    int Al1H1O1_057_gas_species_t_set_param_values(double *values);
    double Al1H1O1_057_gas_species_t_get_param_value(int index);
    int Al1H1O1_057_gas_species_t_set_param_value(int index, double value);
    double Al1H1O1_057_gas_species_t_dparam_g(double t, double p, int index);
    double Al1H1O1_057_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Al1H1O1_057_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Al1H1O1_057_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Al1H1O1_057_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Al1H1O1_057_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Al1H1O1_057_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Al1H1O1_057_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Al1H1O1_057_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Al1H1O1_057_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Al1H1O1_057_gas_species_t_calib_identifier():
    result = <bytes> Al1H1O1_057_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Al1H1O1_057_gas_species_t_calib_name():
    result = <bytes> Al1H1O1_057_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Al1H1O1_057_gas_species_t_calib_formula():
    result = <bytes> Al1H1O1_057_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Al1H1O1_057_gas_species_t_calib_mw():
    result = Al1H1O1_057_gas_species_t_calib_mw()
    return result
def cy_Al1H1O1_057_gas_species_t_calib_elements():
    cdef const double *e = Al1H1O1_057_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Al1H1O1_057_gas_species_t_calib_g(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_dgdt(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_dgdp(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_d2gdt2(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_d2gdp2(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_d3gdt3(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_d3gdp3(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_s(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_v(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_cv(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_cp(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_dcpdt(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_alpha(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_beta(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_K(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_calib_Kp(double t, double p):
    result = Al1H1O1_057_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Al1H1O1_057_gas_species_t_get_param_number():
    result = Al1H1O1_057_gas_species_t_get_param_number()
    return result
def cy_Al1H1O1_057_gas_species_t_get_param_names():
    cdef const char **names = Al1H1O1_057_gas_species_t_get_param_names()
    n = Al1H1O1_057_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al1H1O1_057_gas_species_t_get_param_units():
    cdef const char **units = Al1H1O1_057_gas_species_t_get_param_units()
    n = Al1H1O1_057_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al1H1O1_057_gas_species_t_get_param_values():
    n = Al1H1O1_057_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Al1H1O1_057_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Al1H1O1_057_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Al1H1O1_057_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Al1H1O1_057_gas_species_t_get_param_value(int index):
    result = Al1H1O1_057_gas_species_t_get_param_value(<int> index)
    return result
def cy_Al1H1O1_057_gas_species_t_set_param_value(int index, double value):
    result = Al1H1O1_057_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Al1H1O1_057_gas_species_t_dparam_g(double t, double p, int index):
    result = Al1H1O1_057_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_057_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Al1H1O1_057_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_057_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Al1H1O1_057_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_057_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Al1H1O1_057_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_057_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Al1H1O1_057_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_057_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Al1H1O1_057_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_057_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Al1H1O1_057_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_057_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Al1H1O1_057_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_057_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Al1H1O1_057_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O1_057_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Al1H1O1_057_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Ti1_gas_species_t_calib.h":
    const char *Ti1_gas_species_t_calib_identifier();
    const char *Ti1_gas_species_t_calib_name();
    const char *Ti1_gas_species_t_calib_formula();
    const double Ti1_gas_species_t_calib_mw();
    const double *Ti1_gas_species_t_calib_elements();
    double Ti1_gas_species_t_calib_g(double t, double p);
    double Ti1_gas_species_t_calib_dgdt(double t, double p);
    double Ti1_gas_species_t_calib_dgdp(double t, double p);
    double Ti1_gas_species_t_calib_d2gdt2(double t, double p);
    double Ti1_gas_species_t_calib_d2gdtdp(double t, double p);
    double Ti1_gas_species_t_calib_d2gdp2(double t, double p);
    double Ti1_gas_species_t_calib_d3gdt3(double t, double p);
    double Ti1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Ti1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Ti1_gas_species_t_calib_d3gdp3(double t, double p);
    double Ti1_gas_species_t_calib_s(double t, double p);
    double Ti1_gas_species_t_calib_v(double t, double p);
    double Ti1_gas_species_t_calib_cv(double t, double p);
    double Ti1_gas_species_t_calib_cp(double t, double p);
    double Ti1_gas_species_t_calib_dcpdt(double t, double p);
    double Ti1_gas_species_t_calib_alpha(double t, double p);
    double Ti1_gas_species_t_calib_beta(double t, double p);
    double Ti1_gas_species_t_calib_K(double t, double p);
    double Ti1_gas_species_t_calib_Kp(double t, double p);
    int Ti1_gas_species_t_get_param_number();
    const char **Ti1_gas_species_t_get_param_names();
    const char **Ti1_gas_species_t_get_param_units();
    void Ti1_gas_species_t_get_param_values(double **values);
    int Ti1_gas_species_t_set_param_values(double *values);
    double Ti1_gas_species_t_get_param_value(int index);
    int Ti1_gas_species_t_set_param_value(int index, double value);
    double Ti1_gas_species_t_dparam_g(double t, double p, int index);
    double Ti1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Ti1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Ti1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Ti1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Ti1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Ti1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Ti1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Ti1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Ti1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Ti1_gas_species_t_calib_identifier():
    result = <bytes> Ti1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Ti1_gas_species_t_calib_name():
    result = <bytes> Ti1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Ti1_gas_species_t_calib_formula():
    result = <bytes> Ti1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Ti1_gas_species_t_calib_mw():
    result = Ti1_gas_species_t_calib_mw()
    return result
def cy_Ti1_gas_species_t_calib_elements():
    cdef const double *e = Ti1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Ti1_gas_species_t_calib_g(double t, double p):
    result = Ti1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_dgdt(double t, double p):
    result = Ti1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_dgdp(double t, double p):
    result = Ti1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_d2gdt2(double t, double p):
    result = Ti1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Ti1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_d2gdp2(double t, double p):
    result = Ti1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_d3gdt3(double t, double p):
    result = Ti1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Ti1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Ti1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_d3gdp3(double t, double p):
    result = Ti1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_s(double t, double p):
    result = Ti1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_v(double t, double p):
    result = Ti1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_cv(double t, double p):
    result = Ti1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_cp(double t, double p):
    result = Ti1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_dcpdt(double t, double p):
    result = Ti1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_alpha(double t, double p):
    result = Ti1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_beta(double t, double p):
    result = Ti1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_K(double t, double p):
    result = Ti1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_calib_Kp(double t, double p):
    result = Ti1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Ti1_gas_species_t_get_param_number():
    result = Ti1_gas_species_t_get_param_number()
    return result
def cy_Ti1_gas_species_t_get_param_names():
    cdef const char **names = Ti1_gas_species_t_get_param_names()
    n = Ti1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Ti1_gas_species_t_get_param_units():
    cdef const char **units = Ti1_gas_species_t_get_param_units()
    n = Ti1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Ti1_gas_species_t_get_param_values():
    n = Ti1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Ti1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Ti1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Ti1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Ti1_gas_species_t_get_param_value(int index):
    result = Ti1_gas_species_t_get_param_value(<int> index)
    return result
def cy_Ti1_gas_species_t_set_param_value(int index, double value):
    result = Ti1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Ti1_gas_species_t_dparam_g(double t, double p, int index):
    result = Ti1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Ti1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Ti1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Ti1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Ti1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Ti1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Ti1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Ti1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Ti1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Ti1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Ti1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Ti1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Ti1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Ti1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Ti1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Ti1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Ti1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Ti1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Ti1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "H1O1_gas_species_t_calib.h":
    const char *H1O1_gas_species_t_calib_identifier();
    const char *H1O1_gas_species_t_calib_name();
    const char *H1O1_gas_species_t_calib_formula();
    const double H1O1_gas_species_t_calib_mw();
    const double *H1O1_gas_species_t_calib_elements();
    double H1O1_gas_species_t_calib_g(double t, double p);
    double H1O1_gas_species_t_calib_dgdt(double t, double p);
    double H1O1_gas_species_t_calib_dgdp(double t, double p);
    double H1O1_gas_species_t_calib_d2gdt2(double t, double p);
    double H1O1_gas_species_t_calib_d2gdtdp(double t, double p);
    double H1O1_gas_species_t_calib_d2gdp2(double t, double p);
    double H1O1_gas_species_t_calib_d3gdt3(double t, double p);
    double H1O1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double H1O1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double H1O1_gas_species_t_calib_d3gdp3(double t, double p);
    double H1O1_gas_species_t_calib_s(double t, double p);
    double H1O1_gas_species_t_calib_v(double t, double p);
    double H1O1_gas_species_t_calib_cv(double t, double p);
    double H1O1_gas_species_t_calib_cp(double t, double p);
    double H1O1_gas_species_t_calib_dcpdt(double t, double p);
    double H1O1_gas_species_t_calib_alpha(double t, double p);
    double H1O1_gas_species_t_calib_beta(double t, double p);
    double H1O1_gas_species_t_calib_K(double t, double p);
    double H1O1_gas_species_t_calib_Kp(double t, double p);
    int H1O1_gas_species_t_get_param_number();
    const char **H1O1_gas_species_t_get_param_names();
    const char **H1O1_gas_species_t_get_param_units();
    void H1O1_gas_species_t_get_param_values(double **values);
    int H1O1_gas_species_t_set_param_values(double *values);
    double H1O1_gas_species_t_get_param_value(int index);
    int H1O1_gas_species_t_set_param_value(int index, double value);
    double H1O1_gas_species_t_dparam_g(double t, double p, int index);
    double H1O1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double H1O1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double H1O1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double H1O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double H1O1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double H1O1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double H1O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double H1O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double H1O1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_H1O1_gas_species_t_calib_identifier():
    result = <bytes> H1O1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_H1O1_gas_species_t_calib_name():
    result = <bytes> H1O1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_H1O1_gas_species_t_calib_formula():
    result = <bytes> H1O1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_H1O1_gas_species_t_calib_mw():
    result = H1O1_gas_species_t_calib_mw()
    return result
def cy_H1O1_gas_species_t_calib_elements():
    cdef const double *e = H1O1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_H1O1_gas_species_t_calib_g(double t, double p):
    result = H1O1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_dgdt(double t, double p):
    result = H1O1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_dgdp(double t, double p):
    result = H1O1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_d2gdt2(double t, double p):
    result = H1O1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = H1O1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_d2gdp2(double t, double p):
    result = H1O1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_d3gdt3(double t, double p):
    result = H1O1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = H1O1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = H1O1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_d3gdp3(double t, double p):
    result = H1O1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_s(double t, double p):
    result = H1O1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_v(double t, double p):
    result = H1O1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_cv(double t, double p):
    result = H1O1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_cp(double t, double p):
    result = H1O1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_dcpdt(double t, double p):
    result = H1O1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_alpha(double t, double p):
    result = H1O1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_beta(double t, double p):
    result = H1O1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_K(double t, double p):
    result = H1O1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_calib_Kp(double t, double p):
    result = H1O1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_H1O1_gas_species_t_get_param_number():
    result = H1O1_gas_species_t_get_param_number()
    return result
def cy_H1O1_gas_species_t_get_param_names():
    cdef const char **names = H1O1_gas_species_t_get_param_names()
    n = H1O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H1O1_gas_species_t_get_param_units():
    cdef const char **units = H1O1_gas_species_t_get_param_units()
    n = H1O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H1O1_gas_species_t_get_param_values():
    n = H1O1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    H1O1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_H1O1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = H1O1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_H1O1_gas_species_t_get_param_value(int index):
    result = H1O1_gas_species_t_get_param_value(<int> index)
    return result
def cy_H1O1_gas_species_t_set_param_value(int index, double value):
    result = H1O1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_H1O1_gas_species_t_dparam_g(double t, double p, int index):
    result = H1O1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_H1O1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = H1O1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_H1O1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = H1O1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_H1O1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = H1O1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_H1O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = H1O1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_H1O1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = H1O1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_H1O1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = H1O1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_H1O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = H1O1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_H1O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = H1O1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_H1O1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = H1O1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Al1O2_gas_species_t_calib.h":
    const char *Al1O2_gas_species_t_calib_identifier();
    const char *Al1O2_gas_species_t_calib_name();
    const char *Al1O2_gas_species_t_calib_formula();
    const double Al1O2_gas_species_t_calib_mw();
    const double *Al1O2_gas_species_t_calib_elements();
    double Al1O2_gas_species_t_calib_g(double t, double p);
    double Al1O2_gas_species_t_calib_dgdt(double t, double p);
    double Al1O2_gas_species_t_calib_dgdp(double t, double p);
    double Al1O2_gas_species_t_calib_d2gdt2(double t, double p);
    double Al1O2_gas_species_t_calib_d2gdtdp(double t, double p);
    double Al1O2_gas_species_t_calib_d2gdp2(double t, double p);
    double Al1O2_gas_species_t_calib_d3gdt3(double t, double p);
    double Al1O2_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Al1O2_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Al1O2_gas_species_t_calib_d3gdp3(double t, double p);
    double Al1O2_gas_species_t_calib_s(double t, double p);
    double Al1O2_gas_species_t_calib_v(double t, double p);
    double Al1O2_gas_species_t_calib_cv(double t, double p);
    double Al1O2_gas_species_t_calib_cp(double t, double p);
    double Al1O2_gas_species_t_calib_dcpdt(double t, double p);
    double Al1O2_gas_species_t_calib_alpha(double t, double p);
    double Al1O2_gas_species_t_calib_beta(double t, double p);
    double Al1O2_gas_species_t_calib_K(double t, double p);
    double Al1O2_gas_species_t_calib_Kp(double t, double p);
    int Al1O2_gas_species_t_get_param_number();
    const char **Al1O2_gas_species_t_get_param_names();
    const char **Al1O2_gas_species_t_get_param_units();
    void Al1O2_gas_species_t_get_param_values(double **values);
    int Al1O2_gas_species_t_set_param_values(double *values);
    double Al1O2_gas_species_t_get_param_value(int index);
    int Al1O2_gas_species_t_set_param_value(int index, double value);
    double Al1O2_gas_species_t_dparam_g(double t, double p, int index);
    double Al1O2_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Al1O2_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Al1O2_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Al1O2_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Al1O2_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Al1O2_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Al1O2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Al1O2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Al1O2_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Al1O2_gas_species_t_calib_identifier():
    result = <bytes> Al1O2_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Al1O2_gas_species_t_calib_name():
    result = <bytes> Al1O2_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Al1O2_gas_species_t_calib_formula():
    result = <bytes> Al1O2_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Al1O2_gas_species_t_calib_mw():
    result = Al1O2_gas_species_t_calib_mw()
    return result
def cy_Al1O2_gas_species_t_calib_elements():
    cdef const double *e = Al1O2_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Al1O2_gas_species_t_calib_g(double t, double p):
    result = Al1O2_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_dgdt(double t, double p):
    result = Al1O2_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_dgdp(double t, double p):
    result = Al1O2_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_d2gdt2(double t, double p):
    result = Al1O2_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Al1O2_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_d2gdp2(double t, double p):
    result = Al1O2_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_d3gdt3(double t, double p):
    result = Al1O2_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Al1O2_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Al1O2_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_d3gdp3(double t, double p):
    result = Al1O2_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_s(double t, double p):
    result = Al1O2_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_v(double t, double p):
    result = Al1O2_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_cv(double t, double p):
    result = Al1O2_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_cp(double t, double p):
    result = Al1O2_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_dcpdt(double t, double p):
    result = Al1O2_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_alpha(double t, double p):
    result = Al1O2_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_beta(double t, double p):
    result = Al1O2_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_K(double t, double p):
    result = Al1O2_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_calib_Kp(double t, double p):
    result = Al1O2_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Al1O2_gas_species_t_get_param_number():
    result = Al1O2_gas_species_t_get_param_number()
    return result
def cy_Al1O2_gas_species_t_get_param_names():
    cdef const char **names = Al1O2_gas_species_t_get_param_names()
    n = Al1O2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al1O2_gas_species_t_get_param_units():
    cdef const char **units = Al1O2_gas_species_t_get_param_units()
    n = Al1O2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al1O2_gas_species_t_get_param_values():
    n = Al1O2_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Al1O2_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Al1O2_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Al1O2_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Al1O2_gas_species_t_get_param_value(int index):
    result = Al1O2_gas_species_t_get_param_value(<int> index)
    return result
def cy_Al1O2_gas_species_t_set_param_value(int index, double value):
    result = Al1O2_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Al1O2_gas_species_t_dparam_g(double t, double p, int index):
    result = Al1O2_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Al1O2_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Al1O2_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Al1O2_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Al1O2_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Al1O2_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Al1O2_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Al1O2_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Al1O2_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Al1O2_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Al1O2_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al1O2_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Al1O2_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Al1O2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Al1O2_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Al1O2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Al1O2_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al1O2_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Al1O2_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Mg1O1_gas_species_t_calib.h":
    const char *Mg1O1_gas_species_t_calib_identifier();
    const char *Mg1O1_gas_species_t_calib_name();
    const char *Mg1O1_gas_species_t_calib_formula();
    const double Mg1O1_gas_species_t_calib_mw();
    const double *Mg1O1_gas_species_t_calib_elements();
    double Mg1O1_gas_species_t_calib_g(double t, double p);
    double Mg1O1_gas_species_t_calib_dgdt(double t, double p);
    double Mg1O1_gas_species_t_calib_dgdp(double t, double p);
    double Mg1O1_gas_species_t_calib_d2gdt2(double t, double p);
    double Mg1O1_gas_species_t_calib_d2gdtdp(double t, double p);
    double Mg1O1_gas_species_t_calib_d2gdp2(double t, double p);
    double Mg1O1_gas_species_t_calib_d3gdt3(double t, double p);
    double Mg1O1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Mg1O1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Mg1O1_gas_species_t_calib_d3gdp3(double t, double p);
    double Mg1O1_gas_species_t_calib_s(double t, double p);
    double Mg1O1_gas_species_t_calib_v(double t, double p);
    double Mg1O1_gas_species_t_calib_cv(double t, double p);
    double Mg1O1_gas_species_t_calib_cp(double t, double p);
    double Mg1O1_gas_species_t_calib_dcpdt(double t, double p);
    double Mg1O1_gas_species_t_calib_alpha(double t, double p);
    double Mg1O1_gas_species_t_calib_beta(double t, double p);
    double Mg1O1_gas_species_t_calib_K(double t, double p);
    double Mg1O1_gas_species_t_calib_Kp(double t, double p);
    int Mg1O1_gas_species_t_get_param_number();
    const char **Mg1O1_gas_species_t_get_param_names();
    const char **Mg1O1_gas_species_t_get_param_units();
    void Mg1O1_gas_species_t_get_param_values(double **values);
    int Mg1O1_gas_species_t_set_param_values(double *values);
    double Mg1O1_gas_species_t_get_param_value(int index);
    int Mg1O1_gas_species_t_set_param_value(int index, double value);
    double Mg1O1_gas_species_t_dparam_g(double t, double p, int index);
    double Mg1O1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Mg1O1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Mg1O1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Mg1O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Mg1O1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Mg1O1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Mg1O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Mg1O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Mg1O1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Mg1O1_gas_species_t_calib_identifier():
    result = <bytes> Mg1O1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Mg1O1_gas_species_t_calib_name():
    result = <bytes> Mg1O1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Mg1O1_gas_species_t_calib_formula():
    result = <bytes> Mg1O1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Mg1O1_gas_species_t_calib_mw():
    result = Mg1O1_gas_species_t_calib_mw()
    return result
def cy_Mg1O1_gas_species_t_calib_elements():
    cdef const double *e = Mg1O1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Mg1O1_gas_species_t_calib_g(double t, double p):
    result = Mg1O1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_dgdt(double t, double p):
    result = Mg1O1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_dgdp(double t, double p):
    result = Mg1O1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_d2gdt2(double t, double p):
    result = Mg1O1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Mg1O1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_d2gdp2(double t, double p):
    result = Mg1O1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_d3gdt3(double t, double p):
    result = Mg1O1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Mg1O1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Mg1O1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_d3gdp3(double t, double p):
    result = Mg1O1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_s(double t, double p):
    result = Mg1O1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_v(double t, double p):
    result = Mg1O1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_cv(double t, double p):
    result = Mg1O1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_cp(double t, double p):
    result = Mg1O1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_dcpdt(double t, double p):
    result = Mg1O1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_alpha(double t, double p):
    result = Mg1O1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_beta(double t, double p):
    result = Mg1O1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_K(double t, double p):
    result = Mg1O1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_calib_Kp(double t, double p):
    result = Mg1O1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Mg1O1_gas_species_t_get_param_number():
    result = Mg1O1_gas_species_t_get_param_number()
    return result
def cy_Mg1O1_gas_species_t_get_param_names():
    cdef const char **names = Mg1O1_gas_species_t_get_param_names()
    n = Mg1O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Mg1O1_gas_species_t_get_param_units():
    cdef const char **units = Mg1O1_gas_species_t_get_param_units()
    n = Mg1O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Mg1O1_gas_species_t_get_param_values():
    n = Mg1O1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Mg1O1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Mg1O1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Mg1O1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Mg1O1_gas_species_t_get_param_value(int index):
    result = Mg1O1_gas_species_t_get_param_value(<int> index)
    return result
def cy_Mg1O1_gas_species_t_set_param_value(int index, double value):
    result = Mg1O1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Mg1O1_gas_species_t_dparam_g(double t, double p, int index):
    result = Mg1O1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Mg1O1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Mg1O1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Mg1O1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Mg1O1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Mg1O1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Mg1O1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Mg1O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Mg1O1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Mg1O1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Mg1O1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Mg1O1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Mg1O1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Mg1O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Mg1O1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Mg1O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Mg1O1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Mg1O1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Mg1O1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Al1H1O2_gas_species_t_calib.h":
    const char *Al1H1O2_gas_species_t_calib_identifier();
    const char *Al1H1O2_gas_species_t_calib_name();
    const char *Al1H1O2_gas_species_t_calib_formula();
    const double Al1H1O2_gas_species_t_calib_mw();
    const double *Al1H1O2_gas_species_t_calib_elements();
    double Al1H1O2_gas_species_t_calib_g(double t, double p);
    double Al1H1O2_gas_species_t_calib_dgdt(double t, double p);
    double Al1H1O2_gas_species_t_calib_dgdp(double t, double p);
    double Al1H1O2_gas_species_t_calib_d2gdt2(double t, double p);
    double Al1H1O2_gas_species_t_calib_d2gdtdp(double t, double p);
    double Al1H1O2_gas_species_t_calib_d2gdp2(double t, double p);
    double Al1H1O2_gas_species_t_calib_d3gdt3(double t, double p);
    double Al1H1O2_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Al1H1O2_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Al1H1O2_gas_species_t_calib_d3gdp3(double t, double p);
    double Al1H1O2_gas_species_t_calib_s(double t, double p);
    double Al1H1O2_gas_species_t_calib_v(double t, double p);
    double Al1H1O2_gas_species_t_calib_cv(double t, double p);
    double Al1H1O2_gas_species_t_calib_cp(double t, double p);
    double Al1H1O2_gas_species_t_calib_dcpdt(double t, double p);
    double Al1H1O2_gas_species_t_calib_alpha(double t, double p);
    double Al1H1O2_gas_species_t_calib_beta(double t, double p);
    double Al1H1O2_gas_species_t_calib_K(double t, double p);
    double Al1H1O2_gas_species_t_calib_Kp(double t, double p);
    int Al1H1O2_gas_species_t_get_param_number();
    const char **Al1H1O2_gas_species_t_get_param_names();
    const char **Al1H1O2_gas_species_t_get_param_units();
    void Al1H1O2_gas_species_t_get_param_values(double **values);
    int Al1H1O2_gas_species_t_set_param_values(double *values);
    double Al1H1O2_gas_species_t_get_param_value(int index);
    int Al1H1O2_gas_species_t_set_param_value(int index, double value);
    double Al1H1O2_gas_species_t_dparam_g(double t, double p, int index);
    double Al1H1O2_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Al1H1O2_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Al1H1O2_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Al1H1O2_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Al1H1O2_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Al1H1O2_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Al1H1O2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Al1H1O2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Al1H1O2_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Al1H1O2_gas_species_t_calib_identifier():
    result = <bytes> Al1H1O2_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Al1H1O2_gas_species_t_calib_name():
    result = <bytes> Al1H1O2_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Al1H1O2_gas_species_t_calib_formula():
    result = <bytes> Al1H1O2_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Al1H1O2_gas_species_t_calib_mw():
    result = Al1H1O2_gas_species_t_calib_mw()
    return result
def cy_Al1H1O2_gas_species_t_calib_elements():
    cdef const double *e = Al1H1O2_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Al1H1O2_gas_species_t_calib_g(double t, double p):
    result = Al1H1O2_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_dgdt(double t, double p):
    result = Al1H1O2_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_dgdp(double t, double p):
    result = Al1H1O2_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_d2gdt2(double t, double p):
    result = Al1H1O2_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Al1H1O2_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_d2gdp2(double t, double p):
    result = Al1H1O2_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_d3gdt3(double t, double p):
    result = Al1H1O2_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Al1H1O2_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Al1H1O2_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_d3gdp3(double t, double p):
    result = Al1H1O2_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_s(double t, double p):
    result = Al1H1O2_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_v(double t, double p):
    result = Al1H1O2_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_cv(double t, double p):
    result = Al1H1O2_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_cp(double t, double p):
    result = Al1H1O2_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_dcpdt(double t, double p):
    result = Al1H1O2_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_alpha(double t, double p):
    result = Al1H1O2_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_beta(double t, double p):
    result = Al1H1O2_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_K(double t, double p):
    result = Al1H1O2_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_calib_Kp(double t, double p):
    result = Al1H1O2_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Al1H1O2_gas_species_t_get_param_number():
    result = Al1H1O2_gas_species_t_get_param_number()
    return result
def cy_Al1H1O2_gas_species_t_get_param_names():
    cdef const char **names = Al1H1O2_gas_species_t_get_param_names()
    n = Al1H1O2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al1H1O2_gas_species_t_get_param_units():
    cdef const char **units = Al1H1O2_gas_species_t_get_param_units()
    n = Al1H1O2_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al1H1O2_gas_species_t_get_param_values():
    n = Al1H1O2_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Al1H1O2_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Al1H1O2_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Al1H1O2_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Al1H1O2_gas_species_t_get_param_value(int index):
    result = Al1H1O2_gas_species_t_get_param_value(<int> index)
    return result
def cy_Al1H1O2_gas_species_t_set_param_value(int index, double value):
    result = Al1H1O2_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Al1H1O2_gas_species_t_dparam_g(double t, double p, int index):
    result = Al1H1O2_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O2_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Al1H1O2_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O2_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Al1H1O2_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O2_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Al1H1O2_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O2_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Al1H1O2_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O2_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Al1H1O2_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O2_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Al1H1O2_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O2_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Al1H1O2_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O2_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Al1H1O2_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al1H1O2_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Al1H1O2_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "H1_gas_species_t_calib.h":
    const char *H1_gas_species_t_calib_identifier();
    const char *H1_gas_species_t_calib_name();
    const char *H1_gas_species_t_calib_formula();
    const double H1_gas_species_t_calib_mw();
    const double *H1_gas_species_t_calib_elements();
    double H1_gas_species_t_calib_g(double t, double p);
    double H1_gas_species_t_calib_dgdt(double t, double p);
    double H1_gas_species_t_calib_dgdp(double t, double p);
    double H1_gas_species_t_calib_d2gdt2(double t, double p);
    double H1_gas_species_t_calib_d2gdtdp(double t, double p);
    double H1_gas_species_t_calib_d2gdp2(double t, double p);
    double H1_gas_species_t_calib_d3gdt3(double t, double p);
    double H1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double H1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double H1_gas_species_t_calib_d3gdp3(double t, double p);
    double H1_gas_species_t_calib_s(double t, double p);
    double H1_gas_species_t_calib_v(double t, double p);
    double H1_gas_species_t_calib_cv(double t, double p);
    double H1_gas_species_t_calib_cp(double t, double p);
    double H1_gas_species_t_calib_dcpdt(double t, double p);
    double H1_gas_species_t_calib_alpha(double t, double p);
    double H1_gas_species_t_calib_beta(double t, double p);
    double H1_gas_species_t_calib_K(double t, double p);
    double H1_gas_species_t_calib_Kp(double t, double p);
    int H1_gas_species_t_get_param_number();
    const char **H1_gas_species_t_get_param_names();
    const char **H1_gas_species_t_get_param_units();
    void H1_gas_species_t_get_param_values(double **values);
    int H1_gas_species_t_set_param_values(double *values);
    double H1_gas_species_t_get_param_value(int index);
    int H1_gas_species_t_set_param_value(int index, double value);
    double H1_gas_species_t_dparam_g(double t, double p, int index);
    double H1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double H1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double H1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double H1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double H1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double H1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double H1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double H1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double H1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_H1_gas_species_t_calib_identifier():
    result = <bytes> H1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_H1_gas_species_t_calib_name():
    result = <bytes> H1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_H1_gas_species_t_calib_formula():
    result = <bytes> H1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_H1_gas_species_t_calib_mw():
    result = H1_gas_species_t_calib_mw()
    return result
def cy_H1_gas_species_t_calib_elements():
    cdef const double *e = H1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_H1_gas_species_t_calib_g(double t, double p):
    result = H1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_dgdt(double t, double p):
    result = H1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_dgdp(double t, double p):
    result = H1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_d2gdt2(double t, double p):
    result = H1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = H1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_d2gdp2(double t, double p):
    result = H1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_d3gdt3(double t, double p):
    result = H1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = H1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = H1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_d3gdp3(double t, double p):
    result = H1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_s(double t, double p):
    result = H1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_v(double t, double p):
    result = H1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_cv(double t, double p):
    result = H1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_cp(double t, double p):
    result = H1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_dcpdt(double t, double p):
    result = H1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_alpha(double t, double p):
    result = H1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_beta(double t, double p):
    result = H1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_K(double t, double p):
    result = H1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_calib_Kp(double t, double p):
    result = H1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_H1_gas_species_t_get_param_number():
    result = H1_gas_species_t_get_param_number()
    return result
def cy_H1_gas_species_t_get_param_names():
    cdef const char **names = H1_gas_species_t_get_param_names()
    n = H1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H1_gas_species_t_get_param_units():
    cdef const char **units = H1_gas_species_t_get_param_units()
    n = H1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_H1_gas_species_t_get_param_values():
    n = H1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    H1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_H1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = H1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_H1_gas_species_t_get_param_value(int index):
    result = H1_gas_species_t_get_param_value(<int> index)
    return result
def cy_H1_gas_species_t_set_param_value(int index, double value):
    result = H1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_H1_gas_species_t_dparam_g(double t, double p, int index):
    result = H1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_H1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = H1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_H1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = H1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_H1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = H1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_H1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = H1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_H1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = H1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_H1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = H1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_H1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = H1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_H1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = H1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_H1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = H1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Mg1_gas_species_t_calib.h":
    const char *Mg1_gas_species_t_calib_identifier();
    const char *Mg1_gas_species_t_calib_name();
    const char *Mg1_gas_species_t_calib_formula();
    const double Mg1_gas_species_t_calib_mw();
    const double *Mg1_gas_species_t_calib_elements();
    double Mg1_gas_species_t_calib_g(double t, double p);
    double Mg1_gas_species_t_calib_dgdt(double t, double p);
    double Mg1_gas_species_t_calib_dgdp(double t, double p);
    double Mg1_gas_species_t_calib_d2gdt2(double t, double p);
    double Mg1_gas_species_t_calib_d2gdtdp(double t, double p);
    double Mg1_gas_species_t_calib_d2gdp2(double t, double p);
    double Mg1_gas_species_t_calib_d3gdt3(double t, double p);
    double Mg1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Mg1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Mg1_gas_species_t_calib_d3gdp3(double t, double p);
    double Mg1_gas_species_t_calib_s(double t, double p);
    double Mg1_gas_species_t_calib_v(double t, double p);
    double Mg1_gas_species_t_calib_cv(double t, double p);
    double Mg1_gas_species_t_calib_cp(double t, double p);
    double Mg1_gas_species_t_calib_dcpdt(double t, double p);
    double Mg1_gas_species_t_calib_alpha(double t, double p);
    double Mg1_gas_species_t_calib_beta(double t, double p);
    double Mg1_gas_species_t_calib_K(double t, double p);
    double Mg1_gas_species_t_calib_Kp(double t, double p);
    int Mg1_gas_species_t_get_param_number();
    const char **Mg1_gas_species_t_get_param_names();
    const char **Mg1_gas_species_t_get_param_units();
    void Mg1_gas_species_t_get_param_values(double **values);
    int Mg1_gas_species_t_set_param_values(double *values);
    double Mg1_gas_species_t_get_param_value(int index);
    int Mg1_gas_species_t_set_param_value(int index, double value);
    double Mg1_gas_species_t_dparam_g(double t, double p, int index);
    double Mg1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Mg1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Mg1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Mg1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Mg1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Mg1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Mg1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Mg1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Mg1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Mg1_gas_species_t_calib_identifier():
    result = <bytes> Mg1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Mg1_gas_species_t_calib_name():
    result = <bytes> Mg1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Mg1_gas_species_t_calib_formula():
    result = <bytes> Mg1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Mg1_gas_species_t_calib_mw():
    result = Mg1_gas_species_t_calib_mw()
    return result
def cy_Mg1_gas_species_t_calib_elements():
    cdef const double *e = Mg1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Mg1_gas_species_t_calib_g(double t, double p):
    result = Mg1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_dgdt(double t, double p):
    result = Mg1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_dgdp(double t, double p):
    result = Mg1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_d2gdt2(double t, double p):
    result = Mg1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Mg1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_d2gdp2(double t, double p):
    result = Mg1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_d3gdt3(double t, double p):
    result = Mg1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Mg1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Mg1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_d3gdp3(double t, double p):
    result = Mg1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_s(double t, double p):
    result = Mg1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_v(double t, double p):
    result = Mg1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_cv(double t, double p):
    result = Mg1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_cp(double t, double p):
    result = Mg1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_dcpdt(double t, double p):
    result = Mg1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_alpha(double t, double p):
    result = Mg1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_beta(double t, double p):
    result = Mg1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_K(double t, double p):
    result = Mg1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_calib_Kp(double t, double p):
    result = Mg1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Mg1_gas_species_t_get_param_number():
    result = Mg1_gas_species_t_get_param_number()
    return result
def cy_Mg1_gas_species_t_get_param_names():
    cdef const char **names = Mg1_gas_species_t_get_param_names()
    n = Mg1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Mg1_gas_species_t_get_param_units():
    cdef const char **units = Mg1_gas_species_t_get_param_units()
    n = Mg1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Mg1_gas_species_t_get_param_values():
    n = Mg1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Mg1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Mg1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Mg1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Mg1_gas_species_t_get_param_value(int index):
    result = Mg1_gas_species_t_get_param_value(<int> index)
    return result
def cy_Mg1_gas_species_t_set_param_value(int index, double value):
    result = Mg1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Mg1_gas_species_t_dparam_g(double t, double p, int index):
    result = Mg1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Mg1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Mg1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Mg1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Mg1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Mg1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Mg1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Mg1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Mg1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Mg1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Mg1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Mg1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Mg1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Mg1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Mg1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Mg1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Mg1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Mg1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Mg1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "Al1O1_gas_species_t_calib.h":
    const char *Al1O1_gas_species_t_calib_identifier();
    const char *Al1O1_gas_species_t_calib_name();
    const char *Al1O1_gas_species_t_calib_formula();
    const double Al1O1_gas_species_t_calib_mw();
    const double *Al1O1_gas_species_t_calib_elements();
    double Al1O1_gas_species_t_calib_g(double t, double p);
    double Al1O1_gas_species_t_calib_dgdt(double t, double p);
    double Al1O1_gas_species_t_calib_dgdp(double t, double p);
    double Al1O1_gas_species_t_calib_d2gdt2(double t, double p);
    double Al1O1_gas_species_t_calib_d2gdtdp(double t, double p);
    double Al1O1_gas_species_t_calib_d2gdp2(double t, double p);
    double Al1O1_gas_species_t_calib_d3gdt3(double t, double p);
    double Al1O1_gas_species_t_calib_d3gdt2dp(double t, double p);
    double Al1O1_gas_species_t_calib_d3gdtdp2(double t, double p);
    double Al1O1_gas_species_t_calib_d3gdp3(double t, double p);
    double Al1O1_gas_species_t_calib_s(double t, double p);
    double Al1O1_gas_species_t_calib_v(double t, double p);
    double Al1O1_gas_species_t_calib_cv(double t, double p);
    double Al1O1_gas_species_t_calib_cp(double t, double p);
    double Al1O1_gas_species_t_calib_dcpdt(double t, double p);
    double Al1O1_gas_species_t_calib_alpha(double t, double p);
    double Al1O1_gas_species_t_calib_beta(double t, double p);
    double Al1O1_gas_species_t_calib_K(double t, double p);
    double Al1O1_gas_species_t_calib_Kp(double t, double p);
    int Al1O1_gas_species_t_get_param_number();
    const char **Al1O1_gas_species_t_get_param_names();
    const char **Al1O1_gas_species_t_get_param_units();
    void Al1O1_gas_species_t_get_param_values(double **values);
    int Al1O1_gas_species_t_set_param_values(double *values);
    double Al1O1_gas_species_t_get_param_value(int index);
    int Al1O1_gas_species_t_set_param_value(int index, double value);
    double Al1O1_gas_species_t_dparam_g(double t, double p, int index);
    double Al1O1_gas_species_t_dparam_dgdt(double t, double p, int index);
    double Al1O1_gas_species_t_dparam_dgdp(double t, double p, int index);
    double Al1O1_gas_species_t_dparam_d2gdt2(double t, double p, int index);
    double Al1O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index);
    double Al1O1_gas_species_t_dparam_d2gdp2(double t, double p, int index);
    double Al1O1_gas_species_t_dparam_d3gdt3(double t, double p, int index);
    double Al1O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index);
    double Al1O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index);
    double Al1O1_gas_species_t_dparam_d3gdp3(double t, double p, int index);

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_Al1O1_gas_species_t_calib_identifier():
    result = <bytes> Al1O1_gas_species_t_calib_identifier()
    return result.decode('UTF-8')
def cy_Al1O1_gas_species_t_calib_name():
    result = <bytes> Al1O1_gas_species_t_calib_name()
    return result.decode('UTF-8')
def cy_Al1O1_gas_species_t_calib_formula():
    result = <bytes> Al1O1_gas_species_t_calib_formula()
    return result.decode('UTF-8')
def cy_Al1O1_gas_species_t_calib_mw():
    result = Al1O1_gas_species_t_calib_mw()
    return result
def cy_Al1O1_gas_species_t_calib_elements():
    cdef const double *e = Al1O1_gas_species_t_calib_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Al1O1_gas_species_t_calib_g(double t, double p):
    result = Al1O1_gas_species_t_calib_g(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_dgdt(double t, double p):
    result = Al1O1_gas_species_t_calib_dgdt(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_dgdp(double t, double p):
    result = Al1O1_gas_species_t_calib_dgdp(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_d2gdt2(double t, double p):
    result = Al1O1_gas_species_t_calib_d2gdt2(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_d2gdtdp(double t, double p):
    result = Al1O1_gas_species_t_calib_d2gdtdp(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_d2gdp2(double t, double p):
    result = Al1O1_gas_species_t_calib_d2gdp2(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_d3gdt3(double t, double p):
    result = Al1O1_gas_species_t_calib_d3gdt3(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_d3gdt2dp(double t, double p):
    result = Al1O1_gas_species_t_calib_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_d3gdtdp2(double t, double p):
    result = Al1O1_gas_species_t_calib_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_d3gdp3(double t, double p):
    result = Al1O1_gas_species_t_calib_d3gdp3(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_s(double t, double p):
    result = Al1O1_gas_species_t_calib_s(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_v(double t, double p):
    result = Al1O1_gas_species_t_calib_v(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_cv(double t, double p):
    result = Al1O1_gas_species_t_calib_cv(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_cp(double t, double p):
    result = Al1O1_gas_species_t_calib_cp(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_dcpdt(double t, double p):
    result = Al1O1_gas_species_t_calib_dcpdt(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_alpha(double t, double p):
    result = Al1O1_gas_species_t_calib_alpha(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_beta(double t, double p):
    result = Al1O1_gas_species_t_calib_beta(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_K(double t, double p):
    result = Al1O1_gas_species_t_calib_K(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_calib_Kp(double t, double p):
    result = Al1O1_gas_species_t_calib_Kp(<double> t, <double> p)
    return result
def cy_Al1O1_gas_species_t_get_param_number():
    result = Al1O1_gas_species_t_get_param_number()
    return result
def cy_Al1O1_gas_species_t_get_param_names():
    cdef const char **names = Al1O1_gas_species_t_get_param_names()
    n = Al1O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al1O1_gas_species_t_get_param_units():
    cdef const char **units = Al1O1_gas_species_t_get_param_units()
    n = Al1O1_gas_species_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_Al1O1_gas_species_t_get_param_values():
    n = Al1O1_gas_species_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    Al1O1_gas_species_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_Al1O1_gas_species_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = Al1O1_gas_species_t_set_param_values(m);
    free(m)
    return result
def cy_Al1O1_gas_species_t_get_param_value(int index):
    result = Al1O1_gas_species_t_get_param_value(<int> index)
    return result
def cy_Al1O1_gas_species_t_set_param_value(int index, double value):
    result = Al1O1_gas_species_t_set_param_value(<int> index, <double> value)
    return result
def cy_Al1O1_gas_species_t_dparam_g(double t, double p, int index):
    result = Al1O1_gas_species_t_dparam_g(<double> t, <double> p, <int> index)
    return result
def cy_Al1O1_gas_species_t_dparam_dgdt(double t, double p, int index):
    result = Al1O1_gas_species_t_dparam_dgdt(<double> t, <double> p, <int> index)
    return result
def cy_Al1O1_gas_species_t_dparam_dgdp(double t, double p, int index):
    result = Al1O1_gas_species_t_dparam_dgdp(<double> t, <double> p, <int> index)
    return result
def cy_Al1O1_gas_species_t_dparam_d2gdt2(double t, double p, int index):
    result = Al1O1_gas_species_t_dparam_d2gdt2(<double> t, <double> p, <int> index)
    return result
def cy_Al1O1_gas_species_t_dparam_d2gdtdp(double t, double p, int index):
    result = Al1O1_gas_species_t_dparam_d2gdtdp(<double> t, <double> p, <int> index)
    return result
def cy_Al1O1_gas_species_t_dparam_d2gdp2(double t, double p, int index):
    result = Al1O1_gas_species_t_dparam_d2gdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al1O1_gas_species_t_dparam_d3gdt3(double t, double p, int index):
    result = Al1O1_gas_species_t_dparam_d3gdt3(<double> t, <double> p, <int> index)
    return result
def cy_Al1O1_gas_species_t_dparam_d3gdt2dp(double t, double p, int index):
    result = Al1O1_gas_species_t_dparam_d3gdt2dp(<double> t, <double> p, <int> index)
    return result
def cy_Al1O1_gas_species_t_dparam_d3gdtdp2(double t, double p, int index):
    result = Al1O1_gas_species_t_dparam_d3gdtdp2(<double> t, <double> p, <int> index)
    return result
def cy_Al1O1_gas_species_t_dparam_d3gdp3(double t, double p, int index):
    result = Al1O1_gas_species_t_dparam_d3gdp3(<double> t, <double> p, <int> index)
    return result
