import numpy as np
cimport numpy as cnp # cimport gives us access to NumPy's C API

# here we just replicate the function signature from the header
cdef extern from "Hibonite_hibonite_calc.h":
    const char *Hibonite_hibonite_identifier();
    const char *Hibonite_hibonite_name();
    const char *Hibonite_hibonite_formula();
    const double Hibonite_hibonite_mw();
    const double *Hibonite_hibonite_elements();
    double Hibonite_hibonite_g(double t, double p)
    double Hibonite_hibonite_dgdt(double t, double p)
    double Hibonite_hibonite_dgdp(double t, double p)
    double Hibonite_hibonite_d2gdt2(double t, double p)
    double Hibonite_hibonite_d2gdtdp(double t, double p)
    double Hibonite_hibonite_d2gdp2(double t, double p)
    double Hibonite_hibonite_d3gdt3(double t, double p)
    double Hibonite_hibonite_d3gdt2dp(double t, double p)
    double Hibonite_hibonite_d3gdtdp2(double t, double p)
    double Hibonite_hibonite_d3gdp3(double t, double p)
    double Hibonite_hibonite_s(double t, double p)
    double Hibonite_hibonite_v(double t, double p)
    double Hibonite_hibonite_cv(double t, double p)
    double Hibonite_hibonite_cp(double t, double p)
    double Hibonite_hibonite_dcpdt(double t, double p)
    double Hibonite_hibonite_alpha(double t, double p)
    double Hibonite_hibonite_beta(double t, double p)
    double Hibonite_hibonite_K(double t, double p)
    double Hibonite_hibonite_Kp(double t, double p)

# here is the "wrapper" signature
def cy_Hibonite_hibonite_identifier():
    result = <bytes> Hibonite_hibonite_identifier()
    return result.decode('UTF-8')
def cy_Hibonite_hibonite_name():
    result = <bytes> Hibonite_hibonite_name()
    return result.decode('UTF-8')
def cy_Hibonite_hibonite_formula():
    result = <bytes> Hibonite_hibonite_formula()
    return result.decode('UTF-8')
def cy_Hibonite_hibonite_mw():
    result = Hibonite_hibonite_mw()
    return result
def cy_Hibonite_hibonite_elements():
    cdef const double *e = Hibonite_hibonite_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Hibonite_hibonite_g(double t, double p):
    result = Hibonite_hibonite_g(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_dgdt(double t, double p):
    result = Hibonite_hibonite_dgdt(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_dgdp(double t, double p):
    result = Hibonite_hibonite_dgdp(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_d2gdt2(double t, double p):
    result = Hibonite_hibonite_d2gdt2(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_d2gdtdp(double t, double p):
    result = Hibonite_hibonite_d2gdtdp(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_d2gdp2(double t, double p):
    result = Hibonite_hibonite_d2gdp2(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_d3gdt3(double t, double p):
    result = Hibonite_hibonite_d3gdt3(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_d3gdt2dp(double t, double p):
    result = Hibonite_hibonite_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_d3gdtdp2(double t, double p):
    result = Hibonite_hibonite_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_d3gdp3(double t, double p):
    result = Hibonite_hibonite_d3gdp3(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_s(double t, double p):
    result = Hibonite_hibonite_s(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_v(double t, double p):
    result = Hibonite_hibonite_v(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_cv(double t, double p):
    result = Hibonite_hibonite_cv(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_cp(double t, double p):
    result = Hibonite_hibonite_cp(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_dcpdt(double t, double p):
    result = Hibonite_hibonite_dcpdt(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_alpha(double t, double p):
    result = Hibonite_hibonite_alpha(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_beta(double t, double p):
    result = Hibonite_hibonite_beta(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_K(double t, double p):
    result = Hibonite_hibonite_K(<double> t, <double> p)
    return result
def cy_Hibonite_hibonite_Kp(double t, double p):
    result = Hibonite_hibonite_Kp(<double> t, <double> p)
    return result
