
static char *identifier = "Mon Dec  2 10:25:03 2019";

static double T_r = 298.15;
static double P_r = 1;
static double H_TrPr = 66944.0;
static double S_TrPr = 218.386;
static double k0 = 12.09191998086816;
static double k1 = 0.09596825893458166;
static double k2 = -0.00016540322320045668;
static double k3 = 1.5069154925980045e-07;
static double k4 = -7.169548470542402e-11;
static double k5 = 1.7218038024932976e-14;
static double k6 = -1.6662366701060447e-18;


#include <math.h>

static double gas_species_t_g(double T, double P) {
    double result = 0.0;
    result += H_TrPr + (1.0/7.0)*pow(T, 7)*k6 + (1.0/6.0)*pow(T, 6)*k5 + (1.0/5.0)*pow(T, 5)*k4 + (1.0/4.0)*((T)*(T)*(T)*(T))*k3 + (1.0/3.0)*((T)*(T)*(T))*k2 + (1.0/2.0)*((T)*(T))*k1 + T*k0 - T*(S_TrPr + (1.0/6.0)*pow(T, 6)*k6 + (1.0/5.0)*pow(T, 5)*k5 + (1.0/4.0)*((T)*(T)*(T)*(T))*k4 + (1.0/3.0)*((T)*(T)*(T))*k3 + (1.0/2.0)*((T)*(T))*k2 + T*k1 - 1.0/6.0*pow(T_r, 6)*k6 - 1.0/5.0*pow(T_r, 5)*k5 - 1.0/4.0*((T_r)*(T_r)*(T_r)*(T_r))*k4 - 1.0/3.0*((T_r)*(T_r)*(T_r))*k3 - 1.0/2.0*((T_r)*(T_r))*k2 - T_r*k1 + k0*log(T) - k0*log(T_r)) + 8.3144626181532395*T*log(P) - 1.0/7.0*pow(T_r, 7)*k6 - 1.0/6.0*pow(T_r, 6)*k5 - 1.0/5.0*pow(T_r, 5)*k4 - 1.0/4.0*((T_r)*(T_r)*(T_r)*(T_r))*k3 - 1.0/3.0*((T_r)*(T_r)*(T_r))*k2 - 1.0/2.0*((T_r)*(T_r))*k1 - T_r*k0;
    return result;
}

static double gas_species_t_dgdt(double T, double P) {
    double result = 0.0;
    result += -S_TrPr + (5.0/6.0)*pow(T, 6)*k6 + (4.0/5.0)*pow(T, 5)*k5 + (3.0/4.0)*((T)*(T)*(T)*(T))*k4 + (2.0/3.0)*((T)*(T)*(T))*k3 + (1.0/2.0)*((T)*(T))*k2 - T*(pow(T, 5)*k6 + ((T)*(T)*(T)*(T))*k5 + ((T)*(T)*(T))*k4 + ((T)*(T))*k3 + T*k2 + k1 + k0/T) + (1.0/6.0)*pow(T_r, 6)*k6 + (1.0/5.0)*pow(T_r, 5)*k5 + (1.0/4.0)*((T_r)*(T_r)*(T_r)*(T_r))*k4 + (1.0/3.0)*((T_r)*(T_r)*(T_r))*k3 + (1.0/2.0)*((T_r)*(T_r))*k2 + T_r*k1 - k0*log(T) + k0*log(T_r) + k0 + 8.3144626181532395*log(P);
    return result;
}

static double gas_species_t_dgdp(double T, double P) {
    double result = 0.0;
    result += 8.3144626181532395*T/P;
    return result;
}

static double gas_species_t_d2gdt2(double T, double P) {
    double result = 0.0;
    result += 4*pow(T, 5)*k6 + 3*((T)*(T)*(T)*(T))*k5 + 2*((T)*(T)*(T))*k4 + ((T)*(T))*k3 - T*(5*((T)*(T)*(T)*(T))*k6 + 4*((T)*(T)*(T))*k5 + 3*((T)*(T))*k4 + 2*T*k3 + k2 - k0/((T)*(T))) - k1 - 2*k0/T;
    return result;
}

static double gas_species_t_d2gdtdp(double T, double P) {
    double result = 0.0;
    result += 8.3144626181532395/P;
    return result;
}

static double gas_species_t_d2gdp2(double T, double P) {
    double result = 0.0;
    result += -8.3144626181532395*T/((P)*(P));
    return result;
}

static double gas_species_t_d3gdt3(double T, double P) {
    double result = 0.0;
    result += 15*((T)*(T)*(T)*(T))*k6 + 8*((T)*(T)*(T))*k5 + 3*((T)*(T))*k4 - 2*T*(10*((T)*(T)*(T))*k6 + 6*((T)*(T))*k5 + 3*T*k4 + k3 + k0/((T)*(T)*(T))) - k2 + 3*k0/((T)*(T));
    return result;
}

static double gas_species_t_d3gdt2dp(double T, double P) {
    double result = 0.0;
    result += 0.0;
    return result;
}

static double gas_species_t_d3gdtdp2(double T, double P) {
    double result = 0.0;
    result += -8.3144626181532395/((P)*(P));
    return result;
}

static double gas_species_t_d3gdp3(double T, double P) {
    double result = 0.0;
    result += 16.628925236306479*T/((P)*(P)*(P));
    return result;
}


static double gas_species_t_s(double T, double P) {
    double result = -gas_species_t_dgdt(T, P);
    return result;
}

static double gas_species_t_v(double T, double P) {
    double result = gas_species_t_dgdp(T, P);
    return result;
}

static double gas_species_t_cv(double T, double P) {
    double result = -T*gas_species_t_d2gdt2(T, P);
    double dvdt = gas_species_t_d2gdtdp(T, P);
    double dvdp = gas_species_t_d2gdp2(T, P);
    result += T*dvdt*dvdt/dvdp;
    return result;
}

static double gas_species_t_cp(double T, double P) {
    double result = -T*gas_species_t_d2gdt2(T, P);
    return result;
}

static double gas_species_t_dcpdt(double T, double P) {
    double result = -T*gas_species_t_d3gdt3(T, P) - gas_species_t_d2gdt2(T, P);
    return result;
}

static double gas_species_t_alpha(double T, double P) {
    double result = gas_species_t_d2gdtdp(T, P)/gas_species_t_dgdp(T, P);
    return result;
}

static double gas_species_t_beta(double T, double P) {
    double result = -gas_species_t_d2gdp2(T, P)/gas_species_t_dgdp(T, P);
    return result;
}

static double gas_species_t_K(double T, double P) {
    double result = -gas_species_t_dgdp(T, P)/gas_species_t_d2gdp2(T, P);
    return result;
}

static double gas_species_t_Kp(double T, double P) {
    double result = gas_species_t_dgdp(T, P);
    result *= gas_species_t_d3gdp3(T, P);
    result /= pow(gas_species_t_d2gdp2(T, P), 2.0);
    return result - 1.0;
}


#include <math.h>

static double gas_species_t_dparam_g(double T, double P, int index) {
    double result = 0.0;
    switch (index) {
    case 0: /* T_r */ 
        result += -T*(-pow(T_r, 5)*k6 - ((T_r)*(T_r)*(T_r)*(T_r))*k5 - ((T_r)*(T_r)*(T_r))*k4 - ((T_r)*(T_r))*k3 - T_r*k2 - k1 - k0/T_r) - pow(T_r, 6)*k6 - pow(T_r, 5)*k5 - ((T_r)*(T_r)*(T_r)*(T_r))*k4 - ((T_r)*(T_r)*(T_r))*k3 - ((T_r)*(T_r))*k2 - T_r*k1 - k0;
        break;
    case 1: /* P_r */ 
        result += 0.0;
        break;
    case 2: /* H_TrPr */ 
        result += 1;
        break;
    case 3: /* S_TrPr */ 
        result += -T;
        break;
    case 4: /* k0 */ 
        result += -T*(log(T) - log(T_r)) + T - T_r;
        break;
    case 5: /* k1 */ 
        result += (1.0/2.0)*((T)*(T)) - T*(T - T_r) - 1.0/2.0*((T_r)*(T_r));
        break;
    case 6: /* k2 */ 
        result += (1.0/3.0)*((T)*(T)*(T)) - T*((1.0/2.0)*((T)*(T)) - 1.0/2.0*((T_r)*(T_r))) - 1.0/3.0*((T_r)*(T_r)*(T_r));
        break;
    case 7: /* k3 */ 
        result += (1.0/4.0)*((T)*(T)*(T)*(T)) - T*((1.0/3.0)*((T)*(T)*(T)) - 1.0/3.0*((T_r)*(T_r)*(T_r))) - 1.0/4.0*((T_r)*(T_r)*(T_r)*(T_r));
        break;
    case 8: /* k4 */ 
        result += (1.0/5.0)*pow(T, 5) - T*((1.0/4.0)*((T)*(T)*(T)*(T)) - 1.0/4.0*((T_r)*(T_r)*(T_r)*(T_r))) - 1.0/5.0*pow(T_r, 5);
        break;
    case 9: /* k5 */ 
        result += (1.0/6.0)*pow(T, 6) - T*((1.0/5.0)*pow(T, 5) - 1.0/5.0*pow(T_r, 5)) - 1.0/6.0*pow(T_r, 6);
        break;
    case 10: /* k6 */ 
        result += (1.0/7.0)*pow(T, 7) - T*((1.0/6.0)*pow(T, 6) - 1.0/6.0*pow(T_r, 6)) - 1.0/7.0*pow(T_r, 7);
        break;
    }
    return result;
}

static double gas_species_t_dparam_dgdt(double T, double P, int index) {
    double result = 0.0;
    switch (index) {
    case 0: /* T_r */ 
        result += pow(T_r, 5)*k6 + ((T_r)*(T_r)*(T_r)*(T_r))*k5 + ((T_r)*(T_r)*(T_r))*k4 + ((T_r)*(T_r))*k3 + T_r*k2 + k1 + k0/T_r;
        break;
    case 1: /* P_r */ 
        result += 0.0;
        break;
    case 2: /* H_TrPr */ 
        result += 0.0;
        break;
    case 3: /* S_TrPr */ 
        result += -1;
        break;
    case 4: /* k0 */ 
        result += -log(T) + log(T_r);
        break;
    case 5: /* k1 */ 
        result += -T + T_r;
        break;
    case 6: /* k2 */ 
        result += (1.0/2.0)*(-((T)*(T)) + ((T_r)*(T_r)));
        break;
    case 7: /* k3 */ 
        result += (1.0/3.0)*(-((T)*(T)*(T)) + ((T_r)*(T_r)*(T_r)));
        break;
    case 8: /* k4 */ 
        result += (1.0/4.0)*(-((T)*(T)*(T)*(T)) + ((T_r)*(T_r)*(T_r)*(T_r)));
        break;
    case 9: /* k5 */ 
        result += (1.0/5.0)*(-pow(T, 5) + pow(T_r, 5));
        break;
    case 10: /* k6 */ 
        result += (1.0/6.0)*(-pow(T, 6) + pow(T_r, 6));
        break;
    }
    return result;
}

static double gas_species_t_dparam_dgdp(double T, double P, int index) {
    double result = 0.0;
    switch (index) {
    case 0: /* T_r */ 
        result += 0.0;
        break;
    case 1: /* P_r */ 
        result += 0.0;
        break;
    case 2: /* H_TrPr */ 
        result += 0.0;
        break;
    case 3: /* S_TrPr */ 
        result += 0.0;
        break;
    case 4: /* k0 */ 
        result += 0.0;
        break;
    case 5: /* k1 */ 
        result += 0.0;
        break;
    case 6: /* k2 */ 
        result += 0.0;
        break;
    case 7: /* k3 */ 
        result += 0.0;
        break;
    case 8: /* k4 */ 
        result += 0.0;
        break;
    case 9: /* k5 */ 
        result += 0.0;
        break;
    case 10: /* k6 */ 
        result += 0.0;
        break;
    }
    return result;
}

static double gas_species_t_dparam_d2gdt2(double T, double P, int index) {
    double result = 0.0;
    switch (index) {
    case 0: /* T_r */ 
        result += 0.0;
        break;
    case 1: /* P_r */ 
        result += 0.0;
        break;
    case 2: /* H_TrPr */ 
        result += 0.0;
        break;
    case 3: /* S_TrPr */ 
        result += 0.0;
        break;
    case 4: /* k0 */ 
        result += -1/T;
        break;
    case 5: /* k1 */ 
        result += -1;
        break;
    case 6: /* k2 */ 
        result += -T;
        break;
    case 7: /* k3 */ 
        result += -((T)*(T));
        break;
    case 8: /* k4 */ 
        result += -((T)*(T)*(T));
        break;
    case 9: /* k5 */ 
        result += -((T)*(T)*(T)*(T));
        break;
    case 10: /* k6 */ 
        result += -pow(T, 5);
        break;
    }
    return result;
}

static double gas_species_t_dparam_d2gdtdp(double T, double P, int index) {
    double result = 0.0;
    switch (index) {
    case 0: /* T_r */ 
        result += 0.0;
        break;
    case 1: /* P_r */ 
        result += 0.0;
        break;
    case 2: /* H_TrPr */ 
        result += 0.0;
        break;
    case 3: /* S_TrPr */ 
        result += 0.0;
        break;
    case 4: /* k0 */ 
        result += 0.0;
        break;
    case 5: /* k1 */ 
        result += 0.0;
        break;
    case 6: /* k2 */ 
        result += 0.0;
        break;
    case 7: /* k3 */ 
        result += 0.0;
        break;
    case 8: /* k4 */ 
        result += 0.0;
        break;
    case 9: /* k5 */ 
        result += 0.0;
        break;
    case 10: /* k6 */ 
        result += 0.0;
        break;
    }
    return result;
}

static double gas_species_t_dparam_d2gdp2(double T, double P, int index) {
    double result = 0.0;
    switch (index) {
    case 0: /* T_r */ 
        result += 0.0;
        break;
    case 1: /* P_r */ 
        result += 0.0;
        break;
    case 2: /* H_TrPr */ 
        result += 0.0;
        break;
    case 3: /* S_TrPr */ 
        result += 0.0;
        break;
    case 4: /* k0 */ 
        result += 0.0;
        break;
    case 5: /* k1 */ 
        result += 0.0;
        break;
    case 6: /* k2 */ 
        result += 0.0;
        break;
    case 7: /* k3 */ 
        result += 0.0;
        break;
    case 8: /* k4 */ 
        result += 0.0;
        break;
    case 9: /* k5 */ 
        result += 0.0;
        break;
    case 10: /* k6 */ 
        result += 0.0;
        break;
    }
    return result;
}

static double gas_species_t_dparam_d3gdt3(double T, double P, int index) {
    double result = 0.0;
    switch (index) {
    case 0: /* T_r */ 
        result += 0.0;
        break;
    case 1: /* P_r */ 
        result += 0.0;
        break;
    case 2: /* H_TrPr */ 
        result += 0.0;
        break;
    case 3: /* S_TrPr */ 
        result += 0.0;
        break;
    case 4: /* k0 */ 
        result += pow(T, -2);
        break;
    case 5: /* k1 */ 
        result += 0.0;
        break;
    case 6: /* k2 */ 
        result += -1;
        break;
    case 7: /* k3 */ 
        result += -2*T;
        break;
    case 8: /* k4 */ 
        result += -3*((T)*(T));
        break;
    case 9: /* k5 */ 
        result += -4*((T)*(T)*(T));
        break;
    case 10: /* k6 */ 
        result += -5*((T)*(T)*(T)*(T));
        break;
    }
    return result;
}

static double gas_species_t_dparam_d3gdt2dp(double T, double P, int index) {
    double result = 0.0;
    switch (index) {
    case 0: /* T_r */ 
        result += 0.0;
        break;
    case 1: /* P_r */ 
        result += 0.0;
        break;
    case 2: /* H_TrPr */ 
        result += 0.0;
        break;
    case 3: /* S_TrPr */ 
        result += 0.0;
        break;
    case 4: /* k0 */ 
        result += 0.0;
        break;
    case 5: /* k1 */ 
        result += 0.0;
        break;
    case 6: /* k2 */ 
        result += 0.0;
        break;
    case 7: /* k3 */ 
        result += 0.0;
        break;
    case 8: /* k4 */ 
        result += 0.0;
        break;
    case 9: /* k5 */ 
        result += 0.0;
        break;
    case 10: /* k6 */ 
        result += 0.0;
        break;
    }
    return result;
}

static double gas_species_t_dparam_d3gdtdp2(double T, double P, int index) {
    double result = 0.0;
    switch (index) {
    case 0: /* T_r */ 
        result += 0.0;
        break;
    case 1: /* P_r */ 
        result += 0.0;
        break;
    case 2: /* H_TrPr */ 
        result += 0.0;
        break;
    case 3: /* S_TrPr */ 
        result += 0.0;
        break;
    case 4: /* k0 */ 
        result += 0.0;
        break;
    case 5: /* k1 */ 
        result += 0.0;
        break;
    case 6: /* k2 */ 
        result += 0.0;
        break;
    case 7: /* k3 */ 
        result += 0.0;
        break;
    case 8: /* k4 */ 
        result += 0.0;
        break;
    case 9: /* k5 */ 
        result += 0.0;
        break;
    case 10: /* k6 */ 
        result += 0.0;
        break;
    }
    return result;
}

static double gas_species_t_dparam_d3gdp3(double T, double P, int index) {
    double result = 0.0;
    switch (index) {
    case 0: /* T_r */ 
        result += 0.0;
        break;
    case 1: /* P_r */ 
        result += 0.0;
        break;
    case 2: /* H_TrPr */ 
        result += 0.0;
        break;
    case 3: /* S_TrPr */ 
        result += 0.0;
        break;
    case 4: /* k0 */ 
        result += 0.0;
        break;
    case 5: /* k1 */ 
        result += 0.0;
        break;
    case 6: /* k2 */ 
        result += 0.0;
        break;
    case 7: /* k3 */ 
        result += 0.0;
        break;
    case 8: /* k4 */ 
        result += 0.0;
        break;
    case 9: /* k5 */ 
        result += 0.0;
        break;
    case 10: /* k6 */ 
        result += 0.0;
        break;
    }
    return result;
}

static int gas_species_t_get_param_number(void) {
    return 11;
}

static const char *paramNames[11] = { "T_r", "P_r", "H_TrPr", "S_TrPr", "k0", "k1", "k2", "k3", "k4", "k5", "k6"  };

static const char *paramUnits[11] = { "K", "bar", "J", "J/K", "NA", "NA", "NA", "NA", "NA", "NA", "NA"  };

static const char **gas_species_t_get_param_names(void) {
    return paramNames;
}

static const char **gas_species_t_get_param_units(void) {
    return paramUnits;
}

static void gas_species_t_get_param_values(double **values) {
    (*values)[0] = T_r;
    (*values)[1] = P_r;
    (*values)[2] = H_TrPr;
    (*values)[3] = S_TrPr;
    (*values)[4] = k0;
    (*values)[5] = k1;
    (*values)[6] = k2;
    (*values)[7] = k3;
    (*values)[8] = k4;
    (*values)[9] = k5;
    (*values)[10] = k6;
}

static int gas_species_t_set_param_values(double *values) {
    T_r= values[0];
    P_r= values[1];
    H_TrPr= values[2];
    S_TrPr= values[3];
    k0= values[4];
    k1= values[5];
    k2= values[6];
    k3= values[7];
    k4= values[8];
    k5= values[9];
    k6= values[10];
    return 1;
}

static double gas_species_t_get_param_value(int index) {
    double result = 0.0;
    switch (index) {
    case 0:
        result = T_r;
        break;
    case 1:
        result = P_r;
        break;
    case 2:
        result = H_TrPr;
        break;
    case 3:
        result = S_TrPr;
        break;
    case 4:
        result = k0;
        break;
    case 5:
        result = k1;
        break;
    case 6:
        result = k2;
        break;
    case 7:
        result = k3;
        break;
    case 8:
        result = k4;
        break;
    case 9:
        result = k5;
        break;
    case 10:
        result = k6;
        break;
     default:
         break;
    }
    return result;
}

static int gas_species_t_set_param_value(int index, double value) {
    int result = 1;
    switch (index) {
    case 0:
        T_r = value;
        break;
    case 1:
        P_r = value;
        break;
    case 2:
        H_TrPr = value;
        break;
    case 3:
        S_TrPr = value;
        break;
    case 4:
        k0 = value;
        break;
    case 5:
        k1 = value;
        break;
    case 6:
        k2 = value;
        break;
    case 7:
        k3 = value;
        break;
    case 8:
        k4 = value;
        break;
    case 9:
        k5 = value;
        break;
    case 10:
        k6 = value;
        break;
     default:
         break;
    }
    return result;
}



const char *Al1O1_gas_species_t_calib_identifier(void) {
    return identifier;
}

const char *Al1O1_gas_species_t_calib_name(void) {
    return "Al1O1";
}

const char *Al1O1_gas_species_t_calib_formula(void) {
    return "AlO";
}

const double Al1O1_gas_species_t_calib_mw(void) {
    return 42.980940000000004;
}

static const double elmformula[106] = {
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,1.0,0.0,0.0,0.0,
        0.0,1.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0
    };

const double *Al1O1_gas_species_t_calib_elements(void) {
    return elmformula;
}

double Al1O1_gas_species_t_calib_g(double T, double P) {
    return gas_species_t_g(T, P);
}

double Al1O1_gas_species_t_calib_dgdt(double T, double P) {
    return gas_species_t_dgdt(T, P);
}

double Al1O1_gas_species_t_calib_dgdp(double T, double P) {
    return gas_species_t_dgdp(T, P);
}

double Al1O1_gas_species_t_calib_d2gdt2(double T, double P) {
    return gas_species_t_d2gdt2(T, P);
}

double Al1O1_gas_species_t_calib_d2gdtdp(double T, double P) {
    return gas_species_t_d2gdtdp(T, P);
}

double Al1O1_gas_species_t_calib_d2gdp2(double T, double P) {
    return gas_species_t_d2gdp2(T, P);
}

double Al1O1_gas_species_t_calib_d3gdt3(double T, double P) {
    return gas_species_t_d3gdt3(T, P);
}

double Al1O1_gas_species_t_calib_d3gdt2dp(double T, double P) {
    return gas_species_t_d3gdt2dp(T, P);
}

double Al1O1_gas_species_t_calib_d3gdtdp2(double T, double P) {
    return gas_species_t_d3gdtdp2(T, P);
}

double Al1O1_gas_species_t_calib_d3gdp3(double T, double P) {
    return gas_species_t_d3gdp3(T, P);
}

double Al1O1_gas_species_t_calib_s(double T, double P) {
    return gas_species_t_s(T, P);
}

double Al1O1_gas_species_t_calib_v(double T, double P) {
    return gas_species_t_v(T, P);
}

double Al1O1_gas_species_t_calib_cv(double T, double P) {
    return gas_species_t_cv(T, P);
}

double Al1O1_gas_species_t_calib_cp(double T, double P) {
    return gas_species_t_cp(T, P);
}

double Al1O1_gas_species_t_calib_dcpdt(double T, double P) {
    return gas_species_t_dcpdt(T, P);
}

double Al1O1_gas_species_t_calib_alpha(double T, double P) {
    return gas_species_t_alpha(T, P);
}

double Al1O1_gas_species_t_calib_beta(double T, double P) {
    return gas_species_t_beta(T, P);
}

double Al1O1_gas_species_t_calib_K(double T, double P) {
    return gas_species_t_K(T, P);
}

double Al1O1_gas_species_t_calib_Kp(double T, double P) {
    return gas_species_t_Kp(T, P);
}

int Al1O1_gas_species_t_get_param_number(void) {
    return gas_species_t_get_param_number();
}

const char **Al1O1_gas_species_t_get_param_names(void) {
    return gas_species_t_get_param_names();
}

const char **Al1O1_gas_species_t_get_param_units(void) {
    return gas_species_t_get_param_units();
}

void Al1O1_gas_species_t_get_param_values(double **values) {
    gas_species_t_get_param_values(values);
}

int Al1O1_gas_species_t_set_param_values(double *values) {
    return gas_species_t_set_param_values(values);
}

double Al1O1_gas_species_t_get_param_value(int index) {
    return gas_species_t_get_param_value(index);
}

int Al1O1_gas_species_t_set_param_value(int index, double value) {
    return gas_species_t_set_param_value(index, value);
}

double Al1O1_gas_species_t_dparam_g(double T, double P, int index) {
    return gas_species_t_dparam_g(T, P, index);
}

double Al1O1_gas_species_t_dparam_dgdt(double T, double P, int index) {
    return gas_species_t_dparam_dgdt(T, P, index);
}

double Al1O1_gas_species_t_dparam_dgdp(double T, double P, int index) {
    return gas_species_t_dparam_dgdp(T, P, index);
}

double Al1O1_gas_species_t_dparam_d2gdt2(double T, double P, int index) {
    return gas_species_t_dparam_d2gdt2(T, P, index);
}

double Al1O1_gas_species_t_dparam_d2gdtdp(double T, double P, int index) {
    return gas_species_t_dparam_d2gdtdp(T, P, index);
}

double Al1O1_gas_species_t_dparam_d2gdp2(double T, double P, int index) {
    return gas_species_t_dparam_d2gdp2(T, P, index);
}

double Al1O1_gas_species_t_dparam_d3gdt3(double T, double P, int index) {
    return gas_species_t_dparam_d3gdt3(T, P, index);
}

double Al1O1_gas_species_t_dparam_d3gdt2dp(double T, double P, int index) {
    return gas_species_t_dparam_d3gdt2dp(T, P, index);
}

double Al1O1_gas_species_t_dparam_d3gdtdp2(double T, double P, int index) {
    return gas_species_t_dparam_d3gdtdp2(T, P, index);
}

double Al1O1_gas_species_t_dparam_d3gdp3(double T, double P, int index) {
    return gas_species_t_dparam_d3gdp3(T, P, index);
}

