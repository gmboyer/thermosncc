import numpy as np
cimport numpy as cnp # cimport gives us access to NumPy's C API

# here we just replicate the function signature from the header
cdef extern from "Calciumaluminate_calciumaluminate_calc.h":
    const char *Calciumaluminate_calciumaluminate_identifier();
    const char *Calciumaluminate_calciumaluminate_name();
    const char *Calciumaluminate_calciumaluminate_formula();
    const double Calciumaluminate_calciumaluminate_mw();
    const double *Calciumaluminate_calciumaluminate_elements();
    double Calciumaluminate_calciumaluminate_g(double t, double p)
    double Calciumaluminate_calciumaluminate_dgdt(double t, double p)
    double Calciumaluminate_calciumaluminate_dgdp(double t, double p)
    double Calciumaluminate_calciumaluminate_d2gdt2(double t, double p)
    double Calciumaluminate_calciumaluminate_d2gdtdp(double t, double p)
    double Calciumaluminate_calciumaluminate_d2gdp2(double t, double p)
    double Calciumaluminate_calciumaluminate_d3gdt3(double t, double p)
    double Calciumaluminate_calciumaluminate_d3gdt2dp(double t, double p)
    double Calciumaluminate_calciumaluminate_d3gdtdp2(double t, double p)
    double Calciumaluminate_calciumaluminate_d3gdp3(double t, double p)
    double Calciumaluminate_calciumaluminate_s(double t, double p)
    double Calciumaluminate_calciumaluminate_v(double t, double p)
    double Calciumaluminate_calciumaluminate_cv(double t, double p)
    double Calciumaluminate_calciumaluminate_cp(double t, double p)
    double Calciumaluminate_calciumaluminate_dcpdt(double t, double p)
    double Calciumaluminate_calciumaluminate_alpha(double t, double p)
    double Calciumaluminate_calciumaluminate_beta(double t, double p)
    double Calciumaluminate_calciumaluminate_K(double t, double p)
    double Calciumaluminate_calciumaluminate_Kp(double t, double p)

# here is the "wrapper" signature
def cy_Calciumaluminate_calciumaluminate_identifier():
    result = <bytes> Calciumaluminate_calciumaluminate_identifier()
    return result.decode('UTF-8')
def cy_Calciumaluminate_calciumaluminate_name():
    result = <bytes> Calciumaluminate_calciumaluminate_name()
    return result.decode('UTF-8')
def cy_Calciumaluminate_calciumaluminate_formula():
    result = <bytes> Calciumaluminate_calciumaluminate_formula()
    return result.decode('UTF-8')
def cy_Calciumaluminate_calciumaluminate_mw():
    result = Calciumaluminate_calciumaluminate_mw()
    return result
def cy_Calciumaluminate_calciumaluminate_elements():
    cdef const double *e = Calciumaluminate_calciumaluminate_elements()
    np_array = np.zeros(106)
    for i in range(0,106):
        np_array[i] = e[i]
    return np_array
def cy_Calciumaluminate_calciumaluminate_g(double t, double p):
    result = Calciumaluminate_calciumaluminate_g(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_dgdt(double t, double p):
    result = Calciumaluminate_calciumaluminate_dgdt(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_dgdp(double t, double p):
    result = Calciumaluminate_calciumaluminate_dgdp(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_d2gdt2(double t, double p):
    result = Calciumaluminate_calciumaluminate_d2gdt2(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_d2gdtdp(double t, double p):
    result = Calciumaluminate_calciumaluminate_d2gdtdp(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_d2gdp2(double t, double p):
    result = Calciumaluminate_calciumaluminate_d2gdp2(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_d3gdt3(double t, double p):
    result = Calciumaluminate_calciumaluminate_d3gdt3(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_d3gdt2dp(double t, double p):
    result = Calciumaluminate_calciumaluminate_d3gdt2dp(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_d3gdtdp2(double t, double p):
    result = Calciumaluminate_calciumaluminate_d3gdtdp2(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_d3gdp3(double t, double p):
    result = Calciumaluminate_calciumaluminate_d3gdp3(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_s(double t, double p):
    result = Calciumaluminate_calciumaluminate_s(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_v(double t, double p):
    result = Calciumaluminate_calciumaluminate_v(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_cv(double t, double p):
    result = Calciumaluminate_calciumaluminate_cv(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_cp(double t, double p):
    result = Calciumaluminate_calciumaluminate_cp(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_dcpdt(double t, double p):
    result = Calciumaluminate_calciumaluminate_dcpdt(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_alpha(double t, double p):
    result = Calciumaluminate_calciumaluminate_alpha(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_beta(double t, double p):
    result = Calciumaluminate_calciumaluminate_beta(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_K(double t, double p):
    result = Calciumaluminate_calciumaluminate_K(<double> t, <double> p)
    return result
def cy_Calciumaluminate_calciumaluminate_Kp(double t, double p):
    result = Calciumaluminate_calciumaluminate_Kp(<double> t, <double> p)
    return result
