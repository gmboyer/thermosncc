
const char *H2Mg1O2_gas_species_t_calib_identifier(void);
const char *H2Mg1O2_gas_species_t_calib_name(void);
const char *H2Mg1O2_gas_species_t_calib_formula(void);
const double H2Mg1O2_gas_species_t_calib_mw(void);
const double *H2Mg1O2_gas_species_t_calib_elements(void);

double H2Mg1O2_gas_species_t_calib_g(double T, double P);
double H2Mg1O2_gas_species_t_calib_dgdt(double T, double P);
double H2Mg1O2_gas_species_t_calib_dgdp(double T, double P);
double H2Mg1O2_gas_species_t_calib_d2gdt2(double T, double P);
double H2Mg1O2_gas_species_t_calib_d2gdtdp(double T, double P);
double H2Mg1O2_gas_species_t_calib_d2gdp2(double T, double P);
double H2Mg1O2_gas_species_t_calib_d3gdt3(double T, double P);
double H2Mg1O2_gas_species_t_calib_d3gdt2dp(double T, double P);
double H2Mg1O2_gas_species_t_calib_d3gdtdp2(double T, double P);
double H2Mg1O2_gas_species_t_calib_d3gdp3(double T, double P);

double H2Mg1O2_gas_species_t_calib_s(double T, double P);
double H2Mg1O2_gas_species_t_calib_v(double T, double P);
double H2Mg1O2_gas_species_t_calib_cv(double T, double P);
double H2Mg1O2_gas_species_t_calib_cp(double T, double P);
double H2Mg1O2_gas_species_t_calib_dcpdt(double T, double P);
double H2Mg1O2_gas_species_t_calib_alpha(double T, double P);
double H2Mg1O2_gas_species_t_calib_beta(double T, double P);
double H2Mg1O2_gas_species_t_calib_K(double T, double P);
double H2Mg1O2_gas_species_t_calib_Kp(double T, double P);

int H2Mg1O2_gas_species_t_get_param_number(void);
const char **H2Mg1O2_gas_species_t_get_param_names(void);
const char **H2Mg1O2_gas_species_t_get_param_units(void);
void H2Mg1O2_gas_species_t_get_param_values(double **values);
int H2Mg1O2_gas_species_t_set_param_values(double *values);
double H2Mg1O2_gas_species_t_get_param_value(int index);
int H2Mg1O2_gas_species_t_set_param_value(int index, double value);

double H2Mg1O2_gas_species_t_dparam_g(double T, double P, int index);
double H2Mg1O2_gas_species_t_dparam_dgdt(double T, double P, int index);
double H2Mg1O2_gas_species_t_dparam_dgdp(double T, double P, int index);
double H2Mg1O2_gas_species_t_dparam_d2gdt2(double T, double P, int index);
double H2Mg1O2_gas_species_t_dparam_d2gdtdp(double T, double P, int index);
double H2Mg1O2_gas_species_t_dparam_d2gdp2(double T, double P, int index);
double H2Mg1O2_gas_species_t_dparam_d3gdt3(double T, double P, int index);
double H2Mg1O2_gas_species_t_dparam_d3gdt2dp(double T, double P, int index);
double H2Mg1O2_gas_species_t_dparam_d3gdtdp2(double T, double P, int index);
double H2Mg1O2_gas_species_t_dparam_d3gdp3(double T, double P, int index);

