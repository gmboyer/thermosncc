# Cython numpy wrapper code for arrays is taken from:
# http://gael-varoquaux.info/programming/cython-example-of-exposing-c-computed-arrays-in-python-without-data-copies.html
# Author: Gael Varoquaux, BSD license

# Declare the prototype of the C functions
cdef extern from "IdealGas_t_gas_soln_t_calib.h":
    const char *IdealGas_t_gas_soln_t_calib_identifier();
    const char *IdealGas_t_gas_soln_t_calib_name();
    char *IdealGas_t_gas_soln_t_calib_formula(double T, double P, double n[9]);
    double *IdealGas_t_gas_soln_t_calib_conv_elm_to_moles(double *e);
    int IdealGas_t_gas_soln_t_calib_test_moles(double *n);
    const char *IdealGas_t_gas_soln_t_calib_endmember_name(int index);
    const char *IdealGas_t_gas_soln_t_calib_endmember_formula(int index);
    const double IdealGas_t_gas_soln_t_calib_endmember_mw(int index);
    const double *IdealGas_t_gas_soln_t_calib_endmember_elements(int index);
    double IdealGas_t_gas_soln_t_calib_endmember_mu0(int index, double t, double p);
    double IdealGas_t_gas_soln_t_calib_endmember_dmu0dT(int index, double t, double p);
    double IdealGas_t_gas_soln_t_calib_endmember_dmu0dP(int index, double t, double p);
    double IdealGas_t_gas_soln_t_calib_endmember_d2mu0dT2(int index, double t, double p);
    double IdealGas_t_gas_soln_t_calib_endmember_d2mu0dTdP(int index, double t, double p);
    double IdealGas_t_gas_soln_t_calib_endmember_d2mu0dP2(int index, double t, double p);
    double IdealGas_t_gas_soln_t_calib_endmember_d3mu0dT3(int index, double t, double p);
    double IdealGas_t_gas_soln_t_calib_endmember_d3mu0dT2dP(int index, double t, double p);
    double IdealGas_t_gas_soln_t_calib_endmember_d3mu0dTdP2(int index, double t, double p);
    double IdealGas_t_gas_soln_t_calib_endmember_d3mu0dP3(int index, double t, double p);
    double IdealGas_t_gas_soln_t_calib_g(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_dgdt(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_dgdp(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_d2gdt2(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_d2gdtdp(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_d2gdp2(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_d3gdt3(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_d3gdt2dp(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_d3gdtdp2(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_d3gdp3(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_s(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_v(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_cv(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_cp(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_dcpdt(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_alpha(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_beta(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_K(double t, double p, double n[9])
    double IdealGas_t_gas_soln_t_calib_Kp(double t, double p, double n[9])

    void IdealGas_t_gas_soln_t_calib_dgdn(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d2gdndt(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d2gdndp(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d3gdndt2(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d3gdndtdp(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d3gdndp2(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d4gdndt3(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d4gdndt2dp(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d4gdndtdp2(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d4gdndp3(double T, double P, double n[9], double result[9])

    void IdealGas_t_gas_soln_t_calib_d2gdn2(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d3gdn2dt(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d3gdn2dp(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d4gdn2dt2(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d4gdn2dtdp(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d4gdn2dp2(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d5gdn2dt3(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d5gdn2dt2dp(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d5gdn2dtdp2(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d5gdn2dp3(double T, double P, double n[9], double result[9])

    void IdealGas_t_gas_soln_t_calib_d3gdn3(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d4gdn3dt(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d4gdn3dp(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d5gdn3dt2(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d5gdn3dtdp(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d5gdn3dp2(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d6gdn3dt3(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d6gdn3dt2dp(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d6gdn3dtdp2(double T, double P, double n[9], double result[9])
    void IdealGas_t_gas_soln_t_calib_d6gdn3dp3(double T, double P, double n[9], double result[9])

    int IdealGas_t_gas_soln_t_get_param_number()
    const char **IdealGas_t_gas_soln_t_get_param_names()
    const char **IdealGas_t_gas_soln_t_get_param_units()
    void IdealGas_t_gas_soln_t_get_param_values(double **values)
    int IdealGas_t_gas_soln_t_set_param_values(double *values)
    double IdealGas_t_gas_soln_t_get_param_value(int index)
    int IdealGas_t_gas_soln_t_set_param_value(int index, double value)

    double IdealGas_t_gas_soln_t_dparam_g(double t, double p, double n[9], int index);
    double IdealGas_t_gas_soln_t_dparam_dgdt(double t, double p, double n[9], int index);
    double IdealGas_t_gas_soln_t_dparam_dgdp(double t, double p, double n[9], int index);
    double IdealGas_t_gas_soln_t_dparam_d2gdt2(double t, double p, double n[9], int index);
    double IdealGas_t_gas_soln_t_dparam_d2gdtdp(double t, double p, double n[9], int index);
    double IdealGas_t_gas_soln_t_dparam_d2gdp2(double t, double p, double n[9], int index);
    double IdealGas_t_gas_soln_t_dparam_d3gdt3(double t, double p, double n[9], int index);
    double IdealGas_t_gas_soln_t_dparam_d3gdt2dp(double t, double p, double n[9], int index);
    double IdealGas_t_gas_soln_t_dparam_d3gdtdp2(double t, double p, double n[9], int index);
    double IdealGas_t_gas_soln_t_dparam_d3gdp3(double t, double p, double n[9], int index);

    void IdealGas_t_gas_soln_t_dparam_dgdn(double t, double p, double n[9], int index, double result[9])

from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF
import ctypes

# Import the Python-level symbols of numpy
import numpy as np

# Import the C-level symbols of numpy
cimport numpy as np

# Numpy must be initialized. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
np.import_array()

# here is the "wrapper" signature
def cy_IdealGas_t_gas_soln_t_calib_identifier():
    result = <bytes> IdealGas_t_gas_soln_t_calib_identifier()
    return result.decode('UTF-8')
def cy_IdealGas_t_gas_soln_t_calib_name():
    result = <bytes> IdealGas_t_gas_soln_t_calib_name()
    return result.decode('UTF-8')
def cy_IdealGas_t_gas_soln_t_calib_formula(double t, double p, np_array):
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    r = IdealGas_t_gas_soln_t_calib_formula(<double> t, <double> p, <double *> m)
    result = <bytes> r
    free (m)
    free (r)
    return result.decode('UTF-8')
def cy_IdealGas_t_gas_soln_t_calib_conv_elm_to_moles(np_array):
    cdef double *e = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        e[i] = np_array[i]
    r = IdealGas_t_gas_soln_t_calib_conv_elm_to_moles(<double *> e)
    result = []
    for i in range(9):
        result.append(r[i])
    free (e)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_conv_elm_to_tot_moles(np_array):
    cdef double *e = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        e[i] = np_array[i]
    r = IdealGas_t_gas_soln_t_calib_conv_elm_to_moles(<double *> e)
    result = 0.0
    for i in range(9):
        result += r[i]
    free (e)
    free (r)
    return result
def cy_IdealGas_t_gas_soln_t_calib_conv_elm_to_tot_grams(np_array):
    cdef double *e = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        e[i] = np_array[i]
    r = IdealGas_t_gas_soln_t_calib_conv_elm_to_moles(<double *> e)
    result = 0.0
    for i in range(9):
        mw = cy_IdealGas_t_gas_soln_t_calib_endmember_mw(i)
        result += r[i]*mw
    free (e)
    free (r)
    return result
def cy_IdealGas_t_gas_soln_t_calib_conv_moles_to_tot_moles(np_array):
    result = 0.0
    for i in range(9):
        result += np_array[i]
    return result
def cy_IdealGas_t_gas_soln_t_calib_conv_moles_to_mole_frac(np_array):
    result = np.zeros(9)
    sum = np.sum(np_array)
    for i in range(9):
        result[i] += np_array[i]/sum
    return result
def cy_IdealGas_t_gas_soln_t_calib_conv_moles_to_elm(np_array):
    result = np.zeros(106)
    for i in range(9):
        end = cy_IdealGas_t_gas_soln_t_calib_endmember_elements(i)
        for j in range(0,106):
            result[j] += np_array[i]*end[j]
    return result
def cy_IdealGas_t_gas_soln_t_calib_test_moles(np_array):
    cdef double *n = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        n[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_test_moles(<double *> n)
    free (n)
    return False if result == 0 else True

def cy_IdealGas_t_gas_soln_t_calib_endmember_number():
    return 9
def cy_IdealGas_t_gas_soln_t_calib_endmember_name(int index):
    assert index in range(0,9), "index out of range"
    result = IdealGas_t_gas_soln_t_calib_endmember_name(index);
    return result.decode('UTF-8')
def cy_IdealGas_t_gas_soln_t_calib_endmember_formula(int index):
    assert index in range(0,9), "index out of range"
    result = IdealGas_t_gas_soln_t_calib_endmember_formula(index);
    return result.decode('UTF-8')
def cy_IdealGas_t_gas_soln_t_calib_endmember_mw(int index):
    assert index in range(0,9), "index out of range"
    return IdealGas_t_gas_soln_t_calib_endmember_mw(index);
def cy_IdealGas_t_gas_soln_t_calib_endmember_elements(int index):
    assert index in range(0,9), "index out of range"
    r = IdealGas_t_gas_soln_t_calib_endmember_elements(index);
    result = []
    for i in range(0,106):
        result.append(r[i])
    return np.array(result)

def cy_IdealGas_t_gas_soln_t_calib_species_number():
    return 9
def cy_IdealGas_t_gas_soln_t_calib_species_name(int index):
    assert index in range(0,9), "index out of range"
    result = IdealGas_t_gas_soln_t_calib_endmember_name(index);
    return result.decode('UTF-8')
def cy_IdealGas_t_gas_soln_t_calib_species_formula(int index):
    assert index in range(0,9), "index out of range"
    result = IdealGas_t_gas_soln_t_calib_endmember_formula(index);
    return result.decode('UTF-8')
def cy_IdealGas_t_gas_soln_t_calib_species_mw(int index):
    assert index in range(0,9), "index out of range"
    return IdealGas_t_gas_soln_t_calib_endmember_mw(index);
def cy_IdealGas_t_gas_soln_t_calib_species_elements(int index):
    assert index in range(0,9), "index out of range"
    r = IdealGas_t_gas_soln_t_calib_endmember_elements(index);
    result = []
    for i in range(0,106):
        result.append(r[i])
    return np.array(result)

def cy_IdealGas_t_gas_soln_t_calib_endmember_mu0(int index, double t, double p):
    assert index in range(0,9), "index out of range"
    return IdealGas_t_gas_soln_t_calib_endmember_mu0(index, <double> t, <double> p);
def cy_IdealGas_t_gas_soln_t_calib_endmember_dmu0dT(int index, double t, double p):
    assert index in range(0,9), "index out of range"
    return IdealGas_t_gas_soln_t_calib_endmember_dmu0dT(index, <double> t, <double> p);
def cy_IdealGas_t_gas_soln_t_calib_endmember_dmu0dP(int index, double t, double p):
    assert index in range(0,9), "index out of range"
    return IdealGas_t_gas_soln_t_calib_endmember_dmu0dP(index, <double> t, <double> p);
def cy_IdealGas_t_gas_soln_t_calib_endmember_d2mu0dT2(int index, double t, double p):
    assert index in range(0,9), "index out of range"
    return IdealGas_t_gas_soln_t_calib_endmember_d2mu0dT2(index, <double> t, <double> p);
def cy_IdealGas_t_gas_soln_t_calib_endmember_d2mu0dTdP(int index, double t, double p):
    assert index in range(0,9), "index out of range"
    return IdealGas_t_gas_soln_t_calib_endmember_d2mu0dTdP(index, <double> t, <double> p);
def cy_IdealGas_t_gas_soln_t_calib_endmember_d2mu0dP2(int index, double t, double p):
    assert index in range(0,9), "index out of range"
    return IdealGas_t_gas_soln_t_calib_endmember_d2mu0dP2(index, <double> t, <double> p);
def cy_IdealGas_t_gas_soln_t_calib_endmember_d3mu0dT3(int index, double t, double p):
    assert index in range(0,9), "index out of range"
    return IdealGas_t_gas_soln_t_calib_endmember_d3mu0dT3(index, <double> t, <double> p);
def cy_IdealGas_t_gas_soln_t_calib_endmember_d3mu0dT2dP(int index, double t, double p):
    assert index in range(0,9), "index out of range"
    return IdealGas_t_gas_soln_t_calib_endmember_d3mu0dT2dP(index, <double> t, <double> p);
def cy_IdealGas_t_gas_soln_t_calib_endmember_d3mu0dTdP2(int index, double t, double p):
    assert index in range(0,9), "index out of range"
    return IdealGas_t_gas_soln_t_calib_endmember_d3mu0dTdP2(index, <double> t, <double> p);
def cy_IdealGas_t_gas_soln_t_calib_endmember_d3mu0dP3(int index, double t, double p):
    assert index in range(0,9), "index out of range"
    return IdealGas_t_gas_soln_t_calib_endmember_d3mu0dP3(index, <double> t, <double> p);


import math
import scipy.optimize as opt

def sys_eqns(y, e, lnQ, R, Cb, Cs, print_species=False):
    nT = np.sum(e)
    prod = []
    for i, qq in enumerate(lnQ):
        sum = qq
        for j, yy in enumerate(y):
            sum += R[i,j]*yy
        prod.append(np.exp(sum))
    prod = np.array(prod)
    if print_species:
        print (nT*prod)
    #print (nT*np.matmul(Cb.T,np.exp(y)), nT*np.matmul(Cs.T,prod))
    f = nT*np.matmul(Cb.T,np.exp(y)) + nT*np.matmul(Cs.T,prod) - e
    df  = nT*np.matmul(np.diag(np.exp(y)), Cb) + nT*np.matmul(np.matmul(R.T, np.diag(prod)), Cs)
    return f, df

class Storage:
    t = 0.0
    p = 0.0
    e = np.zeros(1)
    x = np.zeros(1)

def speciate(double t, double p, e):
    if Storage.t == t and Storage.p == p and np.array_equal(Storage.e, e):
        return Storage.x
    Storage.t = t
    Storage.p = p
    Storage.e = np.copy(e)
    Cb = np.array([[1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]])
    Cs = np.array([[1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0], [1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0], [1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0], [1.0, 0.0, 2.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0], [0.0, 0.0, 2.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0], [0.0, 0.0, 2.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0], [1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0], [2.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0], [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0], [1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0], [1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [1.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [2.0, 0.0, 2.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0], [2.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0], [0.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0], [0.0, 0.0, 3.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]])
    R = np.array([[1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0], [1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0], [1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0], [1.0, 0.0, 2.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0], [0.0, 0.0, 2.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0], [0.0, 0.0, 2.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0], [1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0], [2.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0], [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0], [1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0], [1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [1.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [2.0, 0.0, 2.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0], [2.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0], [0.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0], [0.0, 0.0, 3.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]])
    Q = np.empty(24)
    mu_end = np.empty(9)
    mu_end[0] = cy_IdealGas_t_gas_soln_t_calib_endmember_mu0(0, t, p)
    mu_end[1] = cy_IdealGas_t_gas_soln_t_calib_endmember_mu0(1, t, p)
    mu_end[2] = cy_IdealGas_t_gas_soln_t_calib_endmember_mu0(2, t, p)
    mu_end[3] = cy_IdealGas_t_gas_soln_t_calib_endmember_mu0(3, t, p)
    mu_end[4] = cy_IdealGas_t_gas_soln_t_calib_endmember_mu0(4, t, p)
    mu_end[5] = cy_IdealGas_t_gas_soln_t_calib_endmember_mu0(5, t, p)
    mu_end[6] = cy_IdealGas_t_gas_soln_t_calib_endmember_mu0(6, t, p)
    mu_end[7] = cy_IdealGas_t_gas_soln_t_calib_endmember_mu0(7, t, p)
    mu_end[8] = cy_IdealGas_t_gas_soln_t_calib_endmember_mu0(8, t, p)
    Q[0] = math.exp(0.120272355042726*(mu_end[0] + mu_end[5] - 2.76426360063116e-21*t**7 - 1.57527585884597e-17*t**6 + 4.11134595516673e-13*t**5 - 2.49747510449795e-9*t**4 + 7.79832874762599e-6*t**3 - 0.0148707297822744*t**2 + t*(3.22497420073636e-21*t**6 + 1.89033103061516e-17*t**5 - 5.13918244395841e-13*t**4 + 3.32996680599727e-9*t**3 - 1.1697493121439e-5*t**2 + 0.0297414595645487*t + 20.6235937128218*math.log(t) + 62.4472524076614) - 8.31446261815324*t*math.log(p) - 20.6235937128218*t - 252125.070888029)/t)
    Q[1] = math.exp(0.120272355042726*(mu_end[0] + mu_end[2] + mu_end[5] + 1.21320458007363e-19*t**7 - 1.53263177740441e-15*t**6 + 8.44964244131536e-12*t**5 - 2.66503550065132e-8*t**4 + 5.291854482451e-5*t**3 - 0.0684528329328476*t**2 + t*(-1.41540534341923e-19*t**6 + 1.83915813288529e-15*t**5 - 1.05620530516442e-11*t**4 + 3.55338066753509e-8*t**3 - 7.9377817236765e-5*t**2 + 0.136905665865695*t + 4.97195576866001*math.log(t) + 152.302997988496) - 8.31446261815324*t*math.log(p) - 4.97195576866001*t - 27115.3992956674)/t)
    Q[2] = math.exp(0.120272355042726*(mu_end[0] + mu_end[2] + mu_end[5] + 9.1220698805201e-20*t**7 - 1.07172508003969e-15*t**6 + 5.47761867902892e-12*t**5 - 1.6168615238886e-8*t**4 + 3.13588566481245e-5*t**3 - 0.0443650820496362*t**2 + t*(-1.06424148606068e-19*t**6 + 1.28607009604763e-15*t**5 - 6.84702334878614e-12*t**4 + 2.1558153651848e-8*t**3 - 4.70382849721867e-5*t**2 + 0.0887301640992723*t + 12.1799080950045*math.log(t) + 124.240073476734) - 8.31446261815324*t*math.log(p) - 12.1799080950045*t + 186771.675343689)/t)
    Q[3] = math.exp(0.120272355042726*(mu_end[0] + 2*mu_end[2] + mu_end[5] + 9.42921028058586e-20*t**7 - 1.1102593871545e-15*t**6 + 5.69264342038909e-12*t**5 - 1.68466198323769e-8*t**4 + 3.25855245186415e-5*t**3 - 0.0452860004546788*t**2 + t*(-1.10007453273502e-19*t**6 + 1.3323112645854e-15*t**5 - 7.11580427548637e-12*t**4 + 2.24621597765025e-8*t**3 - 4.88782867779622e-5*t**2 + 0.0905720009093577*t + 34.0333047695175*math.log(t) + 37.2817108707994) - 8.31446261815324*t*math.log(p) - 34.0333047695175*t + 473669.495516178)/t)
    Q[4] = math.exp(0.120272355042726*(mu_end[2] + mu_end[5] + 2.38033810015149e-19*t**7 - 2.8696730041555e-15*t**6 + 1.43390969410848e-11*t**5 - 3.76728873149501e-8*t**4 + 5.51344077334856e-5*t**3 - 0.0479841294672908*t**2 + t*(-2.77706111684341e-19*t**6 + 3.4436076049866e-15*t**5 - 1.7923871176356e-11*t**4 + 5.02305164199335e-8*t**3 - 8.27016116002283e-5*t**2 + 0.0959682589345817*t + 12.0919199808682*math.log(t) + 127.032238239301) - 8.31446261815324*t*math.log(p) - 12.0919199808682*t - 60268.7036289389)/t)
    Q[5] = math.exp(0.120272355042726*(2*mu_end[2] + mu_end[5] + 7.92422232047386e-20*t**7 - 9.50193167682578e-16*t**6 + 4.93504405809951e-12*t**5 - 1.45420008912504e-8*t**4 + 2.66689772847453e-5*t**3 - 0.0313091211137954*t**2 + t*(-9.24492604055284e-20*t**6 + 1.14023180121909e-15*t**5 - 6.16880507262439e-12*t**4 + 1.93893345216671e-8*t**3 - 4.0003465927118e-5*t**2 + 0.0626182422275909*t + 39.187454935152*math.log(t) + 12.9763395965377) - 8.31446261815324*t*math.log(p) - 39.187454935152*t + 100054.02681638)/t)
    Q[6] = math.exp(0.120272355042726*(2*mu_end[5] + 1.02584893605863e-19*t**7 - 1.15287039241478e-15*t**6 + 5.44547643616214e-12*t**5 - 1.38051663409568e-8*t**4 + 1.93039789605742e-5*t**3 - 0.0126148840028227*t**2 + t*(-1.19682375873507e-19*t**6 + 1.38344447089773e-15*t**5 - 6.80684554520267e-12*t**4 + 1.84068884546091e-8*t**3 - 2.89559684408613e-5*t**2 + 0.0252297680056455*t + 40.0481316114361*math.log(t) - 0.0325990039399589) - 8.31446261815324*t*math.log(p) - 40.0481316114361*t - 474370.845964965)/t)
    Q[7] = math.exp(0.120272355042726*(mu_end[2] + 2*mu_end[5] + 4.54567792030198e-20*t**7 - 5.79420364638776e-16*t**6 + 3.1962801989992e-12*t**5 - 9.97547091392079e-9*t**4 + 1.93058408999233e-5*t**3 - 0.0238296549368353*t**2 + t*(-5.30329090701897e-20*t**6 + 6.95304437566531e-16*t**5 - 3.995350248749e-12*t**4 + 1.33006278852277e-8*t**3 - 2.89587613498849e-5*t**2 + 0.0476593098736706*t + 43.8700709475995*math.log(t) - 9.57989560875035) - 8.31446261815324*t*math.log(p) - 43.8700709475995*t + 159943.181104371)/t)
    Q[8] = math.exp(0.120272355042726*(2*mu_end[2] + 2*mu_end[5] + 9.21421200052923e-20*t**7 - 1.11549376835201e-15*t**6 + 5.83007859011985e-12*t**5 - 1.71833757541077e-8*t**4 + 3.12596434997326e-5*t**3 - 0.0360044043025888*t**2 + t*(-1.07499140006174e-19*t**6 + 1.33859252202241e-15*t**5 - 7.28759823764981e-12*t**4 + 2.29111676721436e-8*t**3 - 4.68894652495989e-5*t**2 + 0.0720088086051777*t + 57.4046168135318*math.log(t) - 63.9423365882577) - 8.31446261815324*t*math.log(p) - 57.4046168135318*t + 414161.061329184)/t)
    Q[9] = math.exp(0.120272355042726*(mu_end[0] + mu_end[2] + mu_end[7] + 3.37854440021628e-20*t**7 - 3.3934997750811e-16*t**6 + 1.35094334630156e-12*t**5 - 2.71520421980808e-9*t**4 + 3.43033638261682e-6*t**3 - 0.006193926018659*t**2 + t*(-3.94163513358567e-20*t**6 + 4.07219973009731e-16*t**5 - 1.68867918287695e-12*t**4 + 3.62027229307743e-9*t**3 - 5.14550457392523e-6*t**2 + 0.012387852037318*t + 46.1683107666223*math.log(t) - 30.8979902230309) - 8.31446261815324*t*math.log(p) - 46.1683107666223*t + 208120.269101023)/t)
    Q[10] = math.exp(0.120272355042726*(2*mu_end[0] + 2*mu_end[2] + mu_end[7] + 8.50778908055621e-20*t**7 - 8.48540327063744e-16*t**6 + 3.39843646636461e-12*t**5 - 6.94370993592852e-9*t**4 + 8.61977801458076e-6*t**3 - 0.0132218573506504*t**2 + t*(-9.92575392731558e-20*t**6 + 1.01824839247649e-15*t**5 - 4.24804558295576e-12*t**4 + 9.25827991457137e-9*t**3 - 1.29296670218711e-5*t**2 + 0.0264437147013008*t + 76.0127387689416*math.log(t) - 154.433900477804) - 8.31446261815324*t*math.log(p) - 76.0127387689416*t + 634421.520305733)/t)
    Q[11] = math.exp(0.120272355042726*(mu_end[2] + mu_end[7] - 3.69182760823343e-19*t**7 + 3.95026262866721e-15*t**6 - 1.64644128231643e-11*t**5 + 3.36142298265848e-8*t**4 - 3.66049465261648e-5*t**3 + 0.0198569399461368*t**2 + t*(4.30713220960566e-19*t**6 - 4.74031515440066e-15*t**5 + 2.05805160289554e-11*t**4 - 4.48189731021131e-8*t**3 + 5.49074197892472e-5*t**2 - 0.0397138798922737*t + 40.369203143678*math.log(t) - 2.29455392207732) - 8.31446261815324*t*math.log(p) - 40.369203143678*t - 32920.4399900579)/t)
    Q[12] = math.exp(0.120272355042726*(mu_end[0] + mu_end[4] + 2.64140744011339e-20*t**7 - 3.50248736466092e-16*t**6 + 2.02730478153851e-12*t**5 - 6.74512803367821e-9*t**4 + 1.42588874835619e-5*t**3 - 0.0203184643222588*t**2 + t*(-3.08164201346563e-20*t**6 + 4.20298483759311e-16*t**5 - 2.53413097692314e-12*t**4 + 8.99350404490428e-9*t**3 - 2.13883312253429e-5*t**2 + 0.0406369286445176*t + 19.0730342722524*math.log(t) + 74.0956204960503) - 8.31446261815324*t*math.log(p) - 19.0730342722524*t - 161870.344021374)/t)
    Q[13] = math.exp(0.120272355042726*(mu_end[0] + mu_end[2] + mu_end[4] + 4.39210772026971e-20*t**7 - 4.64156479054923e-16*t**6 + 2.02721051308218e-12*t**5 - 4.79000946969286e-9*t**4 + 7.35620774506861e-6*t**3 - 0.0108975412625328*t**2 + t*(-5.124125673648e-20*t**6 + 5.56987774865908e-16*t**5 - 2.53401314135272e-12*t**4 + 6.38667929292382e-9*t**3 - 1.10343116176029e-5*t**2 + 0.0217950825250656*t + 42.6506253869939*math.log(t) - 22.215911469873) - 8.31446261815324*t*math.log(p) - 42.6506253869939*t + 178278.429500936)/t)
    Q[14] = math.exp(0.120272355042726*(mu_end[0] + mu_end[2] - 2.82569168020227e-20*t**7 + 4.05734831047023e-16*t**6 - 2.50202098384527e-12*t**5 + 8.51301119227521e-9*t**4 - 1.67556594935e-5*t**3 + 0.0165442187453185*t**2 + t*(3.29664029356931e-20*t**6 - 4.86881797256428e-16*t**5 + 3.12752622980659e-12*t**4 - 1.13506815897003e-8*t**3 + 2.513348924025e-5*t**2 - 0.0330884374906371*t + 37.2758275996726*math.log(t) - 20.7662734193131) - 8.31446261815324*t*math.log(p) - 37.2758275996726*t - 28961.453869164)/t)
    Q[15] = math.exp(0.120272355042726*(mu_end[0] + 2*mu_end[2] + 3.53211460023406e-20*t**7 - 4.451870154282e-16*t**6 + 2.48400247414149e-12*t**5 - 8.028168063577e-9*t**4 + 1.69062758699963e-5*t**3 - 0.0270672692109634*t**2 + t*(-4.12080036693973e-20*t**6 + 5.3422441851384e-16*t**5 - 3.10500309267686e-12*t**4 + 1.07042240847693e-8*t**3 - 2.53594138049945e-5*t**2 + 0.0541345384219268*t + 22.0718937192624*math.log(t) + 89.202929277007) - 8.31446261815324*t*math.log(p) - 22.0718937192624*t + 6504.65251387989)/t)
    Q[16] = math.exp(0.120272355042726*(2*mu_end[0] + 3.37854440019968e-20*t**7 - 3.09812521498126e-16*t**6 + 9.89329469972254e-13*t**5 - 7.17625205659276e-10*t**4 - 2.87495922051353e-6*t**3 + 0.00520962906844859*t**2 + t*(-3.9416351335663e-20*t**6 + 3.71775025797752e-16*t**5 - 1.23666183746532e-12*t**4 + 9.56833607545701e-10*t**3 + 4.31243883077029e-6*t**2 - 0.0104192581368972*t + 32.4531231895126*math.log(t) - 51.5180892981473) - 8.31446261815324*t*math.log(p) - 32.4531231895126*t + 9292.54401387271)/t)
    Q[17] = math.exp(0.120272355042726*(2*mu_end[0] + 2*mu_end[2] + mu_end[4] + 9.8899208805523e-20*t**7 - 1.04126974675724e-15*t**6 + 4.54166997430063e-12*t**5 - 1.07330252467425e-8*t**4 + 1.63399261196878e-5*t**3 - 0.0231950751774685*t**2 + t*(-1.1538241027311e-19*t**6 + 1.24952369610869e-15*t**5 - 5.67708746787579e-12*t**4 + 1.431070032899e-8*t**3 - 2.45098891795317e-5*t**2 + 0.046390150354937*t + 67.9391971898309*math.log(t) - 131.809889110201) - 8.31446261815324*t*math.log(p) - 67.9391971898309*t + 594330.718192736)/t)
    Q[18] = math.exp(0.120272355042726*(2*mu_end[0] + mu_end[2] - 1.06577718807766e-19*t**7 + 1.20280787754779e-15*t**6 - 6.00919034960628e-12*t**5 + 1.71037199627727e-8*t**4 - 2.84597667877195e-5*t**3 + 0.0206436628825903*t**2 + t*(1.24340671942393e-19*t**6 - 1.44336945305735e-15*t**5 + 7.51148793700785e-12*t**4 - 2.28049599503636e-8*t**3 + 4.26896501815793e-5*t**2 - 0.0412873257651806*t + 42.0148946286873*math.log(t) - 41.4865684879876) - 8.31446261815324*t*math.log(p) - 42.0148946286873*t + 253150.1212647)/t)
    Q[19] = math.exp(0.120272355042726*(mu_end[2] + mu_end[4] + 1.28998968013e-19*t**7 - 3.60184964679638e-15*t**6 + 3.01654659842335e-11*t**5 - 1.22166868059127e-7*t**4 + 0.000272886516406538*t**3 - 0.3373590364518*t**2 + t*(-1.50498796015167e-19*t**6 + 4.32221957615566e-15*t**5 - 3.77068324802919e-11*t**4 + 1.6288915741217e-7*t**3 - 0.000409329774609806*t**2 + 0.6747180729036*t - 159.805623092583*math.log(t) + 954.967263113125) - 8.31446261815324*t*math.log(p) + 159.805623092583*t - 82150.7141494349)/t)
    Q[20] = math.exp(0.120272355042726*(mu_end[2] + mu_end[8] + 4.36139368027401e-20*t**7 - 5.10108226826233e-16*t**6 + 2.58261226217899e-12*t**5 - 7.51371606335218e-9*t**4 + 1.37991641297977e-5*t**3 - 0.016390974711705*t**2 + t*(-5.08829262698635e-20*t**6 + 6.1212987219148e-16*t**5 - 3.22826532772373e-12*t**4 + 1.00182880844696e-8*t**3 - 2.06987461946965e-5*t**2 + 0.0327819494234101*t + 26.4634309684044*math.log(t) + 74.5206698148716) - 8.31446261815324*t*math.log(p) - 26.4634309684044*t - 45356.9671254092)/t)
    Q[21] = math.exp(0.120272355042726*(2*mu_end[2] + 2.76426360008364e-21*t**7 - 9.61125003299e-17*t**6 + 9.26158013809479e-13*t**5 - 4.28768275228985e-9*t**4 + 1.11230447725567e-5*t**3 - 0.0178635412138114*t**2 + t*(-3.22497420009758e-21*t**6 + 1.1533500039588e-16*t**5 - 1.15769751726185e-12*t**4 + 5.71691033638646e-9*t**3 - 1.6684567158835e-5*t**2 + 0.0357270824276229*t + 19.434767455457*math.log(t) + 85.1040115837298) - 8.31446261815324*t*math.log(p) - 19.434767455457*t + 7119.39314913774)/t)
    Q[22] = math.exp(0.120272355042726*(2*mu_end[2] + mu_end[8] + 9.02992776055524e-20*t**7 - 1.09111627106831e-15*t**6 + 5.71395967293537e-12*t**5 - 1.69800814993873e-8*t**4 + 3.1431514366719e-5*t**3 - 0.0373366782630764*t**2 + t*(-1.05349157206478e-19*t**6 + 1.30933952528197e-15*t**5 - 7.14244959116921e-12*t**4 + 2.26401086658498e-8*t**3 - 4.71472715500785e-5*t**2 + 0.0746733565261527*t + 30.0955695944697*math.log(t) + 70.0561879456972) - 8.31446261815324*t*math.log(p) - 30.0955695944697*t + 317012.394498838)/t)
    Q[23] = math.exp(0.120272355042726*(3*mu_end[2] + 1.82134257210558e-19*t**7 - 2.1413249847747e-15*t**6 + 1.08539648990127e-11*t**5 - 3.10329157667594e-8*t**4 + 5.49562399195636e-5*t**3 - 0.062497640620565*t**2 + t*(-2.12489966745651e-19*t**6 + 2.56958998172964e-15*t**5 - 1.35674561237659e-11*t**4 + 4.13772210223459e-8*t**3 - 8.24343598793453e-5*t**2 + 0.12499528124113*t + 13.4615917426282*math.log(t) + 131.298470057141) - 8.31446261815324*t*math.log(p) - 13.4615917426282*t - 134340.21652008)/t)


    # new y guess
    RR = np.hstack((-R,np.eye(24)))
    lnQ = np.log(Q)
    soln = opt.nnls(RR, -lnQ)
    y = -soln[0][:9]
    
    #print("lnQ", lnQ)
    
    # root solver
    result = opt.root(sys_eqns, y, args=(e, np.log(Q), R, Cb, Cs), method="lm", options={'col_deriv':True}, jac=True)
    # print(result)
    if not result.success:
        print("opt.root() was unsuccessful!")
    
    nT = np.sum(e)
    
    #print ('Basis species concentrations:')
    #print (nT*np.exp(result.x))
    #print ('Non-basis species concentrations:')
    #print ('Function value:', sys_eqns(np.array(result.x), e, np.log(Q), R, Cb, Cs, print_species=True))
    
    answ = []
    for i,xx in enumerate(result.x):
        if e[i] == 0:
            answ.append(0.0)
        else:
            answ.append(nT*np.exp(xx))
    Storage.x = np.array(answ)
    return Storage.x

def cy_IdealGas_t_gas_soln_t_calib_g(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_g(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_dgdt(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_dgdt(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_dgdp(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_dgdp(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_d2gdt2(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_d2gdt2(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_d2gdtdp(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_d2gdtdp(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_d2gdp2(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_d2gdp2(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_d3gdt3(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_d3gdt3(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_d3gdt2dp(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_d3gdt2dp(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_d3gdtdp2(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_d3gdtdp2(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_d3gdp3(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_d3gdp3(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_s(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_s(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_v(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_v(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_cv(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_cv(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_cp(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_cp(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_dcpdt(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_dcpdt(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_alpha(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_alpha(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_beta(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_beta(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_K(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_K(<double> t, <double> p, <double *> m)
    free (m)
    return result
def cy_IdealGas_t_gas_soln_t_calib_Kp(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_calib_Kp(<double> t, <double> p, <double *> m)
    free (m)
    return result

def cy_IdealGas_t_gas_soln_t_calib_dgdn(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    cdef double *r = <double *>malloc(9*sizeof(double))
    IdealGas_t_gas_soln_t_calib_dgdn(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(9):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)    
def cy_IdealGas_t_gas_soln_t_calib_d2gdndt(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    cdef double *r = <double *>malloc(9*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d2gdndt(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(9):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d2gdndp(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    cdef double *r = <double *>malloc(9*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d2gdndp(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(9):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d3gdndt2(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    cdef double *r = <double *>malloc(9*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d3gdndt2(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(9):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d3gdndtdp(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    cdef double *r = <double *>malloc(9*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d3gdndtdp(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(9):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d3gdndp2(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    cdef double *r = <double *>malloc(9*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d3gdndp2(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(9):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d4gdndt3(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    cdef double *r = <double *>malloc(9*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d4gdndt3(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(9):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d4gdndt2dp(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    cdef double *r = <double *>malloc(9*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d4gdndt2dp(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(9):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d4gdndtdp2(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    cdef double *r = <double *>malloc(9*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d4gdndtdp2(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(9):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d4gdndp4(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    cdef double *r = <double *>malloc(9*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d4gdndp3(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(9):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)

def cy_IdealGas_t_gas_soln_t_calib_d2gdn2(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    ndim = int(9*(9-1)/2 + 9)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d2gdn2(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d3gdn2dt(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    ndim = int(9*(9-1)/2 + 9)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d3gdn2dt(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d3gdn2dp(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    ndim = int(9*(9-1)/2 + 9)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d3gdn2dp(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d4gdn2dt2(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    ndim = int(9*(9-1)/2 + 9)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d4gdn2dt2(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d4gdn2dtdp(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    ndim = int(9*(9-1)/2 + 9)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d4gdn2dtdp(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d4gdn2dp2(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    ndim = int(9*(9-1)/2 + 9)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d4gdn2dp2(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d5gdn2dt3(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    ndim = int(9*(9-1)/2 + 9)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d5gdn2dt3(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d5gdn2dt2dp(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    ndim = int(9*(9-1)/2 + 9)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d5gdn2dt2dp(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d5gdn2dtdp2(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    ndim = int(9*(9-1)/2 + 9)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d5gdn2dtdp2(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d5gdn2dp3(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    ndim = int(9*(9-1)/2 + 9)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d5gdn2dp3(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)

def cy_IdealGas_t_gas_soln_t_calib_d3gdn3(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    nc = 9
    ndim = int(nc*(nc+1)*(nc+2)/6)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d3gdn3(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d4gdn3dt(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    nc = 9
    ndim = int(nc*(nc+1)*(nc+2)/6)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d4gdn3dt(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d4gdn3dp(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    nc = 9
    ndim = int(nc*(nc+1)*(nc+2)/6)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d4gdn3dp(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d5gdn3dt2(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    nc = 9
    ndim = int(nc*(nc+1)*(nc+2)/6)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d5gdn3dt2(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d5gdn3dtdp(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    nc = 9
    ndim = int(nc*(nc+1)*(nc+2)/6)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d5gdn3dtdp(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d5gdn3dp2(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    nc = 9
    ndim = int(nc*(nc+1)*(nc+2)/6)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d5gdn3dp2(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d6gdn3dt3(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    nc = 9
    ndim = int(nc*(nc+1)*(nc+2)/6)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d6gdn3dt3(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d6gdn3dt2dp(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    nc = 9
    ndim = int(nc*(nc+1)*(nc+2)/6)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d6gdn3dt2dp(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d6gdn3dtdp2(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    nc = 9
    ndim = int(nc*(nc+1)*(nc+2)/6)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d6gdn3dtdp2(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)
def cy_IdealGas_t_gas_soln_t_calib_d6gdn3dp3(double t, double p, np_array):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    nc = 9
    ndim = int(nc*(nc+1)*(nc+2)/6)
    cdef double *r = <double *>malloc(ndim*sizeof(double))
    IdealGas_t_gas_soln_t_calib_d6gdn3dp3(<double> t, <double> p, <double *> m, <double *> r)
    result = []
    for i in range(ndim):
        result.append(r[i])
    free (m)
    free (r)
    return np.array(result)


def cy_IdealGas_t_gas_soln_t_get_param_number():
    result = IdealGas_t_gas_soln_t_get_param_number()
    return result
def cy_IdealGas_t_gas_soln_t_get_param_names():
    cdef const char **names = IdealGas_t_gas_soln_t_get_param_names()
    n = IdealGas_t_gas_soln_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> names[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_IdealGas_t_gas_soln_t_get_param_units():
    cdef const char **units = IdealGas_t_gas_soln_t_get_param_units()
    n = IdealGas_t_gas_soln_t_get_param_number()
    result = []
    for i in range(0,n):
        entry = <bytes> units[i]
        result.append(entry.decode('UTF-8'))
    return result
def cy_IdealGas_t_gas_soln_t_get_param_values():
    n = IdealGas_t_gas_soln_t_get_param_number()
    cdef double *m = <double *>malloc(n*sizeof(double))
    IdealGas_t_gas_soln_t_get_param_values(&m)
    np_array = np.zeros(n)
    for i in range(n):
        np_array[i] = m[i]
    free(m)
    return np_array
def cy_IdealGas_t_gas_soln_t_set_param_values(np_array):
    n = len(np_array)
    cdef double *m = <double *>malloc(n*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_set_param_values(m);
    free(m)
    return result
def cy_IdealGas_t_gas_soln_t_get_param_value(int index):
    result = IdealGas_t_gas_soln_t_get_param_value(<int> index)
    return result
def cy_IdealGas_t_gas_soln_t_set_param_value(int index, double value):
    result = IdealGas_t_gas_soln_t_set_param_value(<int> index, <double> value)
    return result

def cy_IdealGas_t_gas_soln_t_dparam_g(double t, double p, np_array, int index):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_dparam_g(<double> t, <double> p, <double *> m, <int> index)
    free(m)
    return result
def cy_IdealGas_t_gas_soln_t_dparam_dgdt(double t, double p, np_array, int index):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_dparam_dgdt(<double> t, <double> p, <double *> m, <int> index)
    free(m)
    return result
def cy_IdealGas_t_gas_soln_t_dparam_dgdp(double t, double p, np_array, int index):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_dparam_dgdp(<double> t, <double> p, <double *> m, <int> index)
    free(m)
    return result
def cy_IdealGas_t_gas_soln_t_dparam_d2gdt2(double t, double p, np_array, int index):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_dparam_d2gdt2(<double> t, <double> p, <double *> m, <int> index)
    free(m)
    return result
def cy_IdealGas_t_gas_soln_t_dparam_d2gdtdp(double t, double p, np_array, int index):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_dparam_d2gdtdp(<double> t, <double> p, <double *> m, <int> index)
    free(m)
    return result
def cy_IdealGas_t_gas_soln_t_dparam_d2gdp2(double t, double p, np_array, int index):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_dparam_d2gdp2(<double> t, <double> p, <double *> m, <int> index)
    free(m)
    return result
def cy_IdealGas_t_gas_soln_t_dparam_d3gdt3(double t, double p, np_array, int index):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_dparam_d3gdt3(<double> t, <double> p, <double *> m, <int> index)
    free(m)
    return result
def cy_IdealGas_t_gas_soln_t_dparam_d3gdt2dp(double t, double p, np_array, int index):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_dparam_d3gdt2dp(<double> t, <double> p, <double *> m, <int> index)
    free(m)
    return result
def cy_IdealGas_t_gas_soln_t_dparam_d3gdtdp2(double t, double p, np_array, int index):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_dparam_d3gdtdp2(<double> t, <double> p, <double *> m, <int> index)
    free(m)
    return result
def cy_IdealGas_t_gas_soln_t_dparam_d3gdp3(double t, double p, np_array, int index):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    result = IdealGas_t_gas_soln_t_dparam_d3gdp3(<double> t, <double> p, <double *> m, <int> index)
    free(m)
    return result

def cy_IdealGas_t_gas_soln_t_dparam_dgdn(double t, double p, np_array, int index):
    np_array = speciate(t, p, np_array)
    cdef double *m = <double *>malloc(len(np_array)*sizeof(double))
    cdef double *r = <double *>malloc(len(np_array)*sizeof(double))
    for i in range(np_array.size):
        m[i] = np_array[i]
    IdealGas_t_gas_soln_t_dparam_dgdn(<double> t, <double> p, <double *> m, <int> index, <double *> r)
    r_np_array = np.zeros(len(np_array))
    for i in range(r_np_array.size):
        r_np_array[i] = r[i]
    free(m)
    free(r)
    return r_np_array

