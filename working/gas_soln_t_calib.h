#include <math.h>


static double gas_soln_t_dparam_g(double T, double P, double n[9], int index) {
    double n1 = n[0];
    double n2 = n[1];
    double n3 = n[2];
    double n4 = n[3];
    double n5 = n[4];
    double n6 = n[5];
    double n7 = n[6];
    double n8 = n[7];
    double n9 = n[8];
    double result = 0.0;
    switch (index) {
    case 0:
        result += 0;
        break;
    case 1:
        result += 0;
        break;

    default:
        break;
    }
        return result;
}

static double gas_soln_t_dparam_dgdt(double T, double P, double n[9], int index) {
    double n1 = n[0];
    double n2 = n[1];
    double n3 = n[2];
    double n4 = n[3];
    double n5 = n[4];
    double n6 = n[5];
    double n7 = n[6];
    double n8 = n[7];
    double n9 = n[8];
    double result = 0.0;
    switch (index) {
    case 0:
        result += 0;
        break;
    case 1:
        result += 0;
        break;

    default:
        break;
    }
        return result;
}

static double gas_soln_t_dparam_dgdp(double T, double P, double n[9], int index) {
    double n1 = n[0];
    double n2 = n[1];
    double n3 = n[2];
    double n4 = n[3];
    double n5 = n[4];
    double n6 = n[5];
    double n7 = n[6];
    double n8 = n[7];
    double n9 = n[8];
    double result = 0.0;
    switch (index) {
    case 0:
        result += 0;
        break;
    case 1:
        result += 0;
        break;

    default:
        break;
    }
        return result;
}

static double gas_soln_t_dparam_d2gdt2(double T, double P, double n[9], int index) {
    double n1 = n[0];
    double n2 = n[1];
    double n3 = n[2];
    double n4 = n[3];
    double n5 = n[4];
    double n6 = n[5];
    double n7 = n[6];
    double n8 = n[7];
    double n9 = n[8];
    double result = 0.0;
    switch (index) {
    case 0:
        result += 0;
        break;
    case 1:
        result += 0;
        break;

    default:
        break;
    }
        return result;
}

static double gas_soln_t_dparam_d2gdtdp(double T, double P, double n[9], int index) {
    double n1 = n[0];
    double n2 = n[1];
    double n3 = n[2];
    double n4 = n[3];
    double n5 = n[4];
    double n6 = n[5];
    double n7 = n[6];
    double n8 = n[7];
    double n9 = n[8];
    double result = 0.0;
    switch (index) {
    case 0:
        result += 0;
        break;
    case 1:
        result += 0;
        break;

    default:
        break;
    }
        return result;
}

static double gas_soln_t_dparam_d2gdp2(double T, double P, double n[9], int index) {
    double n1 = n[0];
    double n2 = n[1];
    double n3 = n[2];
    double n4 = n[3];
    double n5 = n[4];
    double n6 = n[5];
    double n7 = n[6];
    double n8 = n[7];
    double n9 = n[8];
    double result = 0.0;
    switch (index) {
    case 0:
        result += 0;
        break;
    case 1:
        result += 0;
        break;

    default:
        break;
    }
        return result;
}

static double gas_soln_t_dparam_d3gdt3(double T, double P, double n[9], int index) {
    double n1 = n[0];
    double n2 = n[1];
    double n3 = n[2];
    double n4 = n[3];
    double n5 = n[4];
    double n6 = n[5];
    double n7 = n[6];
    double n8 = n[7];
    double n9 = n[8];
    double result = 0.0;
    switch (index) {
    case 0:
        result += 0;
        break;
    case 1:
        result += 0;
        break;

    default:
        break;
    }
        return result;
}

static double gas_soln_t_dparam_d3gdt2dp(double T, double P, double n[9], int index) {
    double n1 = n[0];
    double n2 = n[1];
    double n3 = n[2];
    double n4 = n[3];
    double n5 = n[4];
    double n6 = n[5];
    double n7 = n[6];
    double n8 = n[7];
    double n9 = n[8];
    double result = 0.0;
    switch (index) {
    case 0:
        result += 0;
        break;
    case 1:
        result += 0;
        break;

    default:
        break;
    }
        return result;
}

static double gas_soln_t_dparam_d3gdtdp2(double T, double P, double n[9], int index) {
    double n1 = n[0];
    double n2 = n[1];
    double n3 = n[2];
    double n4 = n[3];
    double n5 = n[4];
    double n6 = n[5];
    double n7 = n[6];
    double n8 = n[7];
    double n9 = n[8];
    double result = 0.0;
    switch (index) {
    case 0:
        result += 0;
        break;
    case 1:
        result += 0;
        break;

    default:
        break;
    }
        return result;
}

static double gas_soln_t_dparam_d3gdp3(double T, double P, double n[9], int index) {
    double n1 = n[0];
    double n2 = n[1];
    double n3 = n[2];
    double n4 = n[3];
    double n5 = n[4];
    double n6 = n[5];
    double n7 = n[6];
    double n8 = n[7];
    double n9 = n[8];
    double result = 0.0;
    switch (index) {
    case 0:
        result += 0;
        break;
    case 1:
        result += 0;
        break;

    default:
        break;
    }
        return result;
}
static void gas_soln_t_dparam_dgdn(double T, double P, double n[9], int index, double result[9]) {
    double n1 = n[0];
    double n2 = n[1];
    double n3 = n[2];
    double n4 = n[3];
    double n5 = n[4];
    double n6 = n[5];
    double n7 = n[6];
    double n8 = n[7];
    double n9 = n[8];
    switch (index) {
    case 0:
        result[0] = 0;
        result[1] = 0;
        result[2] = 0;
        result[3] = 0;
        result[4] = 0;
        result[5] = 0;
        result[6] = 0;
        result[7] = 0;
        result[8] = 0;
        break;
    case 1:
        result[0] = 0;
        result[1] = 0;
        result[2] = 0;
        result[3] = 0;
        result[4] = 0;
        result[5] = 0;
        result[6] = 0;
        result[7] = 0;
        result[8] = 0;
        break;
    default:
        break;
    }
}

static int gas_soln_t_get_param_number(void) {
    return 2;
}

static const char *paramNames[2] = {"T_r", "P_r"};
static const char *paramUnits[2] = {"K", "bar"};

static const char **gas_soln_t_get_param_names(void) {
    return paramNames;
}

static const char **gas_soln_t_get_param_units(void) {
    return paramUnits;
}

static void gas_soln_t_get_param_values(double **values) {
    (*values)[0] = T_r;
    (*values)[1] = P_r;

}

static int gas_soln_t_set_param_values(double *values) {
    T_r = values[0];
    P_r = values[1];

    return 1;
}

static double gas_soln_t_get_param_value(int index) {
    double result = 0.0;
    switch (index) {
    case 0:
        result = T_r;
        break;
    case 1:
        result = P_r;
        break;

    default:
        break;
    }
    return result;
}

static int gas_soln_t_set_param_value(int index, double value) {
    int result = 1;
    switch (index) {
    case 0:
        T_r = value;
        break;
    case 1:
        P_r = value;
        break;

    default:
        result = 0;
        break;
    }
    return result;
}

