
const char *Calciumaluminate_calciumaluminate_identifier(void);
const char *Calciumaluminate_calciumaluminate_name(void);
const char *Calciumaluminate_calciumaluminate_formula(void);
const double Calciumaluminate_calciumaluminate_mw(void);
const double *Calciumaluminate_calciumaluminate_elements(void);

double Calciumaluminate_calciumaluminate_g(double T, double P);
double Calciumaluminate_calciumaluminate_dgdt(double T, double P);
double Calciumaluminate_calciumaluminate_dgdp(double T, double P);
double Calciumaluminate_calciumaluminate_d2gdt2(double T, double P);
double Calciumaluminate_calciumaluminate_d2gdtdp(double T, double P);
double Calciumaluminate_calciumaluminate_d2gdp2(double T, double P);
double Calciumaluminate_calciumaluminate_d3gdt3(double T, double P);
double Calciumaluminate_calciumaluminate_d3gdt2dp(double T, double P);
double Calciumaluminate_calciumaluminate_d3gdtdp2(double T, double P);
double Calciumaluminate_calciumaluminate_d3gdp3(double T, double P);

double Calciumaluminate_calciumaluminate_s(double T, double P);
double Calciumaluminate_calciumaluminate_v(double T, double P);
double Calciumaluminate_calciumaluminate_cv(double T, double P);
double Calciumaluminate_calciumaluminate_cp(double T, double P);
double Calciumaluminate_calciumaluminate_dcpdt(double T, double P);
double Calciumaluminate_calciumaluminate_alpha(double T, double P);
double Calciumaluminate_calciumaluminate_beta(double T, double P);
double Calciumaluminate_calciumaluminate_K(double T, double P);
double Calciumaluminate_calciumaluminate_Kp(double T, double P);

