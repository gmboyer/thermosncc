from thermoengine import equilibrate
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def create_cond_dict(elm_sys, phs_sys, comp, p, start, stop, by, n_calc=0, write=True, append=False):
    
    # setting a value for "n_calc" overrides the "by" arguement
    if n_calc != 0:
        # temps is an array with "n_calc" elements
        temps = np.linspace(start, stop, n_calc)
    else:
        # temps is an array with stepsize "by"
        temps = np.arange(start, stop+by, by).tolist()
    
    temps = list(reversed(temps)) # start with high temps
    print(temps)
    
    phases_not_to_plot = ["IdealGas", "System"]
    equil = equilibrate.Equilibrate(elm_sys, phs_sys)
    stable_dict = {}
    
    cols = ['t','p']+[phs.phase_name for phs in phs_sys]
    df_moles = pd.DataFrame(columns=cols)
    df_grams = pd.DataFrame(columns=cols)
    
    filename = "_"+"".join(elm_sys)+"_p"+str(p)+"_t"+str(start)+"_"+str(stop)+".csv"
    
    # initialize csvs
    if append == False:
        df_moles.to_csv("moles"+filename)
        df_moles.to_csv("grams"+filename)
    
    for t in temps:
        
        # calculate stabilities of condensates and record them
        try:
            if t == temps[0]:
                print("Initializing equilibration calculation for temperature:", t)
                # initialize
                state = equil.execute(t, p, bulk_comp=comp, debug=0)
            elif float(t)%10 == 0 and n_calc == 0:
                print("Re-initializing equilibration calculation for temperature:", t)
                # initialize
                state = equil.execute(t+(by*2), p, bulk_comp=comp, debug=0)
                state = equil.execute(t+(by), p, state=state, debug=0)
                state = equil.execute(t, p, state=state, debug=0)
            else:
                print("Pickup calculation for temperature:", t)
                # pickup
                try:
                    state = equil.execute(t, p, state=state, debug=0)
                except:
                    print("Pickup failed... re-initializing for temperature:", t)
                    state = equil.execute(t, p, bulk_comp=comp, debug=0)
#                 print("Initializing equilibration calculation for temperature:", t)
#                 # initialize
#                 state = equil.execute(t, p, bulk_comp=comp, debug=0) 
        except:
            print("Encountered a problem. Moving on to next temperature...")
            continue
            
        mole_dict = {phs.phase_name:state.compositions(phs.phase_name, units='moles')[0] for phs in phs_sys}
        gram_dict = {phs.phase_name:state.tot_grams_phase(phs.phase_name) for phs in phs_sys}
        mole_dict.update({'t':t, 'p':p})
        gram_dict.update({'t':t, 'p':p})
        df_moles = df_moles.append(mole_dict, ignore_index=True)
        df_grams = df_grams.append(gram_dict, ignore_index=True)
        
        
        if write == True:
            
            with open("moles"+filename, 'a') as f:
                df_moles.iloc[[-1]].to_csv(f, header=False)
            with open("grams"+filename, 'a') as f:
                df_grams.iloc[[-1]].to_csv(f, header=False)

        # record phase stability
        for cond in [key for key in list(state.properties().keys()) if key not in phases_not_to_plot]:
            if state.compositions(cond, units='moles')[0] > 0.00001:
                try:
                    stable_dict[cond] += [t]
                except:
                    stable_dict[cond] = [t]
        #except:
            #pass
                    
                    
            
    
    # Create a dict of min and max temperatures
    # where condensates appear/disappear.
    cond_dict = {}
    for key in list(stable_dict.keys()):
        if key in [phase.phase_name for phase in phs_sys]:
            this_abbrev = [phase.abbrev for phase in phs_sys if phase.phase_name == key][0]

            cond_dict[this_abbrev.lower()] = []
            condensing = False
            for i,temp in enumerate(temps):
                if temp in stable_dict[key] and not condensing:
                    condensing = True
                    start_temp = temp
                if temp not in stable_dict[key] and condensing:
                    condensing = False
                    stop_temp = temps[i-1]
                    cond_dict[this_abbrev.lower()] += [[start_temp, stop_temp]]
                if temp == temps[-1] and condensing:
                    condensing = False
                    stop_temp = temps[i]
                    cond_dict[this_abbrev.lower()] += [[start_temp, stop_temp]]
                    
    return cond_dict, temps, df_moles, df_grams, "moles"+filename, "grams"+filename
                    

# get x axis value of a condensate if its range is nestable on a plot
def get_x_val(i, cond_dict, cond_name, x_dict, buffer=25):
    successes = []
    for ii in range(0, i):
        conds_at_this_x = [cond for cond in list(x_dict.keys()) if x_dict[cond]==ii]
        for cond in conds_at_this_x:
            if cond_dict[cond_name][1]+buffer < cond_dict[cond][0]:
                successes.append(True)
            else:
                successes.append(False)
        if list(set(successes)) == [True]:
            return ii
    
    try:
        x = max(list(x_dict.values()))+1
    except:
        x = 0
    
    return x


# loop through each condensate temperature range and plot
def condensation_plot(cond_dict, temps=np.linspace(1000, 2000, 10), col_dict={}):
    
    # define default color dictionary
    if col_dict == {}:
        col_dict = {'crn':'orange', # corundum
                    'grst':'green', # grossite
                    'ca1':'black',  # calcium aluminate
                    'mel':'blue',   # mellilite
                    'hib':'red',    # hibonite
                    'prv':'black',  # perovskite 
                    'dsp':'purple', # diaspore
                    'gas':'black',  # ideal gas
                    'h2o':'gray',   # water
                    'coe':'black',  # coesite
                    'crs':'black',
                    'lm':'black',   # lime
                    'qz':'black',   # quartz
                    'trd':'black',
                    'wo':'black',
                    'gh':'black',
                    'an':'black',
                    'grs':'black', # grossular
                    'prh':'black',
                    'prl':'black',
                    'zo':'black',
                    'czo':'black',
                    'pwo':'black',
                    'and':'black',
                    'cr':'black',
                    'ky':'black',
                    'sil':'black',
                    'cats':'black',
                    'rt':'black',
                    'spn':'black',
                    'spl':'black',
                    'crd':'black',
                    'mw':'black',
                   }
    
    x_dict = {}
    for i, cond_name in enumerate(list(cond_dict.keys())):

        # check if this range can nest below another
        #x = get_x_val(i, cond_dict, cond_name, x_dict)
        x = i # test

        x_dict[cond_name] = x

        for i in range(0, len(cond_dict[cond_name])):
            plt.plot([x, x], cond_dict[cond_name][i], lw=10, color=col_dict[cond_name], solid_capstyle="butt")
        plt.text(x, cond_dict[cond_name][-1][1]+10, cond_name, horizontalalignment='center')

    plt.xticks(range(-1, len(list(cond_dict.keys()))))

    # remove ticks on x axis
    plt.tick_params(
        axis='x',
        which='both',
        bottom=False,
        top=False,
        labelbottom=False)

    plt.yticks(range(int(min(temps)), int(max(temps)), 100))
    plt.ylabel("T, K")
    #plt.margins(0.5)
    plt.show()